using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    [RoutePrefix("api/stockissue")]
    public class StockIssueController : BaseApi<StockIssue>
    {
        [HttpPost]
        [Route("query")]
        public ResultObj Query(StockIssueFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(filter);
                var data = raw.Include(x => x.Items).OrderByDescending(x => x.Id)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Number,
                        x.Date,
                        x.CreatedBy,
                        x.CreatedAt,
                        x.ModifiedBy,
                        x.ModifiedAt,
                        x.Destination,
                        x.DestinationId,
                        x.OriginId,
                        x.Driver,
                        Items = x.Items.Select(i => new
                        {
                            i.Id,
                            i.ProductId,
                            Product = new
                            {
                                i.Product.Id,
                                Name = $"{i.Product.Name} ({i.Scheme?.Package.Name ?? i.Product.Package.Name})",
                            },
                            i.Unit,
                            i.SchemeId,
                            i.Quantity
                        }),
                        Status = x.Status.ToString()
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(StockIssue record)
        {
            ResultObj results;
            try
            {
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.Number = ReferenceGenerator.StockIssueNumber();
                record.CreatedBy = User.Identity.Name;
                record.ModifiedBy = User.Identity.Name;
                new StockIssueRepository().Insert(record);

                results = WebHelpers.BuildResponse(record, "Stock Issue Sent.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(StockIssue record)
        {
            ResultObj results;
            try
            {
                if (record.Status == IssueStatus.Issued) throw new Exception("Update not allowed.");
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.ModifiedBy = User.Identity.Name;
                new StockIssueRepository().Update(record);

                results = WebHelpers.BuildResponse(record, "Stock Issue Updated.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpGet]
        [Route("print")]
        public ResultObj PrintWaybill(long id)
        {
            ResultObj results;
            try
            {
                var order = Repository.Get(id);
                var storeInfo = QuickAccess.GetStoreInfo(order.OriginId);

                var contents = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/ReportTemplates/StockIssue.html"));

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[SOURCE]", order.Origin.Name);
                contents = contents.Replace("[DESTINATION]", order.Destination.Name);
                contents = contents.Replace("[DRIVER]", order.Driver);
                contents = contents.Replace("[DATE]", order.Date.ToString("D"));
                contents = contents.Replace("[NUMBER]", order.Number);

                var items = string.Empty;
                foreach (var item in order.Items.GroupBy(q=>q.Product.Type))
                {
                    var count = 0;
                    items += $"<tr><th colspan='3'>{item.Key.Name}</th></tr>";
                    foreach (var sItem in item.ToList())
                    {
                        count++;
                        items += $"<tr><td>{count}</td><td>{sItem.Product.Name} [{sItem.Scheme?.Package.Name ?? sItem.Product.Package.Name}]</td>" +
                            $"<td>{sItem.Quantity}</td></tr>";
                    }
                }



                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);

                results = WebHelpers.BuildResponse(pdfOutput, "Issue Waybill", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Delete(long id)
        {
            Repository = new StockIssueRepository();
            return base.Delete(id);
        }

        private const string NoItemsError = "No items found. Please add items before you can save.";
    }
}