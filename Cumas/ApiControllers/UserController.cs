﻿using System;
using System.Linq;
using Cumas.AxHelpers;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class UserController : BaseApi<User>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().Select(x => new { x.Id, x.Name, x.UserName }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }
    }
}