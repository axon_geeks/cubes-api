using System;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class BankTransactionController : BaseApi<BankTransaction>
    {
        public override ResultObj Post(BankTransaction record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new bank transaction. Username: {User.Identity.Name}");
                if (record.Transaction == Transaction.Deposit)
                {
                    record.Credit = 0;
                    record.Debit = record.Amount;
                }
                else
                {
                    record.Debit = 0;
                    record.Credit = record.Amount;
                }
                new BankTransactionRepository().Insert(SetAudit(record, true));

                results = WebHelpers.BuildResponse(record, "New Transaction Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(BankTransaction record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating BankTransaction. Username: {User.Identity.Name}");
                if (record.Transaction == Transaction.Deposit)
                {
                    record.Credit = 0;
                    record.Debit = record.Amount;
                }
                else
                {
                    record.Debit = 0;
                    record.Credit = record.Amount;
                }

                var oldRecord = Repository.Get(record.Id);
                if (oldRecord.SourceId.HasValue) throw new Exception($"Update not allowed on {oldRecord.Source} transaction.");
                record.OldAmount = oldRecord.Amount;

                new BankTransactionRepository().Update(SetAudit(record));
                results = WebHelpers.BuildResponse(record, "Transaction Update Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/banktransaction/query")]
        public ResultObj Query(BankTransactionFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching BankTransactions. Username: {User.Identity.Name}");
                var raw = Repository.Get(filter);
                var data = raw.OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.Debit,
                        x.Credit,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.StoreId,
                        x.Amount,
                        Transaction = x.Transaction.ToString(),
                        Source = x.Source.ToString(),
                        x.AccountId,
                        x.Note,
                        Store = new { x.Store.Id, x.Store.Name },
                        Account = new { x.Account.Id, x.Account.Name }
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }


        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                new BankTransactionRepository().Delete(id);
                results = WebHelpers.BuildResponse(id, "Transaction Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }
    }
}