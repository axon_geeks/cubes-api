using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class SupplierPaymentController : BaseApi<SupplierPayment>
    {
        [HttpPost]
        [Route("api/supplierPayment/query")]
        public ResultObj Query(SupplierPaymentFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(filter);
                var data = raw.OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.ReceiptNumber,
                        x.Amount,
                        x.TransactionCode,
                        x.InvoiceRef,
                        Supplier = x.Supplier.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(SupplierPayment record)
        {
            ResultObj results;
            try
            {
                //record.Amount = (record.Balance < 0) ? record.Amount + record.Balance : record.Amount;
                //record.Balance = (record.Balance < 0) ? 0 : record.Balance;

                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }

                new SupplierPaymentRepository().Insert(SetAudit(record, true));

                results = WebHelpers.BuildResponse(record, "New SupplierPayment Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/supplierPayment/report")]
        public ResultObj Report(SupplierPaymentFilter filter)
        {

            ResultObj results;
            try
            {
                var payments = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/PaymentReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var totalAmount = payments.Sum(x => x.Amount);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var payment in payments)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{payment.Date.ToString("F")}</td>" +
                                $"<td>{payment.ReceiptNumber}</td>" +
                                $"<td>{payment.Supplier?.Name ?? "Walk-In"}</td>" +
                                $"<td>{payment.Amount}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Payment Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }
    }
}