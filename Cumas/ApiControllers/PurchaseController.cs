using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    [RoutePrefix("api/purchase")]
    public class PurchaseController : BaseApi<Purchase>
    {
        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Reading purchase object. Username: {User.Identity.Name}");
                var raw = Repository.Query(new PurchaseFilter { Id = id })
                    .Include(z => z.Items)
                    .Include(x => x.Expenses)
                    .FirstOrDefault();
                if (raw == null) throw new Exception(ExceptionMessage.NotFound);
                var data = new
                {
                    raw.Id,
                    raw.TotalAmount,
                    raw.NetAmount,
                    raw.TaxAmount,
                    raw.AmountPaid,
                    raw.Tax,
                    raw.InvoiceNumber,
                    raw.Supplier,
                    raw.SupplierId,
                    raw.Store,
                    raw.StoreId,
                    raw.Date,
                    raw.ReceiptNumber,
                    raw.CreatedBy,
                    raw.ModifiedBy,
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        i.ProductId,
                        i.Unit,
                        Product = new { i.Product.Id, Name = $"{i.Product.Name} ({i.Scheme?.Package.Name ?? i.Product.Package.Name})", i.Product.Code },
                        i.Quantity,
                        i.Cost,
                        i.Desc
                    }),
                    Expenses = raw.Expenses.Select(x => new
                    {
                        x.Id,
                        x.TypeId,
                        Type = new { x.Type.Id, x.Type.Name },
                        x.Amount,
                        x.Notes
                    })
                };

                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;


        }

        [HttpPost]
        [Route("report")]
        public ResultObj Report(PurchaseFilter filter)
        {

            ResultObj results;
            try
            {
                Logger.Info($"Generating purchase report. Username: {User.Identity.Name}");
                var purchases = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/PurchaseReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);


                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");


                var totalAmount = 0;
                var items = string.Empty;
                var num = 0;
                foreach (var purchase in purchases)
                {
                    //Add details of purchase
                    totalAmount += (int)purchase.TotalAmount;
                    items += "<tr  style='border:none; border-bottom-style: double'>" +
                             $"<td colspan='6'>#{purchase.Id} &nbsp;&nbsp;&nbsp;" +
                             $"{purchase.Date.ToString("F")}&nbsp;&nbsp;&nbsp;" +
                             $"{purchase.Supplier?.Name}&nbsp;&nbsp;&nbsp;" +
                             $"{purchase.TotalAmount.ToString("##,###.00")}&nbsp;&nbsp;&nbsp;" +
                             $"{purchase.CreatedBy + "--"}&nbsp;&nbsp;&nbsp;" +
                             $"</td>" +
                             "</tr>";
                    foreach (var pitem in purchase.Items)
                    {
                        num++;
                        items += "<tr style='border:none;'>" +
                                $"<td>{num}</td>" +
                                $"<td>{pitem.Product.Name}</td>" +
                                $"<td>{pitem.Package.Name}</td>" +
                                $"<td>{pitem.Cost}</td>" +
                                $"<td>{pitem.Quantity}</td>" +
                                $"<td>{pitem.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                    }
                }
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));
                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Purchase Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(PurchaseFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying purchase. Username: {User.Identity.Name}");
                var raw = Repository.Query(filter);
                var data = raw.OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.ReceiptNumber,
                        x.Date,
                        x.Store,
                        x.StoreId,
                        x.Supplier,
                        x.SupplierId,
                        x.TotalAmount,
                        x.Balance,
                        x.CreatedBy,
                        x.CreatedAt,
                        x.ModifiedBy,
                        x.ModifiedAt,
                        x.InvoiceNumber,
                        x.TaxAmount,
                        x.NetAmount,
                        PoNumber = x.PurchaseOrderId.HasValue ? x.PurchaseOrder.Number : ""
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(Purchase record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new purchase. Username: {User.Identity.Name}");
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.CreatedBy = User.Identity.Name;
                record.ModifiedBy = User.Identity.Name;
                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null)
                        throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }

                new PurchaseRepository().Insert(record);

                //Update purchase order status to delivered
                if (record.PurchaseOrderId.HasValue)
                {
                    var poRepo = new PurchaseOrderRepository();
                    var po = poRepo.Get(record.PurchaseOrderId.Value);
                    po.Status = PurchaseOrderStatus.Delivered;
                    poRepo.Update(po);
                }


                results = WebHelpers.BuildResponse(record, "Purchase Saved.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(Purchase record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating purchase. Username: {User.Identity.Name}");
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.ModifiedBy = User.Identity.Name;
                new PurchaseRepository().Update(record);

                results = WebHelpers.BuildResponse(record, "Purchase Updated.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpGet]
        [Route("generateprint")]
        public ResultObj GeneratePrint(long id)
        {
            ResultObj results;
            try
            {
                var purchase = Repository.Get(id);
                if (purchase == null) throw new Exception(ExceptionMessage.NotFound);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/Purchase.html"));

                var storeInfo = QuickAccess.GetStoreInfo(purchase.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[DATE]", purchase.Date.ToString("D"));
                contents = contents.Replace("[PONUMBER]", purchase.PurchaseOrder?.Number);
                contents = contents.Replace("[INVOICENUMBER]", purchase.InvoiceNumber);
                contents = contents.Replace("[RECEIPTNUMBER]", purchase.ReceiptNumber);
                contents = contents.Replace("[SUPPLIERNAME]", purchase.Supplier.Name);
                contents = contents.Replace("[SUPPLIERADDRESS]", purchase.Supplier.PostalAddress);
                contents = contents.Replace("[SUPPLIERPHONENUMBER]", purchase.Supplier.PhoneNumber);
                //contents = contents.Replace("[STATUS]", purchase.Status.ToString());
                contents = contents.Replace("[PERSONNEL]", purchase.CreatedBy);
                contents = contents.Replace("[DELIVERYLOCATION]", purchase.Store.Name);
                contents = contents.Replace("[TAX]", $"({purchase.Tax * 100}%)");
                contents = contents.Replace("[TAXAMOUNT]", purchase.TaxAmount.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[NETAMOUNT]", purchase.NetAmount.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALAMOUNT]", purchase.TotalAmount.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[AMOUNTPAID]", purchase.AmountPaid.ToString(DefaultKeys.CurrencyFormat));

                var items = string.Empty;
                var num = 0;
                foreach (var item in purchase.Items)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{item.Product.Name} ({item.Scheme?.Package.Name ?? item.Product.Package.Name})</td>" +
                                $"<td>{item.Cost}</td>" +
                                $"<td>{item.Quantity}</td>" +
                                $"<td class='text-right'>{item.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Purchase Order", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Deleting purchase with id={id}. Username: {User.Identity.Name}");
                new PurchaseRepository().Delete(id);
                results = WebHelpers.BuildResponse(id, "Purchase Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        private const string NoItemsError = "No items found. Please add items before you can save.";
    }
}