﻿using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Extensions;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class SaleController : BaseApi<Sale>
    {
        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(new SaleFilter { Id = id }).Include(z => z.Items).Include(x => x.SaleReturns).FirstOrDefault();
                if (raw == null) throw new Exception(ExceptionMessage.NotFound);
                var data = new
                {
                    raw.Id,
                    raw.InvoiceNumber,
                    raw.Date,
                    raw.Customer,
                    raw.Buyer,
                    raw.StoreId,
                    raw.AmountPaid,
                    raw.Discount,
                    raw.TotalAmount,
                    raw.Tax,
                    raw.TaxAmount,
                    raw.TotalCost,
                    raw.CreatedBy,
                    raw.CreatedAt,
                    PaymentMode = raw.PaymentMode.ToString(),
                    Status = raw.Status.ToString(),
                    SaleReturns = raw.SaleReturns?.Select(i => new
                    {
                        i.Date,
                        i.Note,
                        i.Recepient,
                        items = i.Items.Select(x => new { x.Product, x.Quantity, x.Unit, x.Amount })
                    }),
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        i.Desc,
                        Product = new { i.Product.Id, i.Product.Name, i.Product.Price, i.PriceSold },
                        i.ProductId,
                        i.Quantity,
                        i.Amount
                    })
                };
                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("api/sale/getstocksold")]
        public ResultObj GetSalesStock(SaleStockParams filterParams)
        {
            ResultObj results;
            try
            {
                var stocklist = new BaseRepository<SaleItem>().Get(new SaleItemFilter
                {
                    StartDate = filterParams.Date,
                    EndDate = filterParams.Date,
                    StoreId = User.Identity.AsAppUser().StoreId
                });

                var data = (from stock in stocklist
                            group stock by new { stock.Product, stock.Scheme } into item
                            select new
                            {
                                Name = $"{item.Key.Product.Name} ({(item.Key.Scheme?.Package.Name ?? item.Key.Product.Package.Name)})",
                                SchemeId = item.Key.Scheme?.Id ?? 0,
                                Quantity = item.Sum(s => s.Quantity),
                                Unit = item.Key.Scheme?.Quantity ?? 1,
                                ProductId = item.Key.Product.Id
                            }).ToList();

                results = WebHelpers.BuildResponse(data, "", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/sale/closeperiod")]
        public ResultObj ClosePeriod()
        {
            ResultObj results;
            try
            {
                StoreKeeper.CloseSalesPeriod();
                results = WebHelpers.BuildResponse(null, "Sales Period Closed.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpGet]
        [Route("api/sale/getsaleinstance")]
        public ResultObj GetSaleInstance(long id)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(new SaleFilter { Id = id }).Include(z => z.Items).Include(x => x.SaleReturns).FirstOrDefault();
                if (raw == null) throw new Exception(ExceptionMessage.NotFound);
                var data = new
                {
                    raw.Id,
                    raw.InvoiceNumber,
                    raw.Date,
                    Customer = raw.CustomerId.HasValue ? new { raw.Customer.Id, raw.Customer.Name } : null,
                    raw.CustomerId,
                    raw.Buyer,
                    raw.StoreId,
                    raw.AmountPaid,
                    raw.Discount,
                    raw.TotalAmount,
                    raw.TotalCost,
                    raw.CreatedBy,
                    raw.CreatedAt,
                    raw.Status,
                    raw.ActualBalance,
                    raw.Balance,
                    raw.Paid,
                    raw.PaymentMode,
                    SaleReturns = raw.SaleReturns?.Select(i => new
                    {
                        i.Date,
                        i.Note,
                        i.Recepient,
                        i.CreatedBy,
                        items = i.Items.Select(x => new
                        {
                            Product = new
                            {
                                x.ProductId,
                                x.Product.Id,
                                x.Product.Name,
                                package = new
                                {
                                    x.Product.Package.Name
                                }
                            },
                            x.Quantity,
                            x.Unit,
                            x.Amount
                        })
                    }),
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        i.Desc,
                        i.ProductId,
                        Product = new
                        {
                            i.Product.Id,
                            i.Product.Name,
                            i.Product.Price,
                            i.PriceSold,
                            i.Product.MinPrice,
                            CurrentStock = StoreKeeper.GetCurrentStock(i.ProductId, raw.StoreId, i.Unit) + (i.Quantity * i.Unit),
                        },
                        i.Quantity,
                        i.Amount,
                        i.Discount,
                        i.Price,
                        i.PriceSold,
                        i.SaleId,
                        i.SchemeId,
                        i.Unit
                    })
                };
                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/sale/query")]
        public ResultObj Query(SaleFilter filter)
        {
            ResultObj results;
            try
            {
                if (!User.IsInRole(Privileges.SaleUnscoped)) filter.Personel = User.Identity.Name;
                var raw = Repository.Query(filter).Include(x => x.SaleReturns).Include(x => x.Store);
                var data = raw.OrderBy(x => x.Status).ThenByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.Customer,
                        x.Buyer,
                        x.StoreId,
                        x.InvoiceNumber,
                        x.AmountPaid,
                        x.Discount,
                        x.TotalAmount,
                        x.TotalCost,
                        x.Paid,
                        x.CreatedBy,
                        PaymentMode = x.PaymentMode.ToString(),
                        returns = x.SaleReturns.Count,
                        AmountDue = x.ActualBalance,
                        Status = x.Status.ToString(),
                        CanReturn = (x.Store.SaleReturnLimitDays.HasValue) && (SqlFunctions.DateDiff("day", DateTime.Today, x.Date) <= x.Store.SaleReturnLimitDays.Value) && (x.Status != SaleStatus.Pending)
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(Sale record)
        {
            ResultObj results;
            try
            {
                if (record.TotalCost <= 0) throw new Exception("Invalid Sale. Check sale details.");
                var repository = new SaleRepository();
                record.InvoiceNumber = ReferenceGenerator.SalesReceipt();
                repository.Insert(SetAudit(record, true));
                results = WebHelpers.BuildResponse(record, "New Sale Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(Sale record)
        {
            ResultObj results;
            try
            {
                if (record.TotalCost <= 0) throw new Exception("Invalid Sale. Check sale details.");
                var repository = new SaleRepository();
                repository.Update(SetAudit(record));
                results = WebHelpers.BuildResponse(record, $"Sale {record.InvoiceNumber} Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/sale/payment")]
        public virtual ResultObj MakePayment(SalePayment record)
        {
            ResultObj results;
            try
            {
                var repo = new SaleRepository();
                var sale = repo.Get(record.SaleId);
                if (sale == null) throw new Exception("Sale not found");
                sale.Status = (sale.CustomerId.HasValue && (record.AmountPaid < record.TotalAmount))
                ? SaleStatus.CreditSale
                : SaleStatus.Paid;

                sale.TaxAmount = record.TaxAmount;
                sale.Discount = record.Discount;
                sale.TotalAmount = record.TotalAmount;
                //sale.AmountPaid = (record.AmountPaid > record.TotalAmount) ? record.TotalAmount : record.AmountPaid;
                sale.AmountPaid = record.AmountPaid;
                var balance = sale.TotalAmount - sale.AmountPaid;
                sale.Balance = (balance < 0) ? 0 : balance;
                sale.Paid = (sale.Status == SaleStatus.Paid);
                sale.PaymentMode = record.PaymentMode;
                sale.TransactionCode = record.TransactionCode;

                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }
                sale.BankAccountId = record.BankAccountId;

                repo.UpdatePayment(sale);

                results = WebHelpers.BuildResponse(sale, "Payment Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/sale/saveAll")]
        public virtual ResultObj SaveAll(Sale record)
        {
            ResultObj results;
            try
            {
                var repository = new SaleRepository();
                var sale  = repository.SaveAll(SetAudit(record, true));
                results = WebHelpers.BuildResponse(sale, "Sale Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/sale/saveandmakepayment")]
        public ResultObj SaveAndMakePayment(Sale record, SalePayment salePayment)
        {
            ResultObj results;
            try
            {
                if (record.TotalCost <= 0) throw new Exception("Invalid Sale. Check sale details.");
                var repository = new SaleRepository();
                record.InvoiceNumber = ReferenceGenerator.SalesReceipt();
                repository.Insert(SetAudit(record, true));
                //results = WebHelpers.BuildResponse(record, "New Sale Saved Successfully.", true, 1);

                var repo = new SaleRepository();
                //Get pointer to the sale
                var sale = repo.Get(record.Id);
                if (sale == null) throw new Exception("Sale not found");
                sale.Status = (sale.CustomerId.HasValue && (record.AmountPaid < salePayment.TotalAmount))
                ? SaleStatus.CreditSale
                : SaleStatus.Paid;

                var paymentRepo = new SalePayment();
                sale.TaxAmount = salePayment.TaxAmount;
                sale.Discount = salePayment.Discount;
                sale.TotalAmount = salePayment.TotalAmount;
                //sale.AmountPaid = (salePayment.AmountPaid > salePayment.TotalAmount) ? salePayment.TotalAmount : salePayment.AmountPaid;
                sale.AmountPaid = salePayment.AmountPaid;
                var balance = sale.TotalAmount - sale.AmountPaid;
                sale.Balance = (balance < 0) ? 0 : balance;
                sale.Paid = (sale.Status == SaleStatus.Paid);
                sale.PaymentMode = salePayment.PaymentMode;
                sale.TransactionCode = salePayment.TransactionCode;

                if (salePayment.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    salePayment.BankAccountId = chequeAccount.Id;
                }
                sale.BankAccountId = salePayment.BankAccountId;

                repo.UpdatePayment(sale);

                results = WebHelpers.BuildResponse(sale, "Payment Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                new SaleRepository().Delete(id);
                results = WebHelpers.BuildResponse(id, "Sale Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        [HttpPost]
        [Route("api/sale/generateproformainvoice")]
        public ResultObj GenerateProformaInvoice(Sale sale)
        {

            ResultObj results;
            try
            {
                if (sale == null) throw new Exception(ExceptionMessage.NotFound);

                sale.Customer = new BaseRepository<Customer>().Get(sale.CustomerId.GetValueOrDefault());
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/Proforma.html"));

                var storeInfo = QuickAccess.GetStoreInfo(sale.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                //contents = contents.Replace("[DATE]", sale.Date.ToString("f"));
                contents = contents.Replace("[DATE]", $"{sale.Date.ToShortDateString()} {sale.Date.ToShortTimeString()}");
                contents = contents.Replace("[CUSTOMER]", sale.CustomerId.HasValue ? sale.Customer.Name : "Walk-In");
                contents = contents.Replace("[TOTALCOST]", sale.TotalCost.ToString("##,###.00"));
                contents = contents.Replace("[TOTALAMOUNT]", sale.TotalAmount.ToString("##,###.00"));


                var items = string.Empty;
                var num = 0;
                foreach (var item in sale.Items)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{item.Product.Name}</td>" +
                                $"<td>{item.Quantity}</td>" +
                                $"<td class='text-right'>{(item.PriceSold * item.Quantity).ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A5);
                results = WebHelpers.BuildResponse(pdfOutput, "Pro Forma Invoice", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpGet]
        [Route("api/sale/generateinvoice")]
        public ResultObj GenerateInvoice(long id, bool receipt)
        {
            ResultObj results;
            try
            {
                var invoice = Repository.Query(new SaleFilter { Id = id }).Include(z => z.Items).FirstOrDefault();
                if (invoice == null) throw new Exception(ExceptionMessage.NotFound);

                var storeInfo = QuickAccess.GetStoreInfo(invoice.StoreId);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(receipt ? @"~/ReportTemplates/InvoiceReceiptV2.html" : @"~/ReportTemplates/Invoice.html"));


                //var vatRate = decimal.Parse(new AppSettingRepository().Get(ConfigKeys.Vat).Notes);
                var balance = invoice.TotalAmount - invoice.AmountPaid;
                //var vat = invoice.TotalAmount * vatRate;
                //var minusVat = invoice.TotalAmount - vat;
                var user = new UserRepository().Get(invoice.CreatedBy);
                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[FOOTER]", storeInfo.FooterNote);
                //contents = contents.Replace("[DATE]", invoice.Date.ToString("f"));
                contents = contents.Replace("[DATE]", $"{invoice.Date.ToShortDateString()} {invoice.Date.ToShortTimeString()}");
                contents = contents.Replace("[INVOICENUMBER]", invoice.InvoiceNumber);
                contents = contents.Replace("[PAYMENTMODE]", invoice.PaymentMode.ToString());
                contents = contents.Replace("[SALETYPE]", invoice.Status.ToString() == "CreditSale" ? "Credit" : "Cash ");
                contents = contents.Replace("[TRANSACTIONCODE]", invoice.TransactionCode);
                contents = contents.Replace("[CUSTOMER]", invoice.CustomerId.HasValue ? invoice.Customer.Name : "Walk-In");
                contents = contents.Replace("[BUYER]", invoice.Buyer);
                contents = contents.Replace("[PERSONNEL]", user?.Name ?? invoice.CreatedBy);
                contents = contents.Replace("[SHOWVAT]", storeInfo.ShowVat ? $"<b> Sub Total:</b> {invoice.TotalCost}<br/><b> VAT + NHIL:</b> [VAT] <br/>" : "");
                contents = contents.Replace("[VAT]", invoice.TaxAmount.ToString("##,###.00"));
                //contents = contents.Replace("[MINUSVAT]", minusVat.ToString("##,###.00"));
                contents = contents.Replace("[DISCOUNT]", invoice.Discount.ToString("##,###.00"));
                contents = contents.Replace("[TOTALCOST]", invoice.TotalCost.ToString("##,###.00"));
                contents = contents.Replace("[TOTALAMOUNT]", invoice.TotalAmount.ToString("##,###.00"));
                contents = contents.Replace("[TOTALPAID]", invoice.AmountPaid.ToString("##,###.00"));
                contents = contents.Replace("[BALNAME]", balance > 0 ? "Balance" : "Change");
                balance = balance < 0 ? -(balance) : balance;
                contents = contents.Replace("[BALANCE]", balance.ToString("##,###.00"));



                var items = string.Empty;
                var num = 0;
                foreach (var item in invoice.Items)
                {
                    num++;
                    items +=
                        $"- {item.Desc} <br/> &nbsp;&nbsp;&nbsp;{item.Quantity} @ {item.PriceSold}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {item.Amount.ToString("##,###.00")}<br/>";
                    /* items += "<tr>" +
                                 $"<td>{num}</td>" +
                                 $"<td>{item.Desc}</td>" +
                                 $"<td>{item.Quantity}</td>" +
                                 $"<td class='text-right'>{item.Amount.ToString("##,###.00")}</td>" +
                              "</tr>";*/
                }

                contents = contents.Replace("[ITEMS]", items);


                var barcode = Convert.ToBase64String(new BarcodeGenerator().Generate(invoice.InvoiceNumber));
                contents = contents.Replace("[BARCODE]", barcode);

                var pdfOutput = storeInfo.PrinterSet == PrinterSet.POSPrinter
                    ? Reporter.GeneratePdf(contents, 250, 360, 8)
                    : Reporter.GeneratePdf(contents, PageSize.A5);
                results = WebHelpers.BuildResponse(pdfOutput, "Sale Invoice", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/sale/report")]
        public ResultObj Report(SaleFilter filter)
        {

            ResultObj results;
            try
            {
                var sales = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/SalesReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var totalAmount = sales.Sum(x => x.TotalAmount);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var sale in sales)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{sale.Date.ToString("F")}</td>" +
                                $"<td>{sale.Customer?.Name ?? "Walk-In"}</td>" +
                                $"<td>{sale.Buyer}</td>" +
                                $"<td class='text-right'>{sale.TotalCost.ToString("##,###.00")}</td>" +
                                $"<td class='text-right'>{sale.Discount.ToString("##,###.00")}</td>" +
                                $"<td class='text-right'>{sale.TotalAmount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Sales Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/sale/dailyreport")]
        public ResultObj DailyReport(DailySaleSummaryFilter filter)
        {

            ResultObj results;
            try
            {
                var summaries = new BaseRepository<DailySaleSummary>().Get(filter);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/DailySalesReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");

                var items = string.Empty;
                var num = 0;
                foreach (var summary in summaries)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{summary.Date.ToString("D")}</td>" +
                                $"<td>{summary.Store.Name}</td>" +
                                $"<td class='text-right'>{summary.AmountExVat.ToString("##,###.00")}</td>" +
                                $"<td class='text-right'>{summary.VatAmount.ToString("##,###.00")}</td>" +
                                $"<td class='text-right'>{summary.SaleValue.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[TOTALNETAMOUNT]", summaries.Sum(q => q.AmountExVat).ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALVATAMOUNT]", summaries.Sum(q => q.VatAmount).ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALSALEAMOUNT]", summaries.Sum(q => q.SaleValue).ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Sales Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        private static string BestFitPrice(decimal price, decimal minPrice)
        {
            return price == minPrice ? price.ToString("##,###.00") : $"{price} - {minPrice}";
        }

        [HttpPost]
        [Route("api/sale/productsalereport")]
        public ResultObj ProductReport(SaleFilter filter)
        {
            ResultObj results;
            try
            {
                filter.StartDate = filter.StartDate ?? DateTime.Today;

                var raw = new BaseRepository<Sale>().Query(filter)
                    .SelectMany(x => x.Items);
                if (filter.TypeId > 0) raw = raw.Where(q => q.Product.TypeId == filter.TypeId);
                var products = raw.GroupBy(x => new { x.Product, x.Scheme })
                    .ToList()
                    .Select(x => new
                    {
                        Product = $"{x.Key.Product.Name} ({x.Key.Scheme?.Package.Name ?? x.Key.Product.Package.Name}{(!string.IsNullOrEmpty(x.Key.Scheme?.Desc) ? $" - {x.Key.Scheme.Desc}" : "")})",
                        Quantity = x.Sum(q => q.Quantity),
                        UnitCost = x.Key.Scheme?.Cost ?? x.Key.Product.Cost,
                        CostPrice = (x.Key.Scheme?.Cost ?? x.Key.Product.Cost) * x.Sum(q => q.Quantity),
                        UnitPrice = BestFitPrice(x.Key.Scheme?.Price ?? x.Key.Product.Price, x.Key.Scheme?.MinPrice ?? x.Key.Product.MinPrice),
                        Price = x.Sum(q => q.Amount),
                        Margin = 0
                    }).ToList();

                var items = "";
                var num = 0;
                foreach (var item in products)
                {
                    num++;
                    var itemMargin = item.Price - (item.UnitCost * item.Quantity);
                    var marginPercentage = (item.CostPrice > 0) ? ((itemMargin / item.CostPrice)).ToString("0.00%") : "-";
                    items += "<tr>" +
                            $"<td>{num}</td>" +
                            $"<td>{item.Product}</td>" +
                            $"<td class='text-center'>{item.Quantity}</td>" +
                            $"<td class='amt'>{item.UnitCost.ToString("##,###.00")}</td>" +
                            $"<td class='amt'>{item.CostPrice.ToString("##,###.00")}</td>" +
                            $"<td class='amt'>{item.UnitPrice}</td>" +
                            $"<td class='amt'>{item.Price.ToString("##,###.00")}</td>" +
                            $"<td class='amt'>{itemMargin.ToString("##,###.00")}</td>" +
                            $"<td class='amt'>{marginPercentage}</td>" +
                         "</tr>";
                }

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/ProductSaleReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D"));
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D"));

                var margin = products.Sum(q => (q.Price - (q.UnitCost * q.Quantity)));
                var cp = products.Sum(q => q.CostPrice);

                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[COSTPRICE]", cp.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[PRICE]", products.Sum(q => q.Price).ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[MARGIN]", margin.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[MARGINP]", (cp > 0) ? (margin / cp).ToString("0.00%") : "-");

                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Sales Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/sale/report_detail")]
        public ResultObj DetailReport(SaleFilter filter)
        {

            ResultObj results;
            try
            {
                var sales = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/SalesReportDetail.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);


                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");


                var totalAmount = sales.Sum(x => x.TotalAmount);
                var items = string.Empty;
                var num = 0;

                foreach (var sale in sales)
                {
                    //Add details of purchase
                    //totalAmount += (int)sale.TotalAmount;
                    //if (sitem.ProductId != filter.ProductId) continue;
                    items += "<tr  style='border:none; border-bottom-style: double'>" +
                             $"<td>{sale.Id}</td>" +
                             $"<td>{sale.Date.ToString("F")}</td>" +
                             $"<td>{sale.InvoiceNumber}</td>" +
                             $"<td>{sale.Customer?.Name}</td>" +
                             $"<td>{sale.Buyer ?? "--"}</td>" +
                             $"<td>{sale.TotalAmount.ToString("##,###.00")}</td>" +
                             "</tr>";
                    foreach (var sitem in sale.Items)
                    {
                        num++;
                        if (sitem.ProductId != filter.ProductId) continue;

                        items += "<tr style='border:none;'>" +
                                $"<td>{num}</td>" +
                                $"<td>{sitem.Product.Name}</td>" +
                                $"<td>{sitem.PriceSold}</td>" +
                                $"<td>{sitem.Quantity}</td>" +
                                $"<td>{sitem.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                    }
                }

                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));

                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4, true);
                results = WebHelpers.BuildResponse(pdfOutput, "Purchase Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/sale/fixsalesummaries")]
        public ResultObj FixSaleSummaries(SummaryConfig config)
        {
            ResultObj results;
            try
            {
                //new SaleRepository().CloseSalePeriod(new DateTime(2016, 01, 01), DateTime.Today, 17.5m);
                new SaleRepository().CloseSalePeriod(config.StartDate, config.EndDate, config.Rate);
                results = WebHelpers.BuildResponse("Done closing all sales periods.", "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }
    }

    public class SummaryConfig
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Rate { get; set; }
    }

    public class SaleStockParams
    {
        public DateTime Date { get; set; }
        public long? PeriodId { get; set; }
    }

    public class SalePayment
    {
        public long SaleId { get; set; }
        public decimal Discount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Balance { get; set; }
        public long BankAccountId { get; set; }
        public string TransactionCode { get; set; }
        public PaymentMode PaymentMode { get; set; }
    }

}