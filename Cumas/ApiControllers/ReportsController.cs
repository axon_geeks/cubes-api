﻿using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class ReportsController : ApiController
    {
        [Route("api/reports/profitandloss")]
        public ResultObj ProfitAndLoss(ReportFilter filter)
        {
            ResultObj results;
            try
            {
                var expenseRepo = new BaseRepository<Expense>();
                var purchaseRepo = new PurchaseRepository();
                var saleRepo = new SaleRepository();

                var sales = saleRepo.Query(new SaleFilter
                {
                    StoreId = filter.StoreId,
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate
                }).ToList().Sum(q => q.TotalAmount);

                var openingStock = QuickAccess.GetStockLevel(filter.StoreId, filter.StartDate, filter.EndDate);
                var closingStock = QuickAccess.GetStockLevel(filter.StoreId, filter.EndDate, DateTime.UtcNow);


                var purchasesTransactions = purchaseRepo.Query(new PurchaseFilter
                {
                    StoreId = filter.StoreId,
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate
                });

                var purchases = purchasesTransactions.Any() ? purchasesTransactions.Sum(q => q.TotalAmount) : 0;

                var carriageInwardsTransactions = expenseRepo.Query(new ExpenseFilter
                {
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate,
                    StoreId = filter.StoreId,
                    CarriageInwards = true
                });

                var carriageInwards = carriageInwardsTransactions.Any()
                    ? carriageInwardsTransactions.Sum(q => q.Amount)
                    : 0;

                var totalPurchase = purchases + carriageInwards;
                var cogas = openingStock + totalPurchase;


                var costofSales = cogas - closingStock;

                var gross = sales - costofSales;
                var expenses = expenseRepo.Query(new ExpenseFilter
                {
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate,
                    StoreId = filter.StoreId,
                    CarriageInwards = false
                }).GroupBy(x => x.Type).Select(x => new
                {
                    x.Key.Name,
                    Amount = x.Sum(q => q.Amount)
                }).ToList();

                var totalExpense = expenses.Any() ? expenses.Sum(q => q.Amount) : 0;
                var net = gross - totalExpense;

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/ProfitAndLoss.html"));

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[SALES]", sales.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[OPENINGSTOCK]", openingStock.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[PURCHASES]", purchases.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[CARRAIAGEINWARDS]", carriageInwards.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALPURCHASE]", totalPurchase.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[COGAS]", cogas.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[CLOSINGSTOCK]", closingStock.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[COSTOFSALES]", costofSales.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[GROSS]", gross.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALEXPENSES]", totalExpense.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[NET]", net.ToString(DefaultKeys.FormatAmount));

                var items = expenses.Aggregate(string.Empty, (current, expense) => current + ("<tr>" + $"<td class='indent'>{expense.Name}</td>" + $"<td class='amt'>{expense.Amount.ToString("##,###.00")}</td>" + "</tr>"));

                contents = contents.Replace("[EXPENSES]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Profit and Loss Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [Route("api/reports/profitandlosssimple")]
        public ResultObj ProfitAndLossSimple(ReportFilter filter)
        {
            ResultObj results;
            try
            {
                var expenseRepo = new BaseRepository<Expense>();
                var purchaseRepo = new PurchaseRepository();
                var saleRepo = new SaleRepository();

                var totalSales = saleRepo.Query(new SaleFilter
                {
                    StoreId = filter.StoreId,
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate
                }).ToList().Sum(q => q.TotalAmount);

                var itemsSold = new BaseRepository<SaleItem>().Query(new SaleItemFilter
                {
                    StoreId = filter.StoreId,
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate
                });

                decimal costofSales = 0;
                var purchaseItemRepo = new BaseRepository<PurchaseItem>();
                foreach (var item in itemsSold)
                {
                    var purchaseCost = purchaseItemRepo.Query(new PurchaseItemFilter
                    {
                        ProductId = item.ProductId,
                        SchemeId = item.SchemeId ?? 0
                    }).OrderByDescending(x => x.Purchase.Date).FirstOrDefault()?.Cost;
                    costofSales += purchaseCost ?? 0;
                }

                var expenses = expenseRepo.Query(new ExpenseFilter
                {
                    StartDate = filter.StartDate,
                    EndDate = filter.EndDate,
                    StoreId = filter.StoreId,
                    CarriageInwards = false
                }).GroupBy(x => x.Type).Select(x => new
                {
                    x.Key.Name,
                    Amount = x.Sum(q => q.Amount)
                }).ToList();

                var totalExpense = expenses.Any() ? expenses.Sum(q => q.Amount) : 0;

                var gross = totalSales - costofSales;
                var net = totalSales - (totalExpense + costofSales);


                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/ProfitAndLossSimple.html"));

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[SALES]", totalSales.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[COSTOFSALES]", costofSales.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[TOTALEXPENSES]", totalExpense.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[GROSS]", gross.ToString(DefaultKeys.FormatAmount));
                contents = contents.Replace("[NET]", net.ToString(DefaultKeys.FormatAmount));

                var items = expenses.Aggregate(string.Empty, (current, expense) => current + ("<tr>" + $"<td class='indent'>{expense.Name}</td>" + $"<td class='amt'>{expense.Amount.ToString("##,###.00")}</td>" + "</tr>"));

                contents = contents.Replace("[EXPENSES]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Profit and Loss Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [Route("api/reports/salesperiod")]
        public ResultObj SalesPeriod(StockDiaryFilter filter)
        {
            ResultObj results;
            try
            {
                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/SalesPeriod.html"));

                var diary = new BaseRepository<StockDiary>().Get(filter).GroupBy(x => x.Date);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");

                var items = "";
                foreach (var record in diary)
                {
                    items += $"<tr><td colspan='9'>{record.Key.ToString("D")}</td></tr>";
                    items += record.Aggregate(string.Empty,
                        (current, x) => current + ($"<tr><td>{x.Stock.Product.Name}</td>" +
                        $"<td class='amt'>{x.OpeningStock}</td><td class='amt'>{x.QuantityPurchases}</td>" +
                        $"<td class='amt'>{x.OpeningStock + x.QuantityPurchases}</td>" +
                        $"<td class='amt'>{x.QuantitySold}</td><td class='amt'>{x.AmountSold}</td>" +
                        $"<td class='amt'>{x.ClosingStock}</td><td class='amt'>{x.CreditedStock}</td>" +
                        $"<td class='amt'>{x.ClosingStock + x.CreditedStock}</td>" +
                        "</tr>"));
                }

                contents = contents.Replace("[ITEMS]", items);

                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4, true);
                results = WebHelpers.BuildResponse(pdfOutput, "Sales Period Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

    }

}
