﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using Microsoft.Ajax.Utilities;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class CustomerAccountController : BaseApi<CustomerAccount>
    {
        [HttpPost]
        [Route("api/customeraccount/query")]
        public ResultObj Query(CustomerAccountFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Quering customer account. Username: {User.Identity.Name}");
                var raw = Repository.Get(filter);
                var data = raw.OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .ToList();
                results = WebHelpers.BuildResponse(data, data.Sum(q=>q.Credit - q.Debit).ToString("##,###.00"), true, raw.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/customeraccount/report")]
        public ResultObj Report(CustomerAccountFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Generating customer account report. Username: {User.Identity.Name}");
                if (filter.CustomerId == 0) throw new Exception("Please select a customer");
                
                var transactions = Repository.Query(filter).Include(x=>x.Customer).ToList();
                if (transactions.Count < 1) throw new Exception("No transaction found for Customer");

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/CustomerAccounts.html"));

                var storeInfo = QuickAccess.GetStoreInfo(0);
         
                contents = contents.Replace("[CUSTOMERNAME]", transactions[0].Customer.Name);
                contents = contents.Replace("[CUSTOMERPHONE]", transactions[0].Customer.PhoneNumber);
                contents = contents.Replace("[CUSTOMEREMAIL]", transactions[0].Customer.Email);
                contents = contents.Replace("[ACCOUNTBALANCE]", transactions[0].Customer.TotalDebt.ToString(DefaultKeys.CurrencyFormat));

                var totalDebit = transactions.Sum(x => x.Debit);
                var creditDebit = transactions.Sum(x => x.Credit);
                contents = contents.Replace("[TOTALDEBIT]", totalDebit.ToString(DefaultKeys.CurrencyFormat) ?? "");
                contents = contents.Replace("[TOTALCREDIT]", creditDebit.ToString(DefaultKeys.CurrencyFormat) ?? "");


                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);

                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
            
                var items = string.Empty;
                var num = 0;
                foreach (var transaction in transactions)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{transaction.Date.ToString("F")}</td>" +
                                $"<td>{transaction.Notes}</td>" +
                                $"<td>{(transaction.Debit > 0 ? transaction.Debit.ToString(DefaultKeys.CurrencyFormat) : "-")}</td>" +
                                $"<td>{(transaction.Credit > 0 ? transaction.Credit.ToString(DefaultKeys.CurrencyFormat) : "-")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Customer Transaction Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/customeraccount/debtors")]
        public ResultObj Debtor(CustomerAccountFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Generating debtors report. Username: {User.Identity.Name}");
                var transactions = Repository.Query(filter).Include(x => x.Customer).DistinctBy(c=>c.CustomerId).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/DebtorsReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(0);

                DateTime today = DateTime.Today;

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
               
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");

                contents = contents.Replace("[TODAY]", today.ToString("D") ?? "");

                var items = string.Empty;
                var num = 0;
                decimal totalDebt = 0;
                foreach (var transaction in transactions.Where(transaction => transaction.Customer.TotalDebt > 0))
                {
                    num++;
                    items += "<tr>" +
                             $"<td>{num}</td>" +
                             $"<td>{transaction.Customer.Name}</td>" +
                             $"<td>{transaction.Customer.PhoneNumber}</td>" +
                             $"<td>{transaction.Customer.Email}</td>" +
                             $"<td align='right'>{transaction.Customer.TotalDebt.ToString(DefaultKeys.CurrencyFormat)}</td>" +
                             "</tr>";
                    totalDebt += transaction.Customer.TotalDebt;
                }

                contents = contents.Replace("[TOTALAMOUNT]", totalDebt.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[ITEMS]", items);

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Debtors' Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

    }
}