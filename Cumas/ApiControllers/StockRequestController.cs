using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Extensions;
using Cumas.LogicUnit;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class StockRequestController : BaseApi<StockRequest>
    {
        [HttpPost]
        [Route("api/stockrequest/generate")]
        public ResultObj Generate(List<Stock> items)
        {
            ResultObj results;
            try
            {
                var request = new StockRequest
                {
                    Date = DateTime.UtcNow,
                    StoreId = User.Identity.AsAppUser().StoreId,
                    Number = ReferenceGenerator.StockRequestNumber(),
                    Items = items.Select(x => new StockRequestItem
                    {
                        ProductId = x.ProductId,
                        Quantity = x.Quantity,
                        Unit = x.Unit,
                        SchemeId = x.SchemeId
                    }).ToList(),
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    CreatedBy = User.Identity.Name,
                    ModifiedBy = User.Identity.Name
                };

                new StockRequestRepository().Insert(request);

                results = WebHelpers.BuildResponse(true, "Generated stock request.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/stockrequest/quick")]
        public ResultObj Search(string q)
        {
            q = q.ToLower();
            var data = Repository.Query(new StockRequestFilter { Status = RequestStatus.Approved })
                .Include(x => x.Items)
                .Where(a => a.Store.Name.ToLower().Contains(q) ||
                            a.CreatedBy.ToLower().Contains(q) ||
                            a.Number.ToLower().Contains(q))
                .OrderByDescending(x => x.Date)
                .ToList()
                .Select(x => new
                {
                    x.Id,
                    x.Number,
                    x.Date,
                    x.Store,
                    x.StoreId,
                    Status = x.Status.ToString(),
                    x.CreatedBy,
                    x.CreatedAt,
                    x.ModifiedBy,
                    x.ModifiedAt,
                    Items = x.Items.Select(i => new
                    {
                        i.Id,
                        i.ProductId,
                        Product = new
                        {
                            i.Product.Id,
                            Name = $"{i.Product.Name} ({i.Scheme?.Package.Name ?? i.Product.Package.Name})"
                        },
                        i.SchemeId,
                        Unit = i.Scheme?.Quantity ?? 1,
                        i.Quantity,
                        i.Remarks
                    })
                }).ToList();
            return WebHelpers.BuildResponse(data, "Seacrh Results", true, data.Count);
        }

        [HttpPost]
        [Route("api/stockrequest/query")]
        public ResultObj Query(StockRequestFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(filter);
                var data = raw.Include(x => x.Items).OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Number,
                        x.Date,
                        x.Store,
                        x.StoreId,
                        Status = x.Status.ToString(),
                        x.CreatedBy,
                        x.CreatedAt,
                        x.ModifiedBy,
                        x.ModifiedAt,
                        Items = x.Items.Select(i => new
                        {
                            i.Id,
                            i.ProductId,
                            Product = new
                            {
                                i.Product.Id,
                                Name = $"{i.Product.Name} ({i.Scheme?.Package.Name ?? i.Product.Package.Name})",
                                i.SchemeId
                            },
                            i.SchemeId,
                            i.Quantity,
                            i.Remarks
                        })
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(StockRequest record)
        {
            ResultObj results;
            try
            {
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.Number = ReferenceGenerator.StockRequestNumber();
                record.CreatedBy = User.Identity.Name;
                record.ModifiedBy = User.Identity.Name;
                new StockRequestRepository().Insert(record);

                results = WebHelpers.BuildResponse(record, "Stock Request Sent.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(StockRequest record)
        {
            ResultObj results;
            try
            {
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.ModifiedBy = User.Identity.Name;
                new StockRequestRepository().Update(record);

                results = WebHelpers.BuildResponse(record, "Stock Request Updated.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        private const string NoItemsError = "No items found. Please add items before you can save.";
    }
}