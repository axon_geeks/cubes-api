using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Windows.Forms;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class StockController : BaseApi<Stock>
    {
        [HttpGet]
        [Route("api/stock/availablestock")]
        public ResultObj AvailableStock()
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("User not found.");

                var stocks = Repository.Query(new StockFilter { StoreId = user.StoreId, IsAvailable = true })
                    .OrderBy(x=>x.Product.Name)
                    .Include(x => x.Product.PackageSchemes).ToList();
                var data = new List<ProductUnit>();
                foreach (var stock in stocks)
                {
                    data.Add(new ProductUnit
                    {
                        Id = stock.ProductId,
                        Code = stock.Product.Code ?? "",
                        Number = stock.Product.Number ?? "",
                        //Name = $"{stock.Product.Name} ({stock.Product?.Package?.Name}) {stock.Product?.Number}",
                        Name = $"{stock.Product.Name} ({stock.Product?.Package?.Name})",
                        MinPrice = stock.Product.MinPrice,
                        Price = stock.Product.Price,
                        Unit = 1,
                        CurrentStock = stock.CurrentStock
                    });

                    stock.Product.PackageSchemes?.ForEach(q => data.Add(new ProductUnit
                    {
                        Id = stock.ProductId,
                        Code = q.Barcode ?? "",
                        Number = q.Product.Number ?? "",
                        Name = $"{stock.Product.Name} ({q.Package?.Name}{(!string.IsNullOrEmpty(q.Desc) ? $" - {q.Desc}" : "")})",
                        MinPrice = q.MinPrice,
                        Price = q.Price,
                        Unit = q.Quantity,
                        CurrentStock = (stock.CurrentStock / ((q.Quantity < 1) ? 1 : q.Quantity)),
                        SchemeId = q.Id
                    }));
                }

                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/stock/updatebatch")]
        public ResultObj Updatebatch(List<Stock> records)
        {
            ResultObj results;
            try
            {
                Repository.BulkSave(records);
                results = WebHelpers.BuildResponse(records, "Stock Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/stock/updatestock")]
        public ResultObj UpdatePrice(Stock record)
        {
            ResultObj results;
            try
            {
                var theStock = Repository.Get(record.Id);
                theStock.Price = record.Price;
                Repository.Update(theStock);

                results = WebHelpers.BuildResponse(record, "Stock Price Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/stock/report")]
        public ResultObj Report(StockFilter filter)
        {
            ResultObj results;
            try
            {
                var stocks = Repository.Query(filter).Select(x => new
                {
                    x.Product.Id,
                    x.Product.Code,
                    x.Product.Name,
                    Package = x.Product.Package.Name,
                    x.Product.Price,
                    x.Product.ReorderLevel,
                    // Price = (x.Price > 0 ? x.Price : x.Product.Price),
                    x.CurrentStock
                }).ToList();

                //var totalAmount = stocks.Sum(x => (x.Price * x.CurrentStock));
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/StockSummaryReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);


                var items = string.Empty;
                var num = 0;
                var totalStockValue = 0;
                foreach (var stock in stocks)
                {
                    num++;
                    var totalSellingPrice = stock.Price * stock.CurrentStock;
                    totalStockValue += (int)totalSellingPrice;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{stock.Name}</td>" +
                                $"<td>{stock.CurrentStock} &nbsp;&nbsp;&nbsp;{stock.Package}</td>" +
                                $"<td>{stock.ReorderLevel}</td>" +
                                $"<td align='right'>{stock.Price}</td>" +
                                $"<td align='right'>{totalSellingPrice}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[TOTALAMOUNT]", totalStockValue.ToString(DefaultKeys.CurrencyFormat));
                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4, true);
                results = WebHelpers.BuildResponse(pdfOutput, "Stock Summary Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/stock/allstock")]
        public ResultObj AllStock(StockFilter filter)
        {
            ResultObj results;
            try
            {
                var inStocks = Repository.Query(filter).Select(x => new
                {
                    x.Product.Id,
                    x.Product.Name,
                    Package = x.Product.Package.Name,
                    x.Price,
                    x.CurrentStock,
                }).ToList();

                var inStocksIds = inStocks.Select(x => x.Id).ToList();
                var sales = new BaseRepository<SaleItem>();

                var outStock = sales.Query(new SaleItemFilter { Status = SaleStatus.CreditSale, Paid = false, StoreId = filter.StoreId }).Include(y => y.Sale).GroupBy(x => x.Product).Select(x => new
                {
                    x.Key.Id,
                    x.Key.Name,
                    Package = x.Key.Package.Name,
                    QuantityOut = x.Sum(i => i.Quantity)
                }).ToList();

                var allStock = inStocks.Join(outStock, ins => ins.Id, outs => outs.Id,
                    (ins, outs) => new
                    {
                        ins.Id,
                        ins.Name,
                        ins.Package,
                        ins.CurrentStock,
                        ins.Price,
                        outs.QuantityOut,
                        total = ins.CurrentStock + outs.QuantityOut
                    }).ToList();

                // ID, Product, Package, Current Stock, Quantity Out, Total Stock

                //var totalAmount = stocks.Sum(x => (x.Price * x.CurrentStock));
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/CurrentStockReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);


                var items = string.Empty;
                var num = 0;
                var totalStockValue = 0;

                foreach (var stock in allStock)
                {
                    num++;
                    //totalStockValue += (int)totalSellingPrice;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{stock.Name}</td>" +
                                $"<td>{stock.CurrentStock} &nbsp;&nbsp;&nbsp;{stock.Package}</td>" +
                                $"<td>{stock.QuantityOut} &nbsp;&nbsp;&nbsp;{stock.Package}</td>" +
                                $"<td>{stock.total} &nbsp;&nbsp;&nbsp;{stock.Package}</td>" +
                             /* $"<td align='right'>{stock.Price}</td>" +
                              $"<td align='right'>{stock.Price * stock.total}</td>" +*/
                             "</tr>";
                }
                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                contents = contents.Replace("[TOTALAMOUNT]", totalStockValue.ToString(DefaultKeys.CurrencyFormat));
                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4, true);
                results = WebHelpers.BuildResponse(pdfOutput, "Stock Summary Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;

        }

        [HttpPost]
        [Route("api/stock/updatestocklevel")]
        public ResultObj UpdateStockLevel(Stock record)
        {
            ResultObj results;
            try
            {
                var theStock = Repository.Get(record.Id);
                theStock.CurrentStock = record.CurrentStock;
                Repository.Update(theStock);

                results = WebHelpers.BuildResponse(record, "Stock Level Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/stock/query")]
        public ResultObj Query(StockFilter filter)
        {
            ResultObj results;
            try
            {
                if (filter.StoreId == 0) throw new Exception("Select store to view stock levels.");
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.Id)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).Select(x => new
                    {
                        x.Id,
                        x.CurrentStock,
                        x.MaximumStock,
                        Price = x.Price > 0 ? x.Price : x.Product.Price,
                        x.Reorder,
                        Product = new
                        {
                            x.Product.Id,
                            x.Product.TypeId,
                            x.Product.CurrentStock,
                            x.Product.MaximumStock,
                            x.Product.Name,
                            x.Product.Code,
                            x.Product.Price,
                            x.Product.Type,
                            x.Product.ReorderLevel,
                            Categories = x.Product.Categories.Select(c => new
                            {
                                c.Id,
                                c.Name,
                                c.ParentId
                            })
                        }
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/stock/tally")]
        public ResultObj GetStockTally(StockTallyFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = new BaseRepository<StockTally>().Query(filter).ToList();
                results = WebHelpers.BuildResponse(raw, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }


        [HttpPost]
        [Route("api/stock/levels")]
        public ResultObj StockLevels(StockFilter filter)
        {
            ResultObj results;
            try
            {
                var stocks = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/StockBook.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var store = new BaseRepository<Store>().Get(filter.StoreId);

                contents = contents.Replace("[STORE]", store?.Name);
                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);


                var items = string.Empty;

                foreach (var item in stocks.GroupBy(x => x.Product.Type))
                {
                    var num = 0;
                    items += $"<tr><th colspan='6'>{item.Key.Name}</th></tr>";
                    foreach (var stock in item.ToList())
                    {
                        num++;
                        items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{stock.Product?.Code}</td>" +
                                $"<td>{stock.Product?.Name}</td>" +
                                $"<td>{stock.Product?.Type?.Name}</td>" +
                                $"<td>{stock.Product?.Package?.Name}</td>" +
                                $"<td>{stock.CurrentStock}</td>" +
                             "</tr>";
                    }

                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");
                var pdfOutput = Reporter.GeneratePdf(contents, PdfPageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Stock Book", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }
    }

    public class ProductUnit
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal MinPrice { get; set; }
        public long Unit { get; set; }
        public long CurrentStock { get; set; }
        public long? SchemeId { get; set; }
        public long? PackageId { get; set; }
        public string Package { get; set; }
        public string Desc { get; set; }
    }
}