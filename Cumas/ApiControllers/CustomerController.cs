using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class CustomerController : BaseApi<Customer>
    {
        [HttpPost]
        [Route("api/customer/query")]
        public ResultObj Query(CustomerFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying customers. Username: {User.Identity.Name}");
                var raw = Repository.Get(filter);
                var data = raw.OrderBy(x => x.Name)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                results = WebHelpers.BuildResponse(data, "", true, raw.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        // try
        //    {
        //        Logger.Info($"Querying products. Username: {User.Identity.Name}");
        //        var raw = Repository.Query(filter);
        //var data = raw.Include(q => q.Categories).OrderBy(x => x.Id)
        //    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).Select(x => new
        //    {
        //        x.Id,
        //        x.TypeId,
        //        x.Number,
        //        x.Barcode,
        //        x.CurrentStock,
        //        x.Image,
        //        x.MaximumStock,
        //        x.Name,
        //        x.Code,
        //        x.Price,
        //        x.Cost,
        //        x.MinPrice,
        //        x.Type,
        //        x.Margin,
        //        x.ReorderLevel,
        //        Package = new { x.Package.Id, x.Package.Name },
        //        x.PackageId,
        //        Categories = x.Categories.Select(c => new
        //        {
        //            c.Id,
        //            c.Name,
        //            c.ParentId
        //        }),
        //        PackageSchemes = x.PackageSchemes.Select(p => new
        //        {
        //            p.Id,
        //            p.Barcode,
        //            p.Desc,
        //            Package = new { p.Package.Id, p.Package.Name },
        //            p.Quantity,
        //            p.Price,
        //            p.MinPrice,
        //            p.Cost
        //        })
        //    });
        //results = WebHelpers.BuildResponse(data, "", true, raw.Count());
        //    }

        //[HttpGet]
        //[Route("api/customer/")]
        //public override ResultObj Get()
        //{

        //    ResultObj results;
        //    try
        //    {
        //        Logger.Info($"Querying customers. Username: {User.Identity.Name}");
        //        var raw = Repository.Get()
        //            .Select(x => new
        //            {
        //                accounts = x.

        //            });
        //        //var data = raw.OrderBy(x => x.Name)
        //        //    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
        //        results = WebHelpers.BuildResponse(data, "", true, raw.Count);
        //    }
        //    catch (Exception ex)
        //    {
        //        results = WebHelpers.ProcessException(ex);
        //        Logger.Error(ex, User.Identity.Name);
        //    }
        //    return results;
        //}

        [HttpPost]
        [Route("api/customer/upload")]
        public ResultObj BatchSave(List<Customer> records)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Uploading customers. Username: {User.Identity.Name}");
                foreach (var customer in records)
                {
                    customer.CreatedBy = User.Identity.Name;
                    customer.ModifiedBy = User.Identity.Name;
                    customer.CreatedAt = DateTime.UtcNow;
                    customer.ModifiedAt = DateTime.UtcNow;
                }

                Repository.BulkInsert(records);
                results = WebHelpers.BuildResponse(null, "Records Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }
    }
}