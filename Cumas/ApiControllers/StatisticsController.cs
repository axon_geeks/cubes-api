﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    [Authorize]
    public class StatisticsController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("api/start")]
        public ResultObj Start()
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(new WebHelpers().GetSubdomain(), "Your subdomain", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/dailystats")]
        public ResultObj DailyStats(long storeId = 0)
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");

                storeId = (User.IsInRole(Privileges.MultiStore)) ? storeId : user.StoreId;

                var date = DateHelpers.GetDateRange(DatePeriod.Today);

                var sales = new SaleRepository().Query(new SaleFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = storeId
                }).ToList();

                var totalSales = sales.Sum(x => x.TotalAmount);

                var totalPayments = new PaymentRepository().Query(new PaymentFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = User.IsInRole(Privileges.MultiStore) ? user.StoreId : 0
                }).ToList().Sum(x => x.Amount);

                var products = new BaseRepository<SaleItem>().Query(new SaleItemFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = User.IsInRole(Privileges.MultiStore) ? user.StoreId : 0
                }).ToList().Sum(x => x.Quantity);

                var totalExpenses = new BaseRepository<Expense>().Query(new ExpenseFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = User.IsInRole(Privileges.MultiStore) ? user.StoreId : 0
                }).ToList().Sum(x => x.Amount);

                var data = new
                {
                    TotalSales = totalSales,
                    CashSales = sales.Where(q => q.Status == SaleStatus.Paid && q.PaymentMode == PaymentMode.Cash).ToList().Sum(x => x.TotalAmount),
                    CreditSales = sales.Where(q => q.Status == SaleStatus.CreditSale).ToList().Sum(x => x.TotalAmount),
                    PendingSales = sales.Where(q => q.Status == SaleStatus.Pending).ToList().Sum(x => x.TotalAmount),
                    TotalExpenses = totalExpenses,
                    TotalPayments = totalPayments,
                    TotalRevenue = totalSales + totalPayments,
                    TotalProducts = products
                };

                results = WebHelpers.BuildResponse(data, "Daily Stats", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/storesummaries")]
        public ResultObj StoresStats()
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");

                var date = DateHelpers.GetDateRange(DatePeriod.Today);
                var stores = new BaseRepository<Store>().Get();
                var data = new List<StoreSummary>();
                stores.ForEach(q => data.Add(GetStoreSummary(date.Start, date.End, q.Name, q.Id)));

                results = WebHelpers.BuildResponse(data, "Store Summary", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        private static StoreSummary GetStoreSummary(DateTime start, DateTime end, string name, long storeId = 0)
        {
            var sales = new SaleRepository().Query(new SaleFilter
            {
                StartDate = start,
                EndDate = end,
                StoreId = storeId
            });

            var payments = new PaymentRepository().Query(new PaymentFilter
            {
                StartDate = start,
                EndDate = end,
                StoreId = storeId,
            }).ToList();

            var totalPayments = payments.Sum(x => x.Amount);
            var cashPayments = payments.Where(q => q.PaymentMode == PaymentMode.Cash).ToList().Sum(x => x.Amount);

            var expenses = new BaseRepository<Expense>().Query(new ExpenseFilter
            {
                StartDate = start,
                EndDate = end,
                StoreId = storeId
            }).ToList();

            var totalExpenses = expenses.Sum(x => x.Amount);
            var cashExpenses = expenses.Where(q => q.PaymentMode == PaymentMode.Cash).ToList().Sum(q => q.Amount);

            var totalBankDeposits = new BaseRepository<BankTransaction>().Query(new BankTransactionFilter
            {
                StartDate = start,
                EndDate = end,
                StoreId = storeId,
                Transaction = Transaction.Deposit,
                Source = TransactionSource.Bank
            }).ToList().Sum(x => x.Amount);

            var cashSales = sales.Where(q => q.Status == SaleStatus.Paid && q.PaymentMode == PaymentMode.Cash).ToList().Sum(x => x.TotalAmount);
            return new StoreSummary
            {
                Store = name,
                CashSales = cashSales,
                CreditSales = sales.Where(q => q.Status == SaleStatus.CreditSale).ToList().Sum(x => x.TotalAmount),
                Expenses = totalExpenses,
                Payments = totalPayments,
                CashAtHand = (cashSales + cashPayments) - (cashExpenses + totalBankDeposits)
            };
        }

        [HttpGet]
        [Route("api/pendingapproval")]
        public ResultObj PendingApprovals(long storeId = 0)
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");
                storeId = (User.IsInRole(Privileges.MultiStore)) ? storeId : user.StoreId;

                var request = new StockRequestRepository().Count(new StockRequestFilter
                {
                    Status = RequestStatus.Pending,
                    StoreId = storeId
                });

                var po = new PurchaseOrderRepository().Count(new PurchaseOrderFilter
                {
                    Status = PurchaseOrderStatus.Pending,
                    StoreId = storeId
                });

                var data = new { StockRequest = request, PurchaseOrder = po };

                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/duedelivery")]
        public ResultObj DueDelivery(long storeId = 0)
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");
                storeId = (User.IsInRole(Privileges.MultiStore)) ? storeId : user.StoreId;

                var issues = new StockIssueRepository().Query(new StockIssueFilter
                {
                    DestinationId = storeId,
                    Status = IssueStatus.Issued
                }).Select(x => new
                {
                    x.Id,
                    x.Date,
                    x.Number,
                    Origin = x.Origin.Name,
                    x.Driver
                });

                results = WebHelpers.BuildResponse(issues, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/inventoryalerts")]
        public ResultObj InventoryAlerts(long storeId = 0)
        {
            ResultObj results;
            try
            {

                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");
                storeId = (User.IsInRole(Privileges.MultiStore)) ? storeId : user.StoreId;

                var data = new BaseRepository<Stock>().Query(new StockFilter
                {
                    Alert = true,
                    StoreId = storeId
                }).Select(x => new
                {
                    x.Id,
                    x.CurrentStock,
                    x.ProductId,
                    x.Product.Name,
                    x.Product.Code,
                    Store = x.Store.Name,
                    Type = (x.CurrentStock <= x.Product.ReorderLevel) ? "Reorder Alert" : "Over Stock"
                }).ToList();


                results = WebHelpers.BuildResponse(data, "Inverntory Alerts", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("api/performancestats")]
        public ResultObj PerformanceStats(long storeId = 0)
        {
            ResultObj results;
            try
            {
                var user = new UserRepository().Get(User.Identity.Name);
                if (user == null) throw new Exception("Your user account can not be found. Please logout and logback in.");
                storeId = (User.IsInRole(Privileges.MultiStore)) ? storeId : user.StoreId;

                var date = DateHelpers.GetDateRange(DatePeriod.Last7Days);

                var sales = new SaleRepository().Query(new SaleFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = storeId
                }).ToList();

                var expenses = new BaseRepository<Expense>().Query(new ExpenseFilter
                {
                    StartDate = date.Start,
                    EndDate = date.End,
                    StoreId = storeId
                }).ToList();

                var data = new
                {
                    Sales = Enumerable.Range(1, 7).Select(offset => sales
                        .Where(q => q.Date.Date == date.Start.AddDays(offset).Date)
                        .Sum(x => x.TotalAmount)).ToArray(),
                    Expenses = Enumerable.Range(1, 7).Select(offset => expenses
                        .Where(q => q.Date.Date == date.Start.AddDays(offset).Date)
                        .Sum(x => x.Amount)).ToArray(),
                    Dates = Enumerable.Range(1, 7).Select(offset => date.Start.AddDays(offset).ToString("ddd dd MMM")).ToArray()
                };


                results = WebHelpers.BuildResponse(data, "Performance Stats", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

    }

    public class StoreSummary
    {
        public string Store { get; set; }
        public decimal CashSales { get; set; }
        public decimal CreditSales { get; set; }
        public decimal Payments { get; set; }
        public decimal Expenses { get; set; }
        public decimal CashAtHand { get; set; }
    }

}
