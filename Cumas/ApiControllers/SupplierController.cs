﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class SupplierController : BaseApi<Supplier>
    {
        [HttpPost]
        [Route("api/supplier/query")]
        public ResultObj Query(SupplierFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Get(filter);
                var data = raw.OrderBy(x => x.Name)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                results = WebHelpers.BuildResponse(data, "", true, raw.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/supplier/upload")]
        public ResultObj BatchSave(List<Supplier> records)
        {
            ResultObj results;
            try
            {
                foreach (var product in records)
                {
                    product.CreatedBy = User.Identity.Name;
                    product.ModifiedBy = User.Identity.Name;
                    product.CreatedAt = DateTime.UtcNow;
                    product.ModifiedAt = DateTime.UtcNow;
                }

                Repository.BulkInsert(records);
                results = WebHelpers.BuildResponse(null, "Records Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

    }
}