using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class PaymentController : BaseApi<Payment>
    {
        [HttpPost]
        [Route("api/payment/query")]
        public ResultObj Query(PaymentFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying payment records. Username {User.Identity.Name}");
                var raw = Repository.Get(filter);
                var data = raw.OrderByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size);
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(Payment record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new payment record. Username: {User.Identity.Name}");
                record.ReceiptNumber = ReferenceGenerator.PaymentReceipt();
                record.Amount = (record.Balance < 0) ? record.Amount + record.Balance : record.Amount;
                record.Balance = (record.Balance < 0) ? 0 : record.Balance;

                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }

                new PaymentRepository().Insert(SetAudit(record, true));

                results = WebHelpers.BuildResponse(record, "New Payment Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpGet]
        [Route("api/payment/generatereceipt")]
        public ResultObj GenerateReceipt(long id)
        {

            ResultObj results;
            try
            {
                Logger.Info($"Generating payment report. Username: {User.Identity.Name}");
                var payment = Repository.Get(id);
                if (payment == null) throw new Exception(ExceptionMessage.NotFound);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/Receipt.html"));

                var storeInfo = QuickAccess.GetStoreInfo(payment.StoreId);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[DATE]", payment.Date.ToString("f"));
                contents = contents.Replace("[RECEIPTNUMBER]", payment.ReceiptNumber);
                contents = contents.Replace("[CUSTOMER]", payment.Customer.Name);
                contents = contents.Replace("[PERSONNEL]", payment.CreatedBy);
                contents = contents.Replace("[AMOUNT]", payment.Amount.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[BALANCE]", payment.Balance.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[PAIDBY]", payment.PaidBy);
                contents = contents.Replace("[DESCRIPTION]", payment.Notes);
                contents = contents.Replace("[AMOUNTINWORDS]", ALHelpers.Convertors.Amount.ToEnglish((double)payment.Amount, "Ghana Cedis", "Pesewas"));


                var barcode = Convert.ToBase64String(new BarcodeGenerator().Generate(payment.ReceiptNumber));
                contents = contents.Replace("[BARCODE]", barcode);

                var pdfOutput = storeInfo.PrinterSet == PrinterSet.POSPrinter
                    ? Reporter.GeneratePdf(contents, 300, 700, 10)
                    : Reporter.GeneratePdf(contents, PageSize.A5);
                results = WebHelpers.BuildResponse(pdfOutput, "Payment Receipt", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/payment/report")]
        public ResultObj Report(PaymentFilter filter)
        {

            ResultObj results;
            try
            {
                var payments = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/PaymentReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var totalAmount = payments.Sum(x => x.Amount);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var payment in payments.Where(payment => payment.Amount > 0))
                {
                    num++;
                    items += "<tr>" +
                             $"<td>{num}</td>" +
                             $"<td>{payment.Date.ToString("F")}</td>" +
                             $"<td>{payment.ReceiptNumber}</td>" +
                             $"<td>{payment.Customer?.Name ?? "Walk-In"}</td>" +
                             $"<td>{payment.Store?.Name ?? ""}</td>" +
                             $"<td class='text-right'>{payment.Amount}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Payment Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Deleting Payment with id={id}. Username: {User.Identity.Name}");
                new PaymentRepository().Delete(id);
                results = WebHelpers.BuildResponse(id, $"Payment Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }
    }
}