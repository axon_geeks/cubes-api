﻿using System;
using System.Linq;
using Cumas.AxHelpers;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class ProductCategoryController : BaseApi<ProductCategory>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching ProductCategory. Username: {User.Identity.Name}");
                var data = Repository.Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Notes,
                    Parent = x.Parent != null ? new { x.Parent.Id, x.Parent.Name } : null,
                    x.ParentId,
                    x.CreatedBy,
                    x.CreatedAt,
                    x.ModifiedBy,
                    x.ModifiedAt
                });
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

    }
}