﻿using System;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class ProductTransformController : BaseApi<ProductTransform>
    {

        [HttpPost]
        [Route("api/producttransform/query")]
        public ResultObj Query(ProductTransformFilter filter)
        {
            Logger.Info($"Querying product transform. Username: {User.Identity.Name}");
            ResultObj results;
            try
            {
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.Id)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList().Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.CreatedBy,
                        x.CreatedAt,
                        x.ModifiedBy,
                        x.ModifiedAt,
                        x.Quantity,
                        x.ResultQuantity,
                        Source = x.SourceProducts.Select(p =>  $"{p.Product.Name} -> [{p.Quantity}]").Aggregate((a, b) => a + ", " + b),
                        x.EndProductId,
                        EndProduct = new { x.EndProduct.Id, x.EndProduct.Name },
                        x.StoreId
                    });

                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(ProductTransform record)
        {
            Repository = new ProductTransformRepository();
            return base.Post(record);
        }

        public override ResultObj Put(ProductTransform record)
        {
            return WebHelpers.ProcessException(new NotImplementedException());
        }

        public override ResultObj Delete(long id)
        {
            Repository = new ProductTransformRepository();
            return base.Delete(id);
        }
    }
}