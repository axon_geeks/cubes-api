using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class PurchaseOrderController : BaseApi<PurchaseOrder>
    {
        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Reading single purchase order object. Username: {User.Identity.Name}");
                var raw = Repository.Query(new PurchaseOrderFilter { Id = id }).Include(z => z.Items).FirstOrDefault();
                if (raw == null) throw new Exception(ExceptionMessage.NotFound);
                var data = new
                {
                    raw.Id,
                    raw.TotalAmount,
                    raw.Supplier,
                    raw.SupplierId,
                    raw.Store,
                    raw.StoreId,
                    raw.Status,
                    raw.Date,
                    raw.Number,
                    raw.CreatedBy,
                    raw.ModifiedBy,
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        i.ProductId,
                        Product = new
                        {
                            i.Product.Id,
                            Name = $"{i.Product.Name} ({i.Scheme?.Package.Name ?? i.Product.Package.Name})"
                        },
                        i.SchemeId,
                        Unit = i.Scheme?.Quantity ?? 1,
                        i.Quantity,
                        i.Cost,
                        i.Product.PackageId
                    })
                };

                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;


        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/purchaseorder/quick")]
        public ResultObj Search(string q)
        {
            q = q.ToLower();
            var data = Repository.Query(new PurchaseOrderFilter { Status = PurchaseOrderStatus.Approved })
                .Where(a => a.Store.Name.ToLower().Contains(q) ||
                            a.Supplier.Name.ToLower().Contains(q) ||
                            a.Number.ToLower().Contains(q))
                .OrderByDescending(x => x.Date)
                .Select(x => new
                {
                    x.Id,
                    x.Number,
                    x.Date,
                    x.Store,
                    x.StoreId,
                    x.SupplierId,
                    x.Supplier
                }).ToList();
            return WebHelpers.BuildResponse(data, "Seacrh Results", true, data.Count);
        }

        [HttpPost]
        [Route("api/purchaseorder/query")]
        public ResultObj Query(PurchaseOrderFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying purchase order. Username: {User.Identity.Name}");
                var raw = Repository.Query(filter);
                var data = raw.Include(x => x.Items)
                    .OrderBy(x => x.Status)
                    .ThenByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).Select(x => new
                    {
                        x.Id,
                        x.Number,
                        x.Date,
                        x.Store,
                        x.StoreId,
                        x.CreatedBy,
                        x.CreatedAt,
                        x.ModifiedBy,
                        x.ModifiedAt,
                        x.SupplierId,
                        x.Supplier,
                        x.TotalAmount,
                        Status = x.Status.ToString()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/purchaseorder/generateprint")]
        public ResultObj GeneratePrint(long id)
        {
            ResultObj results;
            try
            {
                var purchaseOrder = Repository.Query(new PurchaseOrderFilter { Id = id }).Include(z => z.Items).FirstOrDefault();
                if (purchaseOrder == null) throw new Exception(ExceptionMessage.NotFound);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/PurchaseOrder.html"));

                var storeInfo = QuickAccess.GetStoreInfo(purchaseOrder.StoreId ?? 0);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[DATE]", purchaseOrder.Date.ToString("D"));
                contents = contents.Replace("[PONUMBER]", purchaseOrder.Number);
                contents = contents.Replace("[SUPPLIERNAME]", purchaseOrder.Supplier.Name);
                contents = contents.Replace("[SUPPLIERADDRESS]", purchaseOrder.Supplier.PostalAddress);
                contents = contents.Replace("[SUPPLIERPHONENUMBER]", purchaseOrder.Supplier.PhoneNumber);
                contents = contents.Replace("[STATUS]", purchaseOrder.Status.ToString());
                contents = contents.Replace("[PERSONNEL]", purchaseOrder.CreatedBy);
                contents = contents.Replace("[DELIVERYLOCATION]", purchaseOrder.Store.Name);
                contents = contents.Replace("[TOTALAMOUNT]", purchaseOrder.TotalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var item in purchaseOrder.Items)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{item.Product.Name}</td>" +
                                $"<td>{item.Cost}</td>" +
                                $"<td>{item.Quantity}</td>" +
                                $"<td class='text-right'>{item.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Purchase Order", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        public override ResultObj Post(PurchaseOrder record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new purchase order. Username: {User.Identity.Name}");
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.Number = ReferenceGenerator.PurchaseOrderNumber();
                record.CreatedBy = User.Identity.Name;
                record.ModifiedBy = User.Identity.Name;
                new PurchaseOrderRepository().Insert(record);

                results = WebHelpers.BuildResponse(record, "Purchase Order Saved.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(PurchaseOrder record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating purchase order. Username: {User.Identity.Name}");
                if (record.Items.Count == 0) throw new Exception(NoItemsError);
                record.ModifiedBy = User.Identity.Name;
                new PurchaseOrderRepository().Update(record);

                results = WebHelpers.BuildResponse(record, "Purchase Order Updated.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        private const string NoItemsError = "No items found. Please add items before you can save.";
    }
}