using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class ExpenseController : BaseApi<Expense>
    {
        public override ResultObj Post(Expense record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating a new expense. Username: {User.Identity.Name}");
                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }

                new ExpenseRepository().Insert(SetAudit(record, true));

                results = WebHelpers.BuildResponse(record, "New Expense Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(Expense record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating expense Id={record.Id}. Username: {User.Identity.Name}");
                throw new NotImplementedException("Working hard to get this done.");
                if (record.PaymentMode == PaymentMode.Cheque)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                    record.BankAccountId = chequeAccount.Id;
                }

                var oldRecord = Repository.Get(record.Id);
                record.OldAmount = oldRecord.Amount;

                new ExpenseRepository().Update(SetAudit(record));

                results = WebHelpers.BuildResponse(record, "Expense Update Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/expense/query")]
        public ResultObj Query(ExpenseFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying expenses. Username: {User.Identity.Name}");
                var raw = Repository.Query(filter);
                var data = raw.OrderByDescending(x=>x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.Amount,
                        x.ReceiptNumber,
                        x.Notes,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.Type,
                        x.TypeId,
                        x.StoreId,
                        x.TransactionCode,
                        PaymentMode = x.PaymentMode.ToString()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/expense/report")]
        public ResultObj Report(ExpenseFilter filter)
        {

            ResultObj results;
            try
            {
                Logger.Info($"Generating expense report. Username: {User.Identity.Name}");
                var expenses = Repository.Query(filter).OrderBy(x => x.Date).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/ExpensesReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var totalAmount = expenses.Sum(x => x.Amount);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var expense in expenses)
                {
                    num++;
                    items += "<tr>" +
                             $"<td>{num}</td>" +
                             $"<td>{expense.Date.ToString("F")}</td>" +
                             $"<td>{expense.Type.Name}</td>" +
                             $"<td>{expense.Notes}</td>" +
                             $"<td class='text-right'>{expense.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Expenses Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }
    }
}