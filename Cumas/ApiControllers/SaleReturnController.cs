﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class SaleReturnController : BaseApi<SaleReturn>
    {
        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(new SaleReturnFilter { Id = id }).Include(z => z.Items).FirstOrDefault();
                if (raw == null) throw new Exception(ExceptionMessage.NotFound);
                var data = new
                {
                    raw.Id,
                    raw.Date,
                    raw.Sale.StoreId,
                    raw.Balance,
                    raw.Recepient,
                    raw.Note,
                    raw.CreatedBy,
                    raw.CreatedAt,
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        i.Unit,
                        Product = new { i.Product.Id, i.Product.Name, i.Product.Price },
                        i.ProductId,
                        i.Quantity,
                        i.Amount
                    })
                };
                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/salereturn/getsaleinstance")]
        public ResultObj GetSaleInstance(long id)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Get(id);
                var data = new
                {
                    raw.Id,
                    raw.Date,
                    Customer = raw.Sale.CustomerId.HasValue ? new { raw.Sale.Customer.Id, raw.Sale.Customer.Name } : null,
                    raw.Sale.StoreId,
                    raw.Balance,
                    raw.Recepient,
                    raw.Note,
                    raw.CreatedBy,
                    raw.CreatedAt,
                    Items = raw.Items.Select(i => new
                    {
                        i.Id,
                        Product = new
                        {
                            i.Product.Id,
                            i.Product.Code,
                            CurrentStock = i.Product.CurrentStock + (i.Quantity * i.Unit),
                            i.Product.MinPrice,
                            i.Product.Price,
                        },
                        i.ProductId,
                        i.Quantity,
                        i.Amount,
                        i.Unit
                    })
                };
                results = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/salereturn/query")]
        public ResultObj Query(SaleReturnFilter filter)
        {
            ResultObj results;
            try
            {
                filter.Personel = User.Identity.Name;
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.Date).ThenByDescending(x => x.Date)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.Sale.StoreId,
                        x.Sale.InvoiceNumber,
                        x.Balance,
                        x.Note,
                        x.Recepient,
                        x.CreatedBy,
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(SaleReturn record)
        {
            ResultObj results;
            try
            {
                if (record.Balance <= 0) throw new Exception("Invalid Sale Return. Check sale details.");
                var repository = new SaleReturnRepository();
                repository.Insert(SetAudit(record, true));
                results = WebHelpers.BuildResponse(record, "New Sale Return Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(SaleReturn record)
        {
            ResultObj results;
            try
            {
                if (record.Balance > 0) throw new Exception("Invalid Sale. Check sale details.");
                var repository = new SaleReturnRepository();
                repository.Update(SetAudit(record));
                results = WebHelpers.BuildResponse(record, $"Sale Return Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                new SaleRepository().Delete(id);
                results = WebHelpers.BuildResponse(id, "Sale Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/salereturn/generatereceipt")]
        public ResultObj GenerateReceipt(long id)
        {
            ResultObj results;
            try
            {
                var saleReturn = Repository.Query(new SaleReturnFilter { Id = id }).FirstOrDefault(); ;
                if (saleReturn == null) throw new Exception(ExceptionMessage.NotFound);

                saleReturn.Sale.Customer = new BaseRepository<Customer>().Get(saleReturn.Sale.CustomerId.GetValueOrDefault());
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/SaleReturnReceipt.html"));

                var storeInfo = QuickAccess.GetStoreInfo(saleReturn.Sale.StoreId);
                var user = new UserRepository().Get(saleReturn.CreatedBy);
                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[FOOTER]", storeInfo.FooterNote);
                contents = contents.Replace("[DATE]", saleReturn.Date.ToString("f"));
                contents = contents.Replace("[RECEIVEDBY]", saleReturn.Recepient);
                contents = contents.Replace("[PERSONNEL]", user?.Name ?? saleReturn.CreatedBy);
                contents = contents.Replace("[INVOICENUMBER]", saleReturn.Sale.InvoiceNumber);
                contents = contents.Replace("[TOTALAMOUNT]", saleReturn.Balance.ToString("##,###.00"));

                var items = string.Empty;
                var num = 0;
                foreach (var item in saleReturn.Items)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{item.Product.Name}</td>" +
                                $"<td>{item.Quantity}</td>" +
                                $"<td class='text-right'>{item.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                var barcode = Convert.ToBase64String(new BarcodeGenerator().Generate(saleReturn.Sale.InvoiceNumber));
                contents = contents.Replace("[BARCODE]", barcode);

                var pdfOutput = storeInfo.PrinterSet == PrinterSet.POSPrinter
                    ? Reporter.GeneratePdf(contents, 230, 650, 5)
                    : Reporter.GeneratePdf(contents, PageSize.A5);
                results = WebHelpers.BuildResponse(pdfOutput, "Sale Return Receipt", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpGet]
        [Route("api/salereturn/generateinvoice")]
        public ResultObj GenerateInvoice(long id, bool receipt)
        {

            ResultObj results;
            try
            {
                var invoice = Repository.Query(new SaleReturnFilter { Id = id }).Include(z => z.Items).FirstOrDefault();
                if (invoice == null) throw new Exception(ExceptionMessage.NotFound);

                var storeInfo = QuickAccess.GetStoreInfo(invoice.Sale.StoreId);

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(receipt ? @"~/ReportTemplates/InvoiceReceipt.html" : @"~/ReportTemplates/Invoice.html"));

                var balance = invoice.Balance - invoice.Balance;
                var user = new UserRepository().Get(invoice.CreatedBy);
                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[DATE]", invoice.Date.ToString("f"));
                contents = contents.Replace("[CUSTOMER]", invoice.Sale.CustomerId.HasValue ? invoice.Sale.Customer.Name : "Walk-In");
                contents = contents.Replace("[BUYER]", invoice.Sale.Buyer);
                contents = contents.Replace("[PERSONNEL]", user?.Name ?? invoice.CreatedBy);
                contents = contents.Replace("[BALANCE]", balance.ToString("##,###.00"));
                contents = contents.Replace("[BALNAME]", balance > 0 ? "Balance" : "Change");


                var items = string.Empty;
                var num = 0;
                foreach (var item in invoice.Items)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{item.Unit}</td>" +
                                $"<td>{item.Quantity}</td>" +
                                $"<td class='text-right'>{item.Amount.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                var barcode = Convert.ToBase64String(new BarcodeGenerator().Generate(invoice.Sale.InvoiceNumber));
                contents = contents.Replace("[BARCODE]", barcode);

                var pdfOutput = storeInfo.PrinterSet == PrinterSet.POSPrinter
                    ? Reporter.GeneratePdf(contents, 300, 700, 10)
                    : Reporter.GeneratePdf(contents, PageSize.A5);
                results = WebHelpers.BuildResponse(pdfOutput, "Sale Invoice", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }

        [HttpPost]
        [Route("api/salereturn/report")]
        public ResultObj Report(SaleReturnFilter filter)
        {

            ResultObj results;
            try
            {
                var sales = Repository.Query(filter).ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/SalesReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(filter.StoreId);
                var totalAmount = sales.Sum(x => x.Balance);

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);
                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");
                contents = contents.Replace("[TOTALAMOUNT]", totalAmount.ToString(DefaultKeys.CurrencyFormat));


                var items = string.Empty;
                var num = 0;
                foreach (var sale in sales)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{sale.Date.ToString("F")}</td>" +
                                $"<td>{sale.Sale.Customer?.Name ?? "Walk-In"}</td>" +
                                $"<td>{sale.Sale.Buyer}</td>" +
                                $"<td class='text-right'>{sale.Balance.ToString("##,###.00")}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Sales Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;

        }
    }
}