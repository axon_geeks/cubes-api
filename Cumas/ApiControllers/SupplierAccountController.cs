﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;
using HiQPdf;
using Microsoft.Ajax.Utilities;
using PdfSharp;

namespace Cumas.ApiControllers
{
    public class SupplierAccountController : BaseApi<SupplierAccount>
    {
        [HttpPost]
        [Route("api/supplieraccount/query")]
        public ResultObj Query(SupplierAccountFilter filter)
        {
            ResultObj results;
            try
            {
                var raw = Repository.Query(filter);
                var data = raw.OrderByDescending(x => x.Id)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size)
                    .Select(x => new
                    {
                        x.Id,
                        x.Date,
                        x.Notes,
                        x.Debit,
                        x.Credit
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpPost]
        [Route("api/supplieraccount/transactions")]
        public ResultObj Report(SupplierAccountFilter filter)
        {

            ResultObj results;
            try
            {
                //var supplier = new BaseRepository<Customer>().Get(filter.SupplierId);
                var totalDebit = 0m;
                var supplier = new BaseRepository<Supplier>().Get(filter.SupplierId);
                if (supplier == null) throw new Exception(ExceptionMessage.NotFound);

                var transactions = Repository.Query(filter).ToList();
                var tb = transactions.OrderByDescending(x => x.Id).FirstOrDefault();
                if (tb != null) totalDebit = tb.Debit;
                
                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/supplierAccounts.html"));

                var storeInfo = QuickAccess.GetStoreInfo(0);

                contents = contents.Replace("[SUPPLIERNAME]", supplier.Name);
                contents = contents.Replace("[SUPPLIERPHONE]", supplier.PhoneNumber);
                contents = contents.Replace("[SUPPLIEREMAIL]", supplier.Email);
                contents = contents.Replace("[TOTALDEBT]",totalDebit.ToString(DefaultKeys.CurrencyFormat));

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);

                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");

                var items = string.Empty;
                var num = 0;
                foreach (var transaction in transactions)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{transaction.Date.ToString("F")}</td>" +
                                $"<td>{transaction.Notes}</td>" +
                                $"<td>{transaction.Debit.ToString(DefaultKeys.CurrencyFormat)}</td>" +
                                $"<td>{transaction.Credit.ToString(DefaultKeys.CurrencyFormat)}</td>" +
                                $"<td>{transaction.Balance.ToString(DefaultKeys.CurrencyFormat)}</td>" +
                             "</tr>";
                }

                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Customer Transaction Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;

        }

        [HttpPost]
        [Route("api/supplieraccount/creditors")]
        public ResultObj Debtor(SupplierAccountFilter filter)
        {

            ResultObj results;
            try
            {
                var transactions = new BaseRepository<Supplier>()
                    .Query(new SupplierFilter ()).Where(q=>q.TotalCredit > 0)
                    .ToList();

                var contents = File.ReadAllText(System.Web.HttpContext.Current
                    .Server.MapPath(@"~/ReportTemplates/CreditorsReport.html"));

                var storeInfo = QuickAccess.GetStoreInfo(0);

                DateTime today = DateTime.Today;

                contents = contents.Replace("[STORENAME]", storeInfo.Name);
                contents = contents.Replace("[ADDRESS]", storeInfo.Address);
                contents = contents.Replace("[CONTACT]", storeInfo.PhoneNumber);
                contents = contents.Replace("[EMAIL]", storeInfo.Email);

                contents = contents.Replace("[STARTDATE]", filter.StartDate?.ToString("D") ?? "");
                contents = contents.Replace("[ENDDATE]", filter.EndDate?.ToString("D") ?? "");

                contents = contents.Replace("[TODAY]", today.ToString("D") ?? "");

                var items = string.Empty;
                var num = 0;
                decimal totalDebt = 0;
                foreach (var transaction in transactions)
                {
                    num++;
                    items += "<tr>" +
                                $"<td>{num}</td>" +
                                $"<td>{transaction.Name}</td>" +
                                $"<td>{transaction.PhoneNumber}</td>" +
                                $"<td>{transaction.Email}</td>" +
                                $"<td class='amt'>{transaction.TotalCredit.ToString(DefaultKeys.CurrencyFormat)}</td>" +
                             "</tr>";
                    totalDebt += transaction.TotalCredit;
                }

                contents = contents.Replace("[TOTALAMOUNT]", totalDebt.ToString(DefaultKeys.CurrencyFormat));
                contents = contents.Replace("[ITEMS]", items);
                contents = contents.Replace("[PRINTDATE]", $"{DateTime.Now:F}");

                var pdfOutput = Reporter.GeneratePdf(contents, PageSize.A4);
                results = WebHelpers.BuildResponse(pdfOutput, "Debtors' Report", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

    }
}