using System;
using Cumas.AxHelpers;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class StockIssueItemController : BaseApi<StockIssueItem>
    {
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                Repository.Delete(id);
                results = WebHelpers.BuildResponse(id, "Item Removed", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }
    }
}