using System;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;
using Microsoft.Ajax.Utilities;

namespace Cumas.ApiControllers
{
    public class BadStockController : BaseApi<BadStock>
    {
        [HttpPost]
        [Route("api/badstock/query")]
        public ResultObj Query(BadStockFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching BadStock. Username: {User.Identity.Name}");
                if (filter.StoreId == 0) throw new Exception("Select store to view bad stock.");
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.Id)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).Select(x => new
                    {
                        x.Id,
                        x.Quantity,
                        x.Notes,
                        x.StoreId,
                        Product = new
                        {
                            x.Product.Id,
                            x.Product.Name,
                            x.Product.Code
                        }
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        public override ResultObj Post(BadStock record)
        {
            Repository = new BadStockReoository();
            return base.Post(record);
        }

        public override ResultObj Delete(long id)
        {
            Repository = new BadStockReoository();
            return base.Delete(id);
        }
    }
}