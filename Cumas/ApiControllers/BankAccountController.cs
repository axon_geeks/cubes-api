using System;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class BankAccountController : BaseApi<BankAccount>
    {
        public override ResultObj Post(BankAccount record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new bank account with name {record.Name}. Username: {User.Identity.Name}");
                if (record.ChequeAccount)
                {
                    if (Repository.Any(new BankAccountFilter { ChequeAccount = true }))
                        throw new Exception("Another account is already setup as cheque account.");
                }
                Repository.Insert(SetAudit(record, true));
                results = WebHelpers.BuildResponse(record, "New Bank Account Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(BankAccount record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating bank account {record.Name} with id={record.Id}. Username: {User.Identity.Name}");
                if (record.ChequeAccount)
                {
                    var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                    if (chequeAccount != null && chequeAccount.Id != record.Id) throw new Exception("Another account is already setup as cheque account.");
                }
                Repository.Update(SetAudit(record));
                results = WebHelpers.BuildResponse(record, "Bank Account Update Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }
    }
}