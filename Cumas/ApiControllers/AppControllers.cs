﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.LogicUnit;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class ProfileController : BaseApi<Profile> { }
    public class AppSettingController : BaseApi<AppSetting> { }
    public class ExpenseTypeController : BaseApi<ExpenseType> { }
    public class ProductTypeController : BaseApi<ProductType> { }
    public class StockRequestItemController : BaseApi<StockRequestItem> { }
    public class SaleDetailController : BaseApi<SaleItem>
    {
        [Route("api/saleitem")]
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var saleItem = Repository.Query(new SaleItemFilter { Id = id }).Include(q => q.Sale).FirstOrDefault();
                if (saleItem == null) throw new Exception("Sale Item not found on sale. Please re-select sale.");
                var item = new { saleItem.ProductId, saleItem.Sale.StoreId, saleItem.Quantity, saleItem.SaleId, saleItem.Amount, saleItem.Unit };
                Repository.Delete(id);
                StoreKeeper.IncreaseStock(item.ProductId, item.StoreId, (item.Quantity * item.Unit));
                var saleRepo = new SaleRepository();
                var sale = saleRepo.Get(item.SaleId);
                if (sale != null)
                {
                    sale.TotalCost -= item.Amount;
                    sale.TotalAmount -= item.Amount;
                    saleRepo.Update(sale);
                }

                results = WebHelpers.BuildResponse(id, "Sale item deleted from sale.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }
    }
    public class PurchaseItemController : BaseApi<PurchaseItem> { }
    public class PurchaseOrderItemController : BaseApi<PurchaseOrderItem> { }
    public class ProductPackageController : BaseApi<ProductPackage> { }
    public class PackageSchemeController : BaseApi<PackageScheme> { }
    public class FixController : ApiController
    {
        [HttpGet]
        [Route("api/fix/cusbal")]
        public ResultObj FixCustomerBalances()
        {
            ResultObj results;
            try
            {
                var accounts = new BaseRepository<CustomerAccount>()
                    .Query(new CustomerAccountFilter())
                    .GroupBy(q => q.Customer)
                    .Select(x => new { CustomerId = x.Key.Id, Balance = x.Sum(s => (s.Credit - s.Debit)) })
                    .ToList();


                var cusRepo = new BaseRepository<Customer>();

                foreach (var account in accounts)
                {
                    var customer = cusRepo.Get(account.CustomerId);
                    customer.TotalDebt = account.Balance;
                    cusRepo.Update(customer);
                }

                results = WebHelpers.BuildResponse(null, "Fix Complete", true, 0);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

    }
    public class AccountsController : SysApi<Account> { }
    public class AccountTypeController : SysApi<AccountType> { }

    public class TaxController : BaseApi<Tax>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().Select(x => new { x.Id, x.Percentage, x.Vat, x.Nhil, Value = x.Percentage / 100 }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }
    }
}