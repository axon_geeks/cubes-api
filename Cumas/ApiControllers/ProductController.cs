﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.ApiControllers
{
    public class ProductController : BaseApi<Product>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching Products. Username: {User.Identity.Name}");
                var data = Repository.Query(new ProductFilter())
                    .Include(q => q.PackageSchemes)
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        Code = x.Code ?? "",
                        x.CurrentStock,
                        x.Price,
                        x.Barcode,
                        x.Margin,
                        Package = new { x.Package.Id, x.Name },
                        Packages = x.PackageSchemes.Select(s => new { s.Id, s.Desc, s.Package.Name, s.Quantity, s.Barcode })
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/product/getunits")]
        public ResultObj GetUnits()
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching product units. Username: {User.Identity.Name}");
                var products = Repository.Query(new ProductFilter())
                    .OrderBy(x=>x.Name)
                    .Include(q => q.PackageSchemes).ToList();

                var data = new List<ProductUnit>();
                foreach (var product in products)
                {
                    data.Add(new ProductUnit
                    {
                        Id = product.Id,
                        Code = product.Code,
                        Number = product.Number ?? "",
                        Name = $"{product.Name} ({product.Package?.Name}) {product.Number ?? ""}",
                        MinPrice = product.MinPrice,
                        CostPrice = product.Cost,
                        Price = product.Price,
                        Unit = 1,
                        CurrentStock = product.CurrentStock,
                        PackageId = product.PackageId,
                        Package = product.Package?.Name
                    });

                    product.PackageSchemes.ForEach(q => data.Add(new ProductUnit
                    {
                        Id = product.Id,
                        Code = q.Barcode,
                        Number = product.Number ?? "",
                        Name = $"{product.Name} ({q.Package.Name}{(!string.IsNullOrEmpty(q.Desc) ? $" - {q.Desc}" : "")}) {product.Number ?? ""}",
                        MinPrice = q.MinPrice,
                        Price = q.Price,
                        CostPrice = q.Cost,
                        Unit = q.Quantity,
                        CurrentStock = (product.CurrentStock / ((q.Quantity < 1) ? 1 : q.Quantity)),
                        SchemeId = q.Id,
                        PackageId = q.PackageId,
                        Package = q.Package.Name
                    }));
                }


                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }

        [HttpGet]
        [Route("api/product/stocklist")]
        public ResultObj StockList()
        {
            ResultObj results;
            try
            {
                Logger.Info($"Fetching stock list. Username: {User.Identity.Name}");
                var products = new ProductRepository().Query(new ProductFilter())
                    .Include(x => x.PackageSchemes).ToList();

                var data = new List<ProductUnit>();
                foreach (var product in products)
                {
                    data.Add(new ProductUnit
                    {
                        Id = product.Id,
                        Code = product.Code ?? "",
                        Name = $"{product.Name} ({product.Package.Name})",
                        MinPrice = product.MinPrice,
                        Price = product.Price,
                        Unit = 1,
                        CurrentStock = product.CurrentStock
                    });

                    product.PackageSchemes.ForEach(q => data.Add(new ProductUnit
                    {
                        Id = product.Id,
                        Code = q.Barcode,
                        Name = $"{product.Name} ({q.Package.Name}{(!string.IsNullOrEmpty(q.Desc) ? $" - {q.Desc}" : "")})",
                        MinPrice = q.MinPrice,
                        Price = q.Price,
                        Unit = q.Quantity,
                        SchemeId = q.Id
                    }));
                }

                results = WebHelpers.BuildResponse(data, "Products Stock List.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Post(Product record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Creating new product. Username: {User.Identity.Name}");
                record.CreatedBy = User.Identity.Name;
                record.ModifiedBy = User.Identity.Name;
                new ProductRepository().Insert(record);

                results = WebHelpers.BuildResponse(record, "New Product Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Put(Product record)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Updating product Id={record.Id}. Username: {User.Identity.Name}");
                record.ModifiedBy = User.Identity.Name;
                new ProductRepository().Update(record);
                results = WebHelpers.BuildResponse(record, "Product Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            var product = new BaseRepository<Stock>().Find(new StockFilter { ProductId = id });

            if (product == null)
            {
                try
                {
                    Logger.Info($"Deleting product. Id={id}. Username: {User.Identity.Name}");
                    Repository.Delete(id);
                    results = WebHelpers.BuildResponse(id, "Product Deleted Successfully.", true, 1);
                }
                catch (Exception ex)
                {
                    results = WebHelpers.ProcessException(ex);
                    Logger.Error(ex, User.Identity.Name);
                }
            }
            else
            {
                results = WebHelpers.BuildResponse(id, "Cannot delete Product because product has items in stock", false, 1);
            }
            return results;
        }

        [HttpPost]
        [Route("api/product/upload")]
        public ResultObj BatchSave(List<Product> records)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Uploading products. Username: {User.Identity.Name}");
                foreach (var product in records)
                {
                    product.CreatedBy = User.Identity.Name;
                    product.ModifiedBy = User.Identity.Name;
                    product.CreatedAt = DateTime.UtcNow;
                    product.ModifiedAt = DateTime.UtcNow;
                }

                Repository.BulkInsert(records);
                results = WebHelpers.BuildResponse(null, "Records Saved Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }

            return results;
        }

        [HttpPost]
        [Route("api/product/query")]
        public ResultObj Query(ProductFilter filter)
        {
            ResultObj results;
            try
            {
                Logger.Info($"Querying products. Username: {User.Identity.Name}");
                var raw = Repository.Query(filter);
                //var data = raw.Include(q => q.Categories).OrderBy(x => x.Id)
                    var data = raw.Include(q => q.Categories).OrderBy(x => x.Name)
                    .Skip(filter.Pager.Skip()).Take(filter.Pager.Size).Select(x => new
                    {
                        x.Id,
                        x.TypeId,
                        x.Barcode,
                        x.CurrentStock,
                        x.Image,
                        x.MaximumStock,
                        x.Name,
                        Number = x.Number ?? "",
                        x.Code,
                        x.Price,
                        x.Cost,
                        x.MinPrice,
                        x.Type,
                        x.Margin,
                        x.ReorderLevel,
                        Package = new { x.Package.Id, x.Package.Name },
                        x.PackageId,
                        Categories = x.Categories.Select(c => new
                        {
                            c.Id,
                            c.Name,
                            c.ParentId
                        }),
                        PackageSchemes = x.PackageSchemes.Select(p => new
                        {
                            p.Id,
                            p.Barcode,
                            p.Desc,
                            Package = new { p.Package.Id, p.Package.Name },
                            p.Quantity,
                            p.Price,
                            p.MinPrice,
                            p.Cost
                        })
                    });
                results = WebHelpers.BuildResponse(data, "", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
                Logger.Error(ex, User.Identity.Name);
            }
            return results;
        }
    }
}