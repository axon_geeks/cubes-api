﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class BaseRepository<T> where T : class
    {
        protected readonly AppDbContext DbContext;
        protected DbSet<T> DbSet { get; set; }

        public BaseRepository()
        {
            var name = new WebHelpers().GetSubdomain().Subdomain.Trim();
            if (name == "www" || name == "axoncubes") name = "demo";
            DbContext = new AppDbContext(name);
            DbSet = DbContext.Set<T>();
        }

        public T First() { return DbSet.FirstOrDefault(); }
        public T Get(long id) { return DbSet.Find(id); }

        public T Find(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).FirstOrDefault(); }

        public List<T> Get() { return DbSet.ToList(); }

        public virtual void BulkSave(List<T> entries)
        {
            entries.ForEach(q => DbSet.AddOrUpdate(q));
            DbContext.SaveChanges();
        }

        public long Count() { return DbSet.Count(); }

        public long Count(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).Count(); }
        public bool Any(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).Any(); }

        public List<T> Get(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).ToList(); }

        public virtual IQueryable<T> Query(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)); }
        public virtual IQueryable<T> Query() { return DbSet.Select(x => x); }

        public virtual void Update(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public virtual void Insert(T entity)
        {
            DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public virtual void Save(T entity)
        {
            DbSet.AddOrUpdate(entity);
            DbContext.SaveChanges();
        }

        public virtual void BulkInsert(List<T> entities)
        {
            DbSet.AddRange(entities);
            DbContext.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            var record = DbSet.Find(id);

            var hasLocked = typeof(T).GetProperty(GenericProperties.Locked);
            if (hasLocked != null)
            {
                var islocked = (bool)hasLocked.GetValue(record, null);
                if (islocked) throw new Exception(ExceptionMessage.RecordLocked);
            }

            DbSet.Remove(record);
            DbContext.SaveChanges();
        }

        public void SaveChanges() { DbContext.SaveChanges(); }
    }

    public class SysRepository<T> where T : class
    {
        protected readonly SysDbContext DbContext;
        protected DbSet<T> DbSet { get; set; }

        public SysRepository()
        {
            DbContext = new SysDbContext();
            DbSet = DbContext.Set<T>();
        }

        public T Get(long id) { return DbSet.Find(id); }

        public T Find(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).FirstOrDefault(); }

        public List<T> Get() { return DbSet.ToList(); }

        public virtual void BulkSave(List<T> entries)
        {
            entries.ForEach(q => DbSet.AddOrUpdate(q));
            DbContext.SaveChanges();
        }

        public long Count() { return DbSet.Count(); }

        public long Count(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).Count(); }
        public bool Any(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).Any(); }

        public List<T> Get(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)).ToList(); }

        public virtual IQueryable<T> Query(Filter<T> filter) { return filter.BuildQuery(DbSet.Select(x => x)); }

        public virtual void Update(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public virtual void Insert(T entity)
        {
            DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public virtual void BulkInsert(List<T> entities)
        {
            DbSet.AddRange(entities);
            DbContext.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            var record = DbSet.Find(id);

            var hasLocked = typeof(T).GetProperty(GenericProperties.Locked);
            if (hasLocked != null)
            {
                var islocked = (bool)hasLocked.GetValue(record, null);
                if (islocked) throw new Exception(ExceptionMessage.RecordLocked);
            }

            DbSet.Remove(record);
            DbContext.SaveChanges();
        }

        public void SaveChanges() { DbContext.SaveChanges(); }
    }
}