using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class ProductTransformRepository : BaseRepository<ProductTransform>
    {
        public override void Insert(ProductTransform entity)
        {
            foreach (var sourceProduct in entity.SourceProducts)
            {
                var sourceStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == sourceProduct.ProductId && q.StoreId == entity.StoreId);
                if (sourceStock == null) throw new Exception("This store doesn't have stock of this products.");
                if (sourceStock.CurrentStock < sourceProduct.Quantity) throw new Exception("Current stock is lesser than issue quantity.");
                sourceStock.CurrentStock -= sourceProduct.Quantity;
                DbContext.Stocks.AddOrUpdate(sourceStock);
            }

            var resultStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == entity.EndProductId && q.StoreId == entity.StoreId) ??
                              new Stock { ProductId = entity.EndProductId, StoreId = entity.StoreId, CurrentStock = 0 };
            resultStock.CurrentStock += entity.ResultQuantity;
            
            DbContext.Stocks.AddOrUpdate(resultStock);
            DbSet.Add(entity);
            SaveChanges();
        }

        public override void Delete(long id)
        {
            var entity = DbSet.FirstOrDefault(q => q.Id == id);
            if (entity == null) throw new Exception(ExceptionMessage.NotFound);
            if (entity.Locked) throw new Exception(ExceptionMessage.RecordLocked);

            foreach (var sourceProduct in entity.SourceProducts)
            {
                var sourceStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == sourceProduct.ProductId && q.StoreId == entity.StoreId);
                if (sourceStock != null)
                {
                    sourceStock.CurrentStock += sourceProduct.Quantity;
                    DbContext.Stocks.AddOrUpdate(sourceStock);
                }
            }

           

            var resultStock =DbContext.Stocks.FirstOrDefault(q => q.ProductId == entity.EndProductId && q.StoreId == entity.StoreId);
            if (resultStock != null)
            {
                resultStock.CurrentStock -= entity.ResultQuantity;
                DbContext.Stocks.AddOrUpdate(resultStock);
            }

            DbSet.Remove(entity);
            SaveChanges();
        }

    }
}