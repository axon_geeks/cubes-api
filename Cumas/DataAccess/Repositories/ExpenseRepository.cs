using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class ExpenseRepository : BaseRepository<Expense>
    {
        public override void Insert(Expense entity)
        {
            DbSet.Add(entity);
            DbContext.SaveChanges();

            if (entity.BankAccountId > 0)
            {
                var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);
                bankAccount.Balance -= entity.Amount;

                var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" Transaction Code: {entity.TransactionCode}." : string.Empty;
                var bankTransaction = new BankTransaction
                {
                    Date = entity.Date,
                    Account = bankAccount,
                    Amount = entity.Amount,
                    StoreId = entity.StoreId,
                    Credit = entity.Amount,
                    Debit = 0,
                    Balance = bankAccount.Balance,
                    Transaction = Transaction.Withdrawal,
                    Source = TransactionSource.Expense,
                    SourceId = entity.Id,
                    Note = entity.Notes + transCode.Trim(),
                    CreatedBy = entity.ModifiedBy,
                    ModifiedBy = entity.ModifiedBy,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow
                };

                DbContext.BankTransactions.Add(bankTransaction);
            }

            DbContext.SaveChanges();
        }

        public override void Update(Expense entity)
        {
            if (entity.BankAccountId > 0)
            {
                var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);

                var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" Transaction Code: {entity.TransactionCode}." : string.Empty;

                var bankTransaction = DbContext.BankTransactions
                    .FirstOrDefault(q => q.Source == TransactionSource.Expense && q.SourceId == entity.Id);

                if (bankTransaction == null)
                {
                    bankAccount.Balance -= entity.Amount;
                    bankTransaction = new BankTransaction
                    {
                        Date = entity.Date,
                        Account = bankAccount,
                        Amount = entity.Amount,
                        StoreId = entity.StoreId,
                        Credit = entity.Amount,
                        Debit = 0,
                        Balance = bankAccount.Balance,
                        Transaction = Transaction.Withdrawal,
                        Source = TransactionSource.Expense,
                        SourceId = entity.Id,
                        Note = entity.Notes + transCode.Trim(),
                        CreatedBy = entity.ModifiedBy,
                        ModifiedBy = entity.ModifiedBy,
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow
                    };
                }
                else
                {
                    bankAccount.Balance = (bankAccount.Balance + entity.OldAmount) - entity.Amount;
                    bankTransaction.ModifiedAt = DateTime.UtcNow;
                    bankTransaction.ModifiedBy = entity.ModifiedBy;
                    bankTransaction.Credit = entity.Amount;
                    bankTransaction.Amount = entity.Amount;
                    bankTransaction.Account =  bankAccount;
                }

                DbContext.BankTransactions.AddOrUpdate(bankTransaction);
            }

            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
        }
    }
}