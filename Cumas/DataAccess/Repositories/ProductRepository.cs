using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class ProductRepository : BaseRepository<Product>
    {

        public override void Insert(Product entity)
        {
            entity.Categories = MakeCategories(entity);
            entity.PackageSchemes = MakeSchemes(entity);
            DbSet.Add(entity);
            SaveChanges();
        }

        public override void Update(Product entity)
        {
            var theProduct = DbSet.Include(q => q.Categories).FirstOrDefault(q => q.Id == entity.Id);
            if (theProduct == null) throw new Exception("Invalid update. Record not found");
            var categories = MakeCategories(entity);
            var schemes = MakeSchemes(entity);
            theProduct.Code = entity.Code;
            theProduct.Name = entity.Name;
            theProduct.Number = entity.Number;
            theProduct.Cost = entity.Cost;
            theProduct.TypeId = entity.TypeId;
            theProduct.PackageId = entity.PackageId;
            theProduct.Price = entity.Price;
            theProduct.MinPrice = entity.MinPrice;
            theProduct.ReorderLevel = entity.ReorderLevel;
            theProduct.MaximumStock = entity.MaximumStock;
            theProduct.CurrentStock = entity.CurrentStock;
            theProduct.Image = entity.Image;
            theProduct.Margin = entity.Margin;
            theProduct.Barcode = entity.Barcode;
            theProduct.Categories.Clear();
            theProduct.Categories = categories;
            DbContext.Entry(theProduct).State = EntityState.Modified;

            schemes.ForEach(scheme => DbContext.PackageSchemes.AddOrUpdate(scheme));

            DbContext.SaveChanges();
        }

        public List<ProductCategory> MakeCategories(Product product)
        {
            var theCategories = new List<ProductCategory>();
            product.Categories?.ForEach(q => theCategories.Add(DbContext.ProductCategories.Find(q.Id)));
            return theCategories;
        }

        public List<PackageScheme> MakeSchemes(Product product)
        {
            if (product.PackageSchemes == null) return new List<PackageScheme>();
            return product.PackageSchemes.Select(item => new PackageScheme
            {
                Id = item.Id,
                Desc = item.Desc,
                PackageId = item.PackageId,
                Quantity = item.Quantity,
                Price = item.Price,
                MinPrice = item.MinPrice,
                Cost = item.Cost,
                ProductId = product.Id,
                Barcode = item.Barcode
            }).ToList();
        }
    }
}