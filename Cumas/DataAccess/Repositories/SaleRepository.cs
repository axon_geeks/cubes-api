﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.DataAccess.Filters;
using Cumas.Models;
using WebGrease.Css.Extensions;
using System.Configuration;
using Cumas.LogicUnit;
using System.Transactions.Configuration;


namespace Cumas.DataAccess.Repositories
{
    public class SaleRepository : BaseRepository<Sale>
    {
        public override void Insert(Sale entity)
        {
            entity.Items = MakeItems(entity);
            entity.ActualBalance = entity.Balance;
            UpdateStock(entity);
            DbSet.Add(entity);
            SaveChanges();
        }

        private void UpdateStock(Sale entity)
        {
            //Reduce Stock
            foreach (var item in entity.Items)
            {
                //Check if Product exists
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.StoreId);
                if (stock == null) throw new Exception("This store doesn't have stock of this products.");

                //Ensure you have enough stock
                var quantity = item.Unit * item.Quantity;
                if (stock.CurrentStock < quantity) throw new Exception($"{stock.Product.Name} current stock is lesser than issue quantity.");

                //Update stock with new value
                stock.CurrentStock -= quantity;
                DbContext.Stocks.AddOrUpdate(stock);
                DbContext.StockTallies.Add(new StockTally
                {
                    Quantity = quantity,
                    Balance = stock.CurrentStock,
                    Source = StockSource.Sales,
                    SourceId = entity.Id,
                    SourceReference = entity.InvoiceNumber,
                    StockId = stock.Id,
                    TimeStamp = DateTime.UtcNow,
                    Notes = "Sales of stock items",
                    CreatedAt = DateTime.UtcNow,
                    CreatedBy = entity.CreatedBy,
                    ModifiedBy = entity.ModifiedBy,
                    ModifiedAt = entity.ModifiedAt
                });
            }
        }
        private void UpdateStock(Sale entity, AppDbContext dbContext)
        {
            //Reduce Stock
            foreach (var item in entity.Items)
            {
                //Check if Product exists
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.StoreId);
                if (stock == null) throw new Exception("This store doesn't have stock of this products.");

                //Ensure you have enough stock
                var quantity = item.Unit * item.Quantity;
                if (stock.CurrentStock < quantity) throw new Exception($"{stock.Product.Name} current stock is lesser than issue quantity.");

                //Update stock with new value
                stock.CurrentStock -= quantity;
                dbContext.Stocks.AddOrUpdate(stock);
                dbContext.StockTallies.Add(new StockTally
                {
                    Quantity = quantity,
                    Balance = stock.CurrentStock,
                    Source = StockSource.Sales,
                    SourceId = entity.Id,
                    SourceReference = entity.InvoiceNumber,
                    StockId = stock.Id,
                    TimeStamp = DateTime.UtcNow,
                    Notes = "Sales of stock items",
                    CreatedAt = DateTime.UtcNow,
                    CreatedBy = entity.CreatedBy,
                    ModifiedBy = entity.ModifiedBy,
                    ModifiedAt = entity.ModifiedAt
                });
            }
        }

        public override void Update(Sale entity)
        {
            var theSale = DbSet.FirstOrDefault(q => q.Id == entity.Id);
            if (theSale == null) throw new Exception("Invalid update. Record not found");
            var items = MakeItems(entity);

            //Update Sale Record
            theSale.PaymentMode = entity.PaymentMode;
            theSale.Buyer = entity.Buyer;
            theSale.Notes = entity.Notes;
            theSale.TotalCost = entity.TotalCost;
            theSale.Discount = entity.Discount;
            theSale.TotalAmount = entity.TotalAmount;
            theSale.TaxAmount = entity.TaxAmount;
            theSale.TaxId = entity.TaxId;
            theSale.AmountPaid = entity.AmountPaid;
            theSale.ActualBalance = entity.ActualBalance;
            theSale.Status = entity.Status;
            items.ForEach(i => DbContext.SaleDetails.AddOrUpdate(q => q.Id, i));

            foreach (var item in items.Where(q => q.PreviousQuantity > 0))
            {
                var stock =
                    DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == theSale.StoreId);
                if (stock == null) continue;

                stock.CurrentStock += (item.PreviousQuantity - item.Quantity);
                DbContext.Stocks.AddOrUpdate(stock);
            }


            DbContext.Entry(theSale).State = EntityState.Modified;

            //Apply changes to database
            DbContext.SaveChanges();
        }

        public void UpdatePayment(Sale entity)
        {
            entity.ActualBalance = entity.Balance;
            if (entity.CustomerId.HasValue)
            {
                var customer = DbContext.Customers.Find(entity.CustomerId);
                var accountBalance = customer.TotalDebt;
                if (entity.Balance > 0)
                {
                    accountBalance += entity.TotalAmount;
                    var transaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = entity.TotalAmount,
                        Credit = 0,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    DbContext.CustomerAccounts.Add(transaction);
                }

                if (entity.AmountPaid > 0 && entity.Balance > 0)
                {
                    accountBalance -= entity.AmountPaid;
                    var paymentTransaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = 0,
                        Credit = entity.AmountPaid,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    DbContext.CustomerAccounts.Add(paymentTransaction);
                }

                if (entity.AmountPaid > 0 && entity.Balance <= 0)
                {
                    var paymentTransaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = entity.AmountPaid,
                        Credit = entity.AmountPaid,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    DbContext.CustomerAccounts.Add(paymentTransaction);
                }

                customer.TotalDebt = accountBalance;
                DbContext.Customers.AddOrUpdate(customer);
            }

            if (entity.BankAccountId > 0)
            {
                var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);
                bankAccount.Balance += entity.AmountPaid;

                var paymentMode = (entity.PaymentMode == PaymentMode.Other) ? bankAccount.Name : entity.PaymentMode.ToString();
                var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" with Transaction Code: {entity.TransactionCode}" : string.Empty;
                var bankTransaction = new BankTransaction
                {
                    Date = entity.Date,
                    Account = bankAccount,
                    Amount = entity.AmountPaid,
                    StoreId = entity.StoreId,
                    Credit = 0,
                    Debit = entity.AmountPaid,
                    Transaction = Transaction.Deposit,
                    Source = TransactionSource.Sale,
                    SourceId = entity.Id,
                    Note = $"{paymentMode} payment for sale {entity.InvoiceNumber}{transCode}.",
                    CreatedBy = entity.ModifiedBy,
                    ModifiedBy = entity.ModifiedBy,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                };

                DbContext.BankTransactions.Add(bankTransaction);
            }
            DbContext.Entry(entity).State = EntityState.Modified;
            //Update stock only after payment
            //UpdateStock(entity);

            DbContext.SaveChanges();


        }
        public void UpdatePayment(Sale entity, AppDbContext context)
        {
            entity.ActualBalance = entity.Balance;
            if (entity.CustomerId.HasValue)
            {
                var customer = context.Customers.Find(entity.CustomerId);
                var accountBalance = customer.TotalDebt;
                if (entity.Balance > 0)
                {
                    accountBalance += entity.TotalAmount;
                    var transaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = entity.TotalAmount,
                        Credit = 0,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    context.CustomerAccounts.Add(transaction);
                }

                if (entity.AmountPaid > 0 && entity.Balance > 0)
                {
                    accountBalance -= entity.AmountPaid;
                    var paymentTransaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = 0,
                        Credit = entity.AmountPaid,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    context.CustomerAccounts.Add(paymentTransaction);
                }

                if (entity.AmountPaid > 0 && entity.Balance <= 0)
                {
                    var paymentTransaction = new CustomerAccount
                    {
                        CustomerId = entity.CustomerId.Value,
                        Debit = entity.AmountPaid,
                        Credit = entity.AmountPaid,
                        Balance = accountBalance,
                        RecordId = entity.Id,
                        Source = CustomerTransaction.Sales,
                        Notes = entity.InvoiceNumber,
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = entity.CreatedBy,
                        ModifiedBy = entity.ModifiedBy,
                        ModifiedAt = entity.ModifiedAt,
                        Date = entity.Date
                    };
                    context.CustomerAccounts.Add(paymentTransaction);
                }

                customer.TotalDebt = accountBalance;
                context.Customers.AddOrUpdate(customer);
            }

            if (entity.BankAccountId > 0)
            {
                var bankAccount = context.BankAccounts.Find(entity.BankAccountId);
                bankAccount.Balance += entity.AmountPaid;

                var paymentMode = (entity.PaymentMode == PaymentMode.Other) ? bankAccount.Name : entity.PaymentMode.ToString();
                var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" with Transaction Code: {entity.TransactionCode}" : string.Empty;
                var bankTransaction = new BankTransaction
                {
                    Date = entity.Date,
                    Account = bankAccount,
                    Amount = entity.AmountPaid,
                    StoreId = entity.StoreId,
                    Credit = 0,
                    Debit = entity.AmountPaid,
                    Transaction = Transaction.Deposit,
                    Source = TransactionSource.Sale,
                    SourceId = entity.Id,
                    Note = $"{paymentMode} payment for sale {entity.InvoiceNumber}{transCode}.",
                    CreatedBy = entity.ModifiedBy,
                    ModifiedBy = entity.ModifiedBy,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                };

                context.BankTransactions.Add(bankTransaction);
            }
            context.Entry(entity).State = EntityState.Modified;
            //Update stock only after payment
            //UpdateStock(entity);

            context.SaveChanges();


        }

        private List<SaleItem> MakeItems(Sale sale)
        {
            if (sale.Items == null) return new List<SaleItem>();
            return (from item in sale.Items
                    let previousItem = DbContext.SaleDetails.Find(item.Id)
                    select new SaleItem
                    {
                        Id = item.Id,
                        SchemeId = item.SchemeId,
                        Quantity = item.Quantity,
                        ProductId = item.ProductId,
                        Price = item.Price,
                        PriceSold = item.PriceSold,
                        Amount = item.Quantity * item.PriceSold,
                        Unit = item.Unit,
                        Desc = item.Desc,
                        Discount = ((item.Price - item.PriceSold) < 0 ? 0 : (item.Price - item.PriceSold)),
                        SaleId = sale.Id,
                        PreviousQuantity = previousItem?.Quantity ?? 0
                    }).ToList();
        }

        public override void Delete(long id)
        {
            var record = DbContext.Sales.Include(x => x.Items).FirstOrDefault(x => x.Id == id);
            if (record == null) throw new Exception(ExceptionMessage.NotFound);
            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);

            if (record.Status == SaleStatus.CreditSale && record.ActualBalance < record.TotalAmount)
                throw new Exception("Delete not allowed. Payment has been made for this sale.");

            //Increase Stock
            foreach (var item in record.Items)
            {
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == record.StoreId);
                if (stock == null) continue;
                var quantity = item.Unit * item.Quantity;
                stock.CurrentStock += quantity;
                DbContext.Stocks.AddOrUpdate(stock);
            }

            //Delete Customer Transactions
            if (record.CustomerId.HasValue)
            {
                var customer = DbContext.Customers.Find(record.CustomerId.Value);
                var transactions = DbContext.CustomerAccounts
                    .Where(q => q.CustomerId == record.CustomerId.Value
                    && q.Source == CustomerTransaction.Sales
                    && q.Notes == record.InvoiceNumber
                    && q.RecordId == record.Id).ToList();

                //Modify Customer Total Debt
                customer.TotalDebt -= transactions.Sum(x => x.Debit);
                customer.TotalDebt += transactions.Sum(x => x.Credit);

                DbContext.CustomerAccounts.RemoveRange(transactions);
                DbContext.Customers.AddOrUpdate(customer);
            }

            //Delete Bank Transaction
            if (record.PaymentMode != PaymentMode.Cash)
            {
                var bankTransaction = DbContext.BankTransactions.FirstOrDefault(x => x.SourceId == record.Id && x.Source == TransactionSource.Sale);
                if (bankTransaction != null)
                {
                    var bankAccount = DbContext.BankAccounts.Find(bankTransaction.AccountId);
                    bankAccount.Balance -= bankTransaction.Amount;
                    DbContext.BankAccounts.AddOrUpdate(bankAccount);
                    DbContext.BankTransactions.Remove(bankTransaction);
                }
            }

            DbSet.Remove(record);
            DbContext.SaveChanges();
        }

        public void CloseSalePeriod()
        {
            var startDate = DateTime.Today;
            var endDate = DateTime.Today.AddHours(24);
            var todaysSales = DbSet.Where(q => q.Date >= startDate && q.Date <= endDate).GroupBy(x => x.Store).ToList();

            foreach (var sales in todaysSales)
            {
                var summary = DbContext.DailySaleSummaries.FirstOrDefault(q => q.StoreId == sales.Key.Id && q.Date == DateTime.Today) ??
                              new DailySaleSummary { Date = DateTime.Today, StoreId = sales.Key.Id };

                summary.AmountExVat = sales.Sum(s => s.TotalAmount - s.TaxAmount);
                summary.VatAmount = sales.Sum(s => s.TaxAmount);
                summary.SaleValue = sales.Sum(s => s.TotalAmount);

                DbContext.DailySaleSummaries.AddOrUpdate(summary);
            }

            SaveChanges();
        }

        public void CloseSalePeriod(DateTime startDate, DateTime endDate, decimal taxRate)
        {
            endDate = endDate.AddHours(24);
            var storeSales = DbSet.Where(q => q.Date >= startDate && q.Date <= endDate)
                .GroupBy(x => x.Store).ToList();

            foreach (var storeSale in storeSales)
            {
                var daySales = storeSale.GroupBy(q => q.Date.Date).ToList();

                foreach (var daySale in daySales)
                {
                    var summary = DbContext.DailySaleSummaries.FirstOrDefault(q => q.StoreId == storeSale.Key.Id && q.Date == daySale.Key) ??
                                  new DailySaleSummary { Date = daySale.Key, StoreId = storeSale.Key.Id };

                    var totalAmount = daySale.Sum(s => s.TotalAmount);
                    var taxAmount = (taxRate / (taxRate + 100)) * totalAmount;
                    summary.Name = daySale.Key.ToString("D");
                    summary.AmountExVat = totalAmount - taxAmount;
                    summary.VatAmount = taxAmount;
                    summary.SaleValue = totalAmount;

                    DbContext.DailySaleSummaries.AddOrUpdate(summary);
                }
            }
            SaveChanges();
        }

        public Sale SaveAll(Sale record)
        {
            #region
            using (var tran = DbContext.Database.BeginTransaction())
            {
                try
                {
                    if (record.TotalCost <= 0) throw new Exception("Invalid Sale. Check sale details.");
                    //var repository = new SaleRepository();
                    record.InvoiceNumber = ReferenceGenerator.SalesReceipt();
                    record.Items = MakeItems(record);
                    record.ActualBalance = record.Balance;
                    UpdateStock(record);
                    var sale = DbContext.Sales.Add(record);
                    DbContext.SaveChanges();

                    //var sale = Get(savedSale.Id);
                    if (sale == null)
                    {
                        tran.Rollback();
                        throw new Exception("Sale not found");
                    }
                    sale.Status = (sale.CustomerId.HasValue && (record.AmountPaid < record.TotalAmount))
                    ? SaleStatus.CreditSale
                    : SaleStatus.Paid;

                    sale.TaxAmount = record.TaxAmount;
                    sale.Discount = record.Discount;
                    sale.TotalAmount = record.TotalAmount;
                    //sale.AmountPaid = (record.AmountPaid > record.TotalAmount) ? record.TotalAmount : record.AmountPaid;
                    sale.AmountPaid = record.AmountPaid;
                    var balance = sale.TotalAmount - sale.AmountPaid;
                    sale.Balance = (balance < 0) ? 0 : balance;
                    sale.Paid = (sale.Status == SaleStatus.Paid);
                    sale.PaymentMode = record.PaymentMode;
                    sale.TransactionCode = record.TransactionCode;

                    if (record.PaymentMode == PaymentMode.Cheque)
                    {
                        var chequeAccount = new BaseRepository<BankAccount>().Find(new BankAccountFilter { ChequeAccount = true });
                        if (chequeAccount == null) throw new Exception("No account setup for cheque recievables. Please setup a bank account fro cheque recievables.");
                        record.BankAccountId = chequeAccount.Id;
                    }
                    sale.BankAccountId = record.BankAccountId;

                    UpdatePayment(sale);
                    tran.Commit();
                    return sale;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw new Exception("Error occured while saving sale", ex);
                }

            }
            #endregion

        }


    }
}