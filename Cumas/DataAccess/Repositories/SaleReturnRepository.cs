﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class SaleReturnRepository : BaseRepository<SaleReturn>
    {
        public override void Insert(SaleReturn entity)
        {
            entity.Items = MakeItems(entity);
            entity.Balance = entity.Balance;
            entity.SaleId = entity.Sale.Id;
            var sale = DbContext.Sales.Find(entity.Sale.Id);
            entity.Sale = sale;
            //Update Sale and stock
            UpdateSaleAndStock(entity, sale,false);
            DbSet.Add(entity);
            SaveChanges();

            UpdateCustomerAccount(entity);
        }

        public override void Update(SaleReturn entity)
        {
        }

        private void UpdateCustomerAccount(SaleReturn saleReturn)
        {
            if (!saleReturn.Sale.CustomerId.HasValue) return;
            //Reduce Customer Debt
            var customer = DbContext.Customers.Find(saleReturn.Sale.CustomerId);
            var transaction = new CustomerAccount
            {
                CustomerId = saleReturn.Sale.CustomerId.Value,
                Credit = saleReturn.Balance,
                Debit = 0,
                Balance = customer.TotalDebt - saleReturn.Balance,
                RecordId = saleReturn.Id,
                Source = CustomerTransaction.SaleReturns,
                Notes = saleReturn.Sale.InvoiceNumber,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = saleReturn.CreatedBy,
                ModifiedBy = saleReturn.ModifiedBy,
                ModifiedAt = saleReturn.ModifiedAt,
                Date = saleReturn.Date
            };
            customer.TotalDebt -= saleReturn.Balance;
            DbContext.Customers.AddOrUpdate(q => q.Id, customer);
            DbContext.CustomerAccounts.Add(transaction);
            SaveChanges();
        }

        private void UpdateSaleAndStock(SaleReturn saleReturn, Sale sale, bool delete)
        {
            foreach (var returnItem in saleReturn.Items)
            {
                foreach (var item in sale.Items.Where(item => item.Id == returnItem.Id))
                {
                    //Update Stock
                    var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == sale.StoreId);
                    var quantity = item.Unit * returnItem.Quantity;
                    if (stock == null)
                    {
                        if (delete)
                        {
                            throw new Exception($"Cancelling this sale return means that the product has to be taken from stock but this store does not have ${item.Product.Name} in stock.");
                        }
                        stock = new Stock { ProductId = item.ProductId, StoreId = sale.StoreId, CurrentStock = quantity };
                    }
                    else
                    {
                        if (delete)
                        {
                            if (stock.CurrentStock < quantity) throw new Exception($"This store has only ${stock.CurrentStock} in stock ");
                        }
                        stock.CurrentStock = (delete) ? stock.CurrentStock - quantity : stock.CurrentStock + quantity;
                    }
                    DbContext.Stocks.AddOrUpdate(stock);

                    var value = returnItem.Quantity * item.PriceSold;
                    sale.TotalCost = (delete) ? sale.TotalCost + value : sale.TotalCost - value;
                    sale.TotalAmount = (delete) ? sale.TotalAmount + value : sale.TotalAmount - value;
                    item.Quantity = (delete) ? item.Quantity + returnItem.Quantity : item.Quantity - returnItem.Quantity;
                    item.Amount = item.Quantity * item.PriceSold;

                    DbContext.SaleDetails.AddOrUpdate(q => q.Id, item);
                    DbContext.Entry(sale).State = EntityState.Modified;
                    DbContext.StockTallies.Add(new StockTally
                    {
                        Quantity = quantity,
                        Balance = stock.CurrentStock,
                        Source = StockSource.SalesReturn,
                        SourceId = sale.Id,
                        StockId = stock.Id,
                        TimeStamp = DateTime.UtcNow,
                        Notes = "Sales Return",
                        CreatedAt = DateTime.UtcNow,
                        CreatedBy = saleReturn.CreatedBy,
                        ModifiedBy = saleReturn.ModifiedBy,
                        ModifiedAt = saleReturn.ModifiedAt
                    });

                    DbContext.SaveChanges();
                }
            }
        }
        
        private static List<SaleReturnItem> MakeItems(SaleReturn sale)
        {
            if (sale.Items == null) return new List<SaleReturnItem>();
            return sale.Items.Select(item => new SaleReturnItem
            {
                Id = item.Id,
                Quantity = item.Quantity,
                ProductId = item.ProductId,
                Amount = item.Quantity * item.Unit,
                Unit = item.Unit
            }).ToList();
        }

        public override void Delete(long id)
        {
            var record = DbContext.SaleReturn.Include(x => x.Items).FirstOrDefault(x => x.Id == id);
            if (record == null) throw new Exception(ExceptionMessage.NotFound);
            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);

            var sale = DbContext.Sales.Find(record.Sale.Id);
            //Update Stock
            UpdateSaleAndStock(record, sale, true);

            //Delete Customer Transactions
            if (record.Sale.CustomerId.HasValue)
            {
                var customer = DbContext.Customers.Find(record.Sale.CustomerId.Value);
                var transactions = DbContext.CustomerAccounts
                    .Where(q => q.CustomerId == record.Sale.CustomerId.Value
                    && q.Source == CustomerTransaction.Sales
                    && q.RecordId == record.Id).ToList();

                //Modify Customer Total Debt
                customer.TotalDebt -= transactions.Sum(x => x.Debit);
                customer.TotalDebt += transactions.Sum(x => x.Credit);

                DbContext.CustomerAccounts.RemoveRange(transactions);
                DbContext.Customers.AddOrUpdate(customer);
            }

            DbSet.Remove(record);
            DbContext.SaveChanges();
        }
    }
}