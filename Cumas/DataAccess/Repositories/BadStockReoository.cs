using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class BadStockReoository : BaseRepository<BadStock>
    {
        public override void Insert(BadStock entity)
        {
            DbSet.Add(entity);
            var stock = DbContext.Stocks.FirstOrDefault(q => q.Product.Id == entity.ProductId && q.Store.Id == entity.StoreId);
            if (stock == null) throw new Exception("Invalid Operation. Product not in stock.");
            if (stock.CurrentStock < entity.Quantity) throw new Exception("Invalid Operation. Current stock in store is less than quantity.");
            stock.CurrentStock -= entity.Quantity;
            DbContext.Stocks.AddOrUpdate(stock);
            DbContext.StockTallies.Add(new StockTally
            {
                Quantity = entity.Quantity,
                Balance = stock.CurrentStock,
                Source = StockSource.BadStock,
                SourceId = entity.Id,
                StockId = stock.Id,
                TimeStamp = DateTime.UtcNow,
                Notes = "Bad Stock",
                CreatedAt = DateTime.UtcNow,
                CreatedBy = entity.CreatedBy,
                ModifiedBy = entity.ModifiedBy,
                ModifiedAt = entity.ModifiedAt
            });
            SaveChanges();
        }

        public override void Delete(long id)
        {
            var record = DbSet.FirstOrDefault(q => q.Id == id);
            if (record == null) throw new Exception(ExceptionMessage.NotFound);
            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);
            var stock = DbContext.Stocks.FirstOrDefault(q => q.Product.Id == record.ProductId && q.Store.Id == record.StoreId);
            if (stock == null) throw new Exception("Invalid Operation. Product not in stock.");
            stock.CurrentStock += record.Quantity;
            DbContext.Stocks.AddOrUpdate(stock);
            DbSet.Remove(record);
            SaveChanges();
        }
    }
}