﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class SupplierPaymentRepository : BaseRepository<SupplierPayment>
    {
        public override void Insert(SupplierPayment entity)
        {
            var amountAvailable = entity.Amount;
            //Fetch All UnPaid Purchases from a Supplier
            var supplier = DbContext.Suppliers.Find(entity.SupplierId);
            var purchases = DbContext.Purchases.Where(q => q.SupplierId == entity.SupplierId && q.Paid == false).ToList();

            // Update all purchases that the current payment would allow
            foreach (var purchase in purchases)
            {
                if (amountAvailable == 0) break; //Break When Amount == 0
                if (amountAvailable == purchase.Balance) //Amount available is equal to the balance
                {
                    purchase.Balance = 0;
                    purchase.Paid = true;
                    amountAvailable = 0;
                    //Update amount paid?
                }
                else if (amountAvailable > purchase.Balance) //Amount available is greater than the balance
                {
                    amountAvailable -= purchase.Balance;
                    purchase.Balance = 0;
                    purchase.Paid = true;
                }
                else //Amount available is less than the balance
                {
                    purchase.Balance -= amountAvailable;
                    amountAvailable = 0;
                }
                DbContext.Purchases.AddOrUpdate(purchase);
            }
            DbSet.Add(entity);
            DbContext.SaveChanges();

            //Update Supplier balance. This also takes care of negative balance where the store has overpaid the supplier
            supplier.TotalCredit -= entity.Amount;
            DbContext.Suppliers.AddOrUpdate(q => q.Id, supplier);
            DbContext.SaveChanges();



            var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" with Transaction Code: {entity.TransactionCode}" : string.Empty;

            //Update Supplier Account Record
            var transaction = new SupplierAccount
            {
                SupplierId = entity.SupplierId,
                Date = entity.Date,
                Credit = entity.Amount,
                Balance = supplier.TotalCredit,
                RecordId = entity.Id,
                Notes = $"Made a {entity.PaymentMode} Payment{transCode}.",
                Source = SupplierTransaction.SupplierPayment,
                CreatedBy = entity.CreatedBy,
                ModifiedBy = entity.ModifiedBy
            };
            DbContext.SupplierAccounts.Add(transaction);

            if (entity.BankAccountId > 0)
            {
                var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);
                bankAccount.Balance -= entity.Amount;
                
                var bankTransaction = new BankTransaction
                {
                    Date = entity.Date,
                    Account = bankAccount,
                    Amount = entity.Amount,
                    StoreId = entity.StoreId,
                    Credit = entity.Amount,
                    Debit = 0,
                    Balance = bankAccount.Balance,
                    Transaction = Transaction.Withdrawal,
                    Source = TransactionSource.SupplierPayment,
                    SourceId = entity.Id,
                    Note = $"Payment made to supplier{transCode}.",
                    CreatedBy = entity.ModifiedBy,
                    ModifiedBy = entity.ModifiedBy,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow
                };

                DbContext.BankTransactions.Add(bankTransaction);
            }
            DbContext.SaveChanges();
        }
    }
}