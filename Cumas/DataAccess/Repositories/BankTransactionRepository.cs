using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class BankTransactionRepository : BaseRepository<BankTransaction>
    {
        public override void Insert(BankTransaction entity)
        {
            var account = DbContext.BankAccounts.Find(entity.AccountId);
            account.Balance = (entity.Transaction == Transaction.Deposit)
                ? account.Balance + entity.Amount
                : account.Balance - entity.Amount;

            entity.Account = account;
            DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public override void Update(BankTransaction entity)
        {
            var account = DbContext.BankAccounts.Find(entity.AccountId);
            account.Balance = (entity.Transaction == Transaction.Deposit)
                ? account.Balance - entity.OldAmount + entity.Credit
                : account.Balance + entity.OldAmount - entity.Debit;

            entity.Account = account;
            entity.Balance = account.Balance;
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public override void Delete(long id)
        {
            var record = DbSet.Find(id);

            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);
            if (record.SourceId.HasValue) throw new Exception($"Operation not allowed. This transaction can not be deleted because it has relation to a {record.Source} transaction.");

            var account = DbContext.BankAccounts.Find(record.AccountId);
            account.Balance -= record.Amount;
            DbContext.BankAccounts.AddOrUpdate(account);

            DbSet.Remove(record);
            DbContext.SaveChanges();
        }
    }
}