using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class PurchaseRepository : BaseRepository<Purchase>
    {
        //private Supplier _supplier;

        public override void Insert(Purchase entity)
        {
            var supplier = DbContext.Suppliers.FirstOrDefault(q => q.Id == entity.SupplierId);
            if (supplier == null) throw new Exception("Please select a supplier");

            entity.Items = MakeItems(entity);
            entity.Expenses = MakeExpenses(entity);

            //Update Balance to total amount
            entity.Balance = entity.TotalAmount - entity.AmountPaid;

            #region Update Product Cost Price
            foreach (var item in entity.Items)
            {
                if (item.SchemeId.HasValue)
                {
                    var scheme = DbContext.PackageSchemes.Find(item.SchemeId);
                    scheme.Cost = item.Cost;
                    DbContext.PackageSchemes.AddOrUpdate(scheme);
                }
                else
                {
                    var product = DbContext.Products.Find(item.ProductId);
                    product.Cost = item.Cost;
                    DbContext.Products.AddOrUpdate(product);
                }
            }
            #endregion

            DbSet.Add(entity);
            //IncreaseStock(entity); NB: [ALBERT] This code is no longer necessary.

            SaveChanges(); //NB: This seems neccessary because the purchase must be save to get its ID to use for operations below.

            var recieptNo = !string.IsNullOrEmpty(entity.ReceiptNumber)
                ? $" with receipt number: {entity.ReceiptNumber}"
                : string.Empty;

            DbContext.SupplierAccounts.Add(new SupplierAccount
            {
                SupplierId = entity.SupplierId,
                Date = entity.Date,
                RecordId = entity.Id,
                Credit = 0,
                Debit = entity.TotalAmount,
                Notes = $"Purchase{recieptNo}.",
                Balance = entity.TotalAmount,
                Source = SupplierTransaction.Purchase,
                CreatedBy = entity.CreatedBy,
                ModifiedBy = entity.ModifiedBy
            });

            if (entity.AmountPaid > 0)
            {
                DbContext.SupplierAccounts.Add(new SupplierAccount
                {
                    SupplierId = entity.SupplierId,
                    Date = entity.Date,
                    RecordId = entity.Id,
                    Credit = entity.AmountPaid,
                    Debit = 0,
                    Notes = $"Payment for purchase{recieptNo}.",
                    Source = SupplierTransaction.Purchase,
                    Balance = entity.Balance,
                    CreatedBy = entity.CreatedBy,
                    ModifiedBy = entity.ModifiedBy
                });

                DbContext.SupplierPayments.Add(new SupplierPayment
                {
                    StoreId = entity.StoreId,
                    Date = entity.Date,
                    SupplierId = entity.SupplierId,
                    Amount = entity.AmountPaid,
                    ReceiptNumber = entity.ReceiptNumber,
                    Balance = supplier.TotalCredit + entity.Balance,
                    CreatedBy = entity.CreatedBy,
                    ModifiedBy = entity.ModifiedBy
                });

                if (entity.BankAccountId > 0)
                {
                    var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);
                    bankAccount.Balance -= entity.AmountPaid;

                    var paymentMode = (entity.PaymentMode == PaymentMode.Other) ? bankAccount.Name : entity.PaymentMode.ToString();
                    var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" and Transaction Code: {entity.TransactionCode}" : string.Empty;
                    var bankTransaction = new BankTransaction
                    {
                        Date = entity.Date,
                        Account = bankAccount,
                        Amount = entity.AmountPaid,
                        StoreId = entity.StoreId,
                        Credit = entity.AmountPaid,
                        Debit = 0,
                        Balance = bankAccount.Balance,
                        Transaction = Transaction.Withdrawal,
                        Source = TransactionSource.Purchase,
                        SourceId = entity.Id,
                        Note = $"Made purchase with {paymentMode}{transCode}.",
                        CreatedBy = entity.ModifiedBy,
                        ModifiedBy = entity.ModifiedBy,
                        CreatedAt = DateTime.UtcNow,
                        ModifiedAt = DateTime.UtcNow
                    };

                    DbContext.BankTransactions.Add(bankTransaction);
                }
            }

            supplier.TotalCredit += entity.Balance;
            DbContext.Suppliers.AddOrUpdate(supplier);
            SaveChanges();
        }

        private void IncreaseStock(Purchase entity)
        {
            foreach (var item in entity.Items)
            {
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.StoreId);
                var quantity = item.Quantity * item.Unit;
                if (stock != null)
                {
                    stock.CurrentStock += quantity;
                }
                else
                {
                    stock = new Stock
                    {
                        ProductId = item.ProductId,
                        StoreId = entity.StoreId,
                        CurrentStock = quantity
                    };
                }
                DbContext.Stocks.AddOrUpdate(stock);
            }
        }

        private void DecreaseStock(Purchase entity)
        {
            foreach (var item in entity.Items)
            {
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.StoreId);
                var quantity = item.Quantity * item.Unit;
                if (stock != null)
                {
                    stock.CurrentStock -= quantity;
                    if (stock.CurrentStock < 0) throw new Exception("Deleting this purchase is not allowed because it will result in negative stock.");
                }

                DbContext.Stocks.AddOrUpdate(stock);
            }
        }

        public override void Delete(long id)
        {
            var record = DbSet.Include(x => x.Items).Include(x => x.Expenses).FirstOrDefault(q => q.Id == id);
            if (record == null) throw new Exception(ExceptionMessage.NotFound);
            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);
            DecreaseStock(record);
            DbContext.PurchaseItems.RemoveRange(record.Items);
            DbContext.Expenses.RemoveRange(record.Expenses);

            var supplierAccountEntries = DbContext.SupplierAccounts
                .Where(q => q.Source == SupplierTransaction.Purchase && q.RecordId == record.Id)
                .ToList();

            var amountOnPurchase = supplierAccountEntries.LastOrDefault()?.Balance ?? 0;
            DbContext.SupplierAccounts.RemoveRange(supplierAccountEntries);

            var supplier = DbContext.Suppliers.Find(record.SupplierId);
            supplier.TotalCredit -= amountOnPurchase;
            DbContext.Suppliers.AddOrUpdate(supplier);

            var bankTransactions = DbContext.BankTransactions
                .Where(q => q.Source == TransactionSource.Purchase && q.SourceId == record.Id)
                .ToList();

            var bankAccount = bankTransactions.FirstOrDefault()?.Account;
            if (bankAccount != null)
            {
                bankAccount.Balance += bankTransactions.Sum(a => a.Amount);
                DbContext.BankAccounts.AddOrUpdate(bankAccount);
            }

            DbContext.BankTransactions.RemoveRange(bankTransactions);

            DbSet.Remove(record);
            SaveChanges();
        }

        private List<PurchaseItem> MakeItems(Purchase purchase)
        {
            var items = new List<PurchaseItem>();
            if (purchase.Items == null) return items;
            foreach (var item in purchase.Items)
            {
                var pItem = new PurchaseItem
                {
                    Id = item.Id,
                    Quantity = item.Quantity,
                    ProductId = item.ProductId,
                    Unit = item.Unit,
                    PackageId = item.PackageId,
                    Cost = item.Cost,
                    PurchaseId = purchase.Id,
                    Desc = item.Desc,
                    ExpireDate = item.ExpireDate,
                    Amount = item.Quantity * item.Cost,
                    SchemeId = item.SchemeId
                };

                items.Add(pItem);

                var stockQuantity = item.Quantity * item.Unit;
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == purchase.StoreId)
                    ?? new Stock { ProductId = item.ProductId, CurrentStock = 0, StoreId = purchase.StoreId };
                stock.CurrentStock += stockQuantity;

                DbContext.StockTallies.Add(new StockTally
                {
                    Quantity = stockQuantity,
                    Balance = stock.CurrentStock,
                    Source = StockSource.Purchase,
                    SourceId = purchase.Id,
                    SourceReference = purchase.ReceiptNumber,
                    Stock = stock,
                    TimeStamp = DateTime.UtcNow,
                    Notes = "Purchase of stock items",
                    CreatedAt = DateTime.UtcNow,
                    CreatedBy = purchase.CreatedBy,
                    ModifiedBy = purchase.ModifiedBy,
                    ModifiedAt = purchase.ModifiedAt
                });
            }
            return items;
        }

        private static List<Expense> MakeExpenses(Purchase purchase)
        {
            if (purchase.Expenses == null) return new List<Expense>();
            return purchase.Expenses.Select(expense => new Expense
            {
                Id = expense.Id,
                TypeId = expense.TypeId,
                Date = purchase.Date,
                Amount = expense.Amount,
                StoreId = purchase.StoreId,
                Notes = expense.Notes,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow,
                CreatedBy = purchase.CreatedBy,
                ModifiedBy = purchase.ModifiedBy
            }).ToList();
        }

    }
}