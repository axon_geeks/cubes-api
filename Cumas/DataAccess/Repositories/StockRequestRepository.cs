using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class StockRequestRepository : BaseRepository<StockRequest>
    {
        public override void Insert(StockRequest entity)
        {
            entity.Items = MakeItems(entity);
            DbSet.Add(entity);
            SaveChanges();
        }

        public override void Update(StockRequest entity)
        {
            var theStockRequest = DbContext.StockRequests.Include(x => x.Items)
                .FirstOrDefault(x => x.Id == entity.Id);
            if (theStockRequest == null) throw new Exception("Record not found to update.");
            var items = MakeItems(entity);
            theStockRequest.Date = entity.Date;
            theStockRequest.StoreId = entity.StoreId;
            theStockRequest.Notes = entity.Notes;
            theStockRequest.Status = entity.Status;

            items.ForEach(q => DbContext.StockRequestItems.AddOrUpdate(q));
            DbContext.Entry(theStockRequest).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        private static List<StockRequestItem> MakeItems(StockRequest request)
        {
            if (request.Items == null) return new List<StockRequestItem>();
            return request.Items.Select(item => new StockRequestItem
            {
                Id = item.Id,
                Quantity = item.Quantity,
                ProductId = item.ProductId,
                Remarks = item.Remarks,
                StockRequestId = request.Id,
                SchemeId = (item.SchemeId != 0) ? item.SchemeId : null
            }).ToList();

        }
    }
}