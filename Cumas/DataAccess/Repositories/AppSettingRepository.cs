﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cumas.AxHelpers;
using Cumas.DataAccess.Filters;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class AppSettingRepository:BaseRepository<AppSetting>
    {
        public AppSetting Get(string name)
        {
            return DbSet.FirstOrDefault(q => q.Name == name);
        }
    }

}