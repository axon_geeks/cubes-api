using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class PurchaseOrderRepository : BaseRepository<PurchaseOrder>
    {
        public override void Insert(PurchaseOrder entity)
        {
            entity.Items = MakeItems(entity);
            DbSet.Add(entity);
            SaveChanges();
        }

        public override void Update(PurchaseOrder entity)
        {
            var thePurchaseOrder = DbContext.PurchaseOrders.Include(x => x.Items)
                .FirstOrDefault(x => x.Id == entity.Id);
            if (thePurchaseOrder == null) throw new Exception("Record not found to update.");
            var items = MakeItems(entity);
            thePurchaseOrder.Date = entity.Date;
            thePurchaseOrder.StoreId = entity.StoreId;
            thePurchaseOrder.Notes = entity.Notes;
            thePurchaseOrder.SupplierId = entity.SupplierId;
            thePurchaseOrder.TotalAmount = entity.TotalAmount;
            thePurchaseOrder.ModifiedBy = entity.ModifiedBy;
            thePurchaseOrder.Status = entity.Status;

            items.ForEach(q => DbContext.PurchaseOrderItems.AddOrUpdate(q));
            DbContext.Entry(thePurchaseOrder).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        private static List<PurchaseOrderItem> MakeItems(PurchaseOrder request)
        {
            if (request.Items == null) return new List<PurchaseOrderItem>();
            return request.Items.Select(item => new PurchaseOrderItem
            {
                Id = item.Id,
                Quantity = item.Quantity,
                ProductId = item.ProductId,
                PurchaseOrderId = request.Id,
                SchemeId = item.SchemeId,
                Cost = item.Cost,
                Amount = item.Quantity * item.Cost
            }).ToList();

        }
    }
}