using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class StockIssueRepository : BaseRepository<StockIssue>
    {
        public override void Insert(StockIssue entity)
        {
            using (var db = DbContext.Database.BeginTransaction())
            {
                try
                {
                    entity.Items = MakeItems(entity);
                    var tallies = new List<StockTally>();
                    foreach (var item in entity.Items)
                    {
                        var quantity = item.Unit * item.Quantity;

                        var outStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.OriginId);
                        if (outStock == null) throw new Exception("This store doesn't have stock of this products.");
                        if (outStock.CurrentStock < quantity) throw new Exception("Current stock is lesser than issue quantity.");
                        outStock.CurrentStock -= quantity;
                        DbContext.Stocks.AddOrUpdate(outStock);

                        var inStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == entity.DestinationId) ?? new Stock
                        {
                            StoreId = entity.DestinationId,
                            ProductId = item.ProductId,
                            CurrentStock = 0
                        };

                        inStock.CurrentStock += quantity;

                        DbContext.Stocks.AddOrUpdate(inStock);
                        DbContext.SaveChanges();

                        tallies.AddRange(new List<StockTally>
                        {
                            new StockTally {
                                Quantity = quantity,
                                Balance = outStock.CurrentStock,
                                Source = StockSource.StockIssue,
                                SourceId = entity.Id,
                                SourceReference = entity.Number,
                                StockId = outStock.Id,
                                TimeStamp = DateTime.UtcNow,
                                Notes = "Stock Issue (Transfered)",
                                CreatedAt = DateTime.UtcNow,
                                CreatedBy = entity.CreatedBy,
                                ModifiedBy = entity.ModifiedBy,
                                ModifiedAt = entity.ModifiedAt
                            },
                            new StockTally {
                                Quantity = quantity,
                                Balance = inStock.CurrentStock,
                                Source = StockSource.StockIssue,
                                SourceId = entity.Id,
                                SourceReference = entity.Number,
                                StockId = inStock.Id,
                                TimeStamp = DateTime.UtcNow,
                                Notes = "Stock Issue (Received)",
                                CreatedAt = DateTime.UtcNow,
                                CreatedBy = entity.CreatedBy,
                                ModifiedBy = entity.ModifiedBy,
                                ModifiedAt = entity.ModifiedAt
                            }
                        });
                    }

                    DbSet.Add(entity);
                    DbContext.StockTallies.AddRange(tallies);
                    SaveChanges();
                    db.Commit();
                }
                catch (Exception)
                {
                    db.Rollback();
                    throw;
                }
            }


        }

        public override void Update(StockIssue entity)
        {
            throw new NotImplementedException("Coming Soon!!!");
            var theStockIssue = DbContext.StockIssues.FirstOrDefault(x => x.Id == entity.Id);
            if (theStockIssue == null) throw new Exception("Record not found to update.");
            theStockIssue.Status = entity.Status;
            theStockIssue.ModifiedBy = entity.ModifiedBy;
            foreach (var item in entity.Items)
            {
                var stock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId ==
                    ((entity.Status == IssueStatus.Delivered) ? entity.DestinationId : entity.OriginId));
                var quantity = item.Unit * item.Quantity;
                if (stock != null) { stock.CurrentStock += quantity; }
                else
                {
                    stock = new Stock
                    {
                        CurrentStock = quantity,
                        ProductId = item.ProductId,
                        StoreId = entity.DestinationId
                    };
                }
                DbContext.Stocks.AddOrUpdate(stock);
            }

            DbContext.Entry(theStockIssue).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public override void Delete(long id)
        {
            var record = DbSet.Include(x => x.Items).FirstOrDefault(q => q.Id == id);
            if (record == null) throw new Exception(ExceptionMessage.NotFound);
            if (record.Locked) throw new Exception(ExceptionMessage.RecordLocked);
            if (record.Status == IssueStatus.Delivered) throw new Exception("Delivered stock issues can not be deleted.");

            foreach (var item in record.Items)
            {
                //Return stock issued
                var outStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == record.OriginId);
                if (outStock != null) { outStock.CurrentStock += (item.Unit * item.Quantity); }
                DbContext.Stocks.AddOrUpdate(outStock);

                //Take out stock
                var inStock = DbContext.Stocks.FirstOrDefault(q => q.ProductId == item.ProductId && q.StoreId == record.DestinationId);
                if (inStock != null) { inStock.CurrentStock -= (item.Unit * item.Quantity); }
                DbContext.Stocks.AddOrUpdate(inStock);
            }

            DbSet.Remove(record);
            SaveChanges();
        }

        private static List<StockIssueItem> MakeItems(StockIssue request)
        {
            if (request.Items == null) return new List<StockIssueItem>();
            return request.Items.Select(item => new StockIssueItem
            {
                Id = item.Id,
                Quantity = item.Quantity,
                ProductId = item.ProductId,
                StockIssueId = request.Id,
                SchemeId = item.SchemeId,
                Unit = item.Unit
            }).ToList();

        }
    }
}