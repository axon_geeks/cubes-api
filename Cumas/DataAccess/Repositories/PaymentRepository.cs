﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Cumas.DataAccess.Filters;
using Cumas.Models;

namespace Cumas.DataAccess.Repositories
{
    public class PaymentRepository : BaseRepository<Payment>
    {
        public override void Insert(Payment entity)
        {
            //Get Payment ID
            DbSet.Add(entity);
            DbContext.SaveChanges();
            
            var amountAvailable = entity.Amount;

            //Fetch All UnPaid Sales Of Customer
            var customer = DbContext.Customers.Find(entity.CustomerId);
            var sales = DbContext.Sales.Where(q => q.CustomerId == entity.CustomerId && q.Paid == false).ToList();
            var invoicePayments = new List<InvoicePayment>();
            foreach (var sale in sales)
            {
                if (amountAvailable == 0) break; //Break When Amount == 0

                if (amountAvailable == sale.ActualBalance)
                {
                    invoicePayments.Add(new InvoicePayment { SaleId = sale.Id, Amount = sale.ActualBalance,PaymentId = entity.Id });
                    sale.ActualBalance = 0;
                    sale.Paid = true;
                    amountAvailable = 0;
                }
                else if (amountAvailable > sale.ActualBalance)
                {
                    invoicePayments.Add(new InvoicePayment { SaleId = sale.Id, Amount = sale.ActualBalance,PaymentId = entity.Id});
                    sale.ActualBalance = 0;
                    sale.Paid = true;
                    amountAvailable -= sale.ActualBalance;
                }
                else
                {
                    invoicePayments.Add(new InvoicePayment { SaleId = sale.Id, Amount = amountAvailable , PaymentId = entity.Id });
                    sale.ActualBalance -= amountAvailable;
                    amountAvailable = 0;
                }
                DbContext.Sales.AddOrUpdate(sale);
            }

            entity.InvoicePayments = invoicePayments;
            DbContext.SaveChanges();

            DbContext.CustomerAccounts.Add(new CustomerAccount
            {
                CustomerId = entity.CustomerId,
                Debit = 0,
                Credit = entity.Amount,
                Balance = customer.TotalDebt - entity.Amount,
                RecordId = entity.Id,
                Source = CustomerTransaction.Payments,
                Notes = entity.ReceiptNumber,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = entity.CreatedBy,
                ModifiedBy = entity.ModifiedBy,
                ModifiedAt = entity.ModifiedAt,
                Date = entity.Date
            });

            if (entity.BankAccountId > 0)
            {
                var bankAccount = DbContext.BankAccounts.Find(entity.BankAccountId);
                bankAccount.Balance += entity.Amount;

                var transCode = !string.IsNullOrEmpty(entity.TransactionCode) ? $" with Transaction Code: {entity.TransactionCode}" : string.Empty;
                var bankTransaction = new BankTransaction
                {
                    Date = entity.Date,
                    Account = bankAccount,
                    Amount = entity.Amount,
                    StoreId = entity.StoreId,
                    Credit = 0,
                    Debit = entity.Amount,
                    Balance = bankAccount.Balance,
                    Transaction = Transaction.Deposit,
                    Source = TransactionSource.CustomerPayment,
                    SourceId = entity.Id,
                    Note = $"Customer credit payment{transCode}.",
                    CreatedBy = entity.ModifiedBy,
                    ModifiedBy = entity.ModifiedBy,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow
                };
                DbContext.BankTransactions.Add(bankTransaction);
            }

            //Reduce Customer Debt
            customer.TotalDebt -= entity.Amount;
            DbContext.Customers.AddOrUpdate(customer);
            DbContext.SaveChanges();
        }

        public override void Delete(long id)
        {
            // Get payment record
            var payment = DbContext.Payments.Find(id);
            if (payment == null) throw new Exception(ExceptionMessage.NotFound);
            if (payment.Locked) throw new Exception(ExceptionMessage.RecordLocked);

            //Revert Paid Invoices
            foreach (var inPmt in payment.InvoicePayments)
            {
                var sale = DbContext.Sales.Find(inPmt.SaleId);
                if (sale == null) throw new Exception(ExceptionMessage.NotFound);
                sale.ActualBalance = sale.ActualBalance + inPmt.Amount;
                sale.Paid = false;
                DbContext.Sales.AddOrUpdate(sale);
            }

            //Delete Credit from CustomerAccount
            var customerAccount = DbContext.CustomerAccounts.FirstOrDefault(x=>x.RecordId == payment.Id);
            DbContext.CustomerAccounts.Remove(customerAccount);

            //Update Customer total debt
            var customer = DbContext.Customers.Find(payment.CustomerId);
            customer.TotalDebt = customer.TotalDebt + payment.Amount;
            DbContext.Customers.AddOrUpdate(customer);

            DbSet.Remove(payment);
            DbContext.SaveChanges();
        }
    }
}