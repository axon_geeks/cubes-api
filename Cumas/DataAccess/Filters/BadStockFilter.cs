using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class BadStockFilter : Filter<BadStock>
    {
        public long ProductId;
        public long StoreId;
        public string Name;
        public string Code;

        public override IQueryable<BadStock> BuildQuery(IQueryable<BadStock> query)
        {
            if (ProductId > 0) query = query.Where(q => q.Product.Id == ProductId);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Product.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Product.Code.ToLower().Contains(Code.ToLower()));

            return query;
        }
    }
}