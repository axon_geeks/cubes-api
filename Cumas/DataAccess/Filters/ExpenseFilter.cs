﻿using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class ExpenseFilter : Filter<Expense>
    {
        public string Reference;
        public string ReceiptNumber;
        public long StoreId;
        public long TypeId;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public bool? CarriageInwards;

        public override IQueryable<Expense> BuildQuery(IQueryable<Expense> query)
        {
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.ReceiptNumber.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(ReceiptNumber)) query = query.Where(q => q.ReceiptNumber.ToLower() == ReceiptNumber.ToLower());
            if (TypeId > 0) query = query.Where(q => q.Type.Id == TypeId);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (CarriageInwards.HasValue)
            {
                query = CarriageInwards.Value ? query.Where(q => q.PurchaseId.HasValue) : query.Where(q => !q.PurchaseId.HasValue);
            }
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }


    public class BankTransactionFilter : Filter<BankTransaction>
    {
        public long AccountId;
        public long StoreId;
        public Transaction? Transaction;
        public TransactionSource? Source;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<BankTransaction> BuildQuery(IQueryable<BankTransaction> query)
        {
            if (Transaction.HasValue) query = query.Where(q => q.Transaction == Transaction.Value);
            if (Source.HasValue) query = query.Where(q => q.Source == Source.Value);
            if (AccountId > 0) query = query.Where(q => q.Account.Id == AccountId);
            if (StoreId > 0) query = query.Where(q => q.StoreId == StoreId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }

    public class BankAccountFilter : Filter<BankAccount>
    {
        public long Id;
        public bool? ChequeAccount;

        public override IQueryable<BankAccount> BuildQuery(IQueryable<BankAccount> query)
        {
            if (ChequeAccount.HasValue) query = query.Where(q => q.ChequeAccount == ChequeAccount.Value);
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}