using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class StockIssueFilter : Filter<StockIssue>
    {
        public string Reference;
        public string Number;
        public long StoreId;
        public long OriginId;
        public long DestinationId;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public IssueStatus? Status;

        public override IQueryable<StockIssue> BuildQuery(IQueryable<StockIssue> query)
        {
            if (!string.IsNullOrWhiteSpace(Number)) query = query.Where(x => x.Number.ToLower() == Number.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.Number.ToLower().Contains(Reference.ToLower()));
            if (OriginId > 0) query = query.Where(q => q.Origin.Id == OriginId);
            if (DestinationId > 0) query = query.Where(q => q.Destination.Id == DestinationId);
            if (StoreId > 0) query = query.Where(q => q.Destination.Id == StoreId || q.OriginId == StoreId);
            if (Status.HasValue) query = query.Where(q => q.Status == Status.Value);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}