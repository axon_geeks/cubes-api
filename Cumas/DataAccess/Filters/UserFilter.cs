using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class UserFilter : Filter<User>
    {
        public long ProfileId;

        public override IQueryable<User> BuildQuery(IQueryable<User> query)
        {
            if (ProfileId > 0) query = query.Where(q => q.Profile.Id == ProfileId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class StockDiaryFilter : Filter<StockDiary>
    {
        public long StoreId;
        public long StockId;
        public DateTime? StartDate; 
        public DateTime? EndDate;

        public override IQueryable<StockDiary> BuildQuery(IQueryable<StockDiary> query)
        {
            if (StoreId > 0) query = query.Where(q => q.Stock.StoreId == StoreId);
            if (StockId > 0) query = query.Where(q => q.Stock.Id == StockId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            return query;
        }
    }

    public class DailySaleSummaryFilter : Filter<DailySaleSummary>
    {
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<DailySaleSummary> BuildQuery(IQueryable<DailySaleSummary> query)
        {
            if (StoreId > 0) query = query.Where(q => q.StoreId == StoreId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            return query;
        }
    }

    public class PackageSchemeFilter : Filter<PackageScheme>
    {
        public long PackageId;

        public override IQueryable<PackageScheme> BuildQuery(IQueryable<PackageScheme> query)
        {
            if (PackageId > 0) query = query.Where(q => q.Package.Id == PackageId);

            return query;
        }
    }

}