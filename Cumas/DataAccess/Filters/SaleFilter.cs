﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cumas.Models;
using Microsoft.Ajax.Utilities;

namespace Cumas.DataAccess.Filters
{
    public class SaleFilter : Filter<Sale>
    {
        public long Id;
        public string Reference;
        public string InvoiceNumber;
        public string Personel;
        public long CustomerId;
        public long ProductId;
        public long StoreId;
        public long TypeId;
        public DateTime? Date;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public SaleStatus? Status;
        public PaymentMode? PaymentMode;
        public bool? Paid;

        public override IQueryable<Sale> BuildQuery(IQueryable<Sale> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(InvoiceNumber)) query = query.Where(x => x.InvoiceNumber.ToLower() == InvoiceNumber.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.InvoiceNumber.ToLower().Contains(Reference.ToLower()));
            if (Status.HasValue && Status.Value == SaleStatus.Paid) { query = query.Where(q => q.Paid == true); }
            if (Status.HasValue && Status.Value != SaleStatus.Paid) query = query.Where(q => q.Status == Status.Value);
            if (PaymentMode.HasValue) query = query.Where(q => q.PaymentMode == PaymentMode.Value);
            if (Paid.HasValue) query = query.Where(q => q.Paid == Paid.Value);
            if (CustomerId > 0) query = query.Where(q => q.Customer.Id == CustomerId);
            if (ProductId > 0) query = query.Where(q => q.Items.Select(x => x.ProductId).ToList().Contains(ProductId));
           // if (TypeId > 0) query = query.Where(q => q.Items.Any(i => i.Product.TypeId == TypeId));
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (!string.IsNullOrWhiteSpace(Personel)) query = query.Where(x => x.CreatedBy == Personel);
            if (Date.HasValue) { StartDate = Date; EndDate = Date; }
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}