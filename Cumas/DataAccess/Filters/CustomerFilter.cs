using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class CustomerFilter : Filter<Customer>
    {
        public string Name;
        public string PhoneNumber;
        public string Email;


        public override IQueryable<Customer> BuildQuery(IQueryable<Customer> query)
        {
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}