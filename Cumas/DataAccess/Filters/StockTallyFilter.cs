﻿using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class StockTallyFilter : Filter<StockTally>
    {
        public long StockId;
        public StockSource? Source;

        public override IQueryable<StockTally> BuildQuery(IQueryable<StockTally> query)
        {
            if (StockId > 0) query = query.Where(q => q.StockId == StockId);
            if (Source > 0) query = query.Where(q => q.Source == Source);
            return query;
        }
    }
}