using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class CustomerAccountFilter : Filter<CustomerAccount>
    {
        public long CustomerId;
        public long RecordId;
        public CustomerTransaction? Source;
        public DateTime? StartDate;
        public DateTime? EndDate;


        public override IQueryable<CustomerAccount> BuildQuery(IQueryable<CustomerAccount> query)
        {
            if (CustomerId > 0) query = query.Where(q => q.Customer.Id == CustomerId);
            if (RecordId > 0) query = query.Where(q => q.RecordId == RecordId);
            if (Source.HasValue) query = query.Where(q => q.Source == Source.Value);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}