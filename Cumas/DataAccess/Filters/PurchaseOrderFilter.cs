using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class PurchaseOrderFilter : Filter<PurchaseOrder>
    {
        public long Id;
        public string Reference;
        public string Number;
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public PurchaseOrderStatus? Status;

        public override IQueryable<PurchaseOrder> BuildQuery(IQueryable<PurchaseOrder> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(Number)) query = query.Where(x => string.Equals(x.Number, Number, StringComparison.CurrentCultureIgnoreCase));
            //if (!string.IsNullOrWhiteSpace(Number)) query = query.Where(x => x.Number.ToLower() == Number.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.Number.ToLower().Contains(Reference.ToLower()));
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (Status.HasValue) query = query.Where(q => q.Status == Status.Value);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}