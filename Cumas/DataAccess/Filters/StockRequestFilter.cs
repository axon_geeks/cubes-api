using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class StockRequestFilter:Filter<StockRequest>
    {
        public string Reference;
        public string Number;
        public RequestStatus? Status;
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<StockRequest> BuildQuery(IQueryable<StockRequest> query)
        {
            if (!string.IsNullOrWhiteSpace(Number)) query = query.Where(x => x.Number.ToLower() == Number.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.Number.ToLower().Contains(Reference.ToLower()));
            if (Status.HasValue) query = query.Where(q => q.Status == Status.Value);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}