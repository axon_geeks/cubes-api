﻿using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class SupplierPaymentFilter : Filter<SupplierPayment>
    {
        public string ReceiptNumber;
        public string TransactionCode;
        public string InvoiceRef;
        public long SupplierId;
        public long PurchaseId;
        public string PaidBy;
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<SupplierPayment> BuildQuery(IQueryable<SupplierPayment> query)
        {
            if (!string.IsNullOrEmpty(PaidBy)) query = query.Where(q => q.PaidBy.ToLower().Contains(PaidBy.ToLower()));
            if (!string.IsNullOrEmpty(ReceiptNumber)) query = query.Where(q => q.ReceiptNumber.ToLower() == ReceiptNumber.ToLower());
            if (!string.IsNullOrEmpty(TransactionCode)) query = query.Where(q => q.TransactionCode.ToLower() == TransactionCode.ToLower());
            if (!string.IsNullOrEmpty(InvoiceRef)) query = query.Where(q => q.InvoiceRef.ToLower() == InvoiceRef.ToLower());
            if (SupplierId > 0) query = query.Where(q => q.Supplier.Id == SupplierId);
            if (PurchaseId > 0) query = query.Where(q => q.PurchaseId == PurchaseId);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}