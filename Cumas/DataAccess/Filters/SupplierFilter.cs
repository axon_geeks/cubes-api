using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class SupplierFilter : Filter<Supplier>
    {
        public string Name;
        public string PhoneNumber;
        public string Email;


        public override IQueryable<Supplier> BuildQuery(IQueryable<Supplier> query)
        {
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}