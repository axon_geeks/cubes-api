using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class PurchaseFilter : Filter<Purchase>
    {
        public long Id;
        public string Reference;
        public string ReceiptNumber;
        public long StoreId;
        public long SupplierId;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public string PoNumber;

        public override IQueryable<Purchase> BuildQuery(IQueryable<Purchase> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(ReceiptNumber)) query = query.Where(x => x.ReceiptNumber.ToLower() == ReceiptNumber.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.ReceiptNumber.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrWhiteSpace(PoNumber)) query = query.Where(x => x.PurchaseOrder.Number.ToLower().Contains(PoNumber.ToLower()));
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (SupplierId > 0) query = query.Where(q => q.SupplierId == SupplierId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }

    public class PurchaseItemFilter : Filter<PurchaseItem>
    {
        public long Id;
        public string Reference;
        public string ReceiptNumber;
        public long StoreId;
        public long SupplierId;
        public long PackageId;
        public DateTime? Date;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public string PoNumber;
        public long ProductId;
        public long SchemeId;


        public override IQueryable<PurchaseItem> BuildQuery(IQueryable<PurchaseItem> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(ReceiptNumber)) query = query.Where(x => x.Purchase.ReceiptNumber.ToLower() == ReceiptNumber.ToLower());
            if (!string.IsNullOrWhiteSpace(Reference)) query = query.Where(x => x.Purchase.ReceiptNumber.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrWhiteSpace(PoNumber)) query = query.Where(x => x.Purchase.PurchaseOrder.Number.ToLower().Contains(PoNumber.ToLower()));
            if (PackageId > 0) query = query.Where(q => q.PackageId == PackageId);
            if (ProductId > 0) query = query.Where(q => q.ProductId == ProductId);
            if (SchemeId > 0) query = query.Where(q => q.SchemeId == SchemeId);
            if (StoreId > 0) query = query.Where(q => q.Purchase.Store.Id == StoreId);
            if (SupplierId > 0) query = query.Where(q => q.Purchase.SupplierId == SupplierId);
            if (Date.HasValue) { StartDate = Date; EndDate = Date; }
            if (StartDate.HasValue) query = query.Where(q => q.Purchase.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23).AddMinutes(59);
                query = query.Where(q => q.Purchase.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Purchase.Hidden);

            return query;
        }
    }
}