using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class StockFilter : Filter<Stock>
    {
        public string Name;
        public string Code;
        public long ProductId;
        public long StoreId;
        public long TypeId;
        public bool? IsLow;
        public bool? IsAvailable;
        public bool? IsHigh;
        public bool? Alert;
        public StockStatus? Status;

        public override IQueryable<Stock> BuildQuery(IQueryable<Stock> query)
        {
            if (ProductId > 0) query = query.Where(q => q.Product.Id == ProductId);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (TypeId > 0) query = query.Where(q => q.Product.TypeId == TypeId);
            if (IsLow.HasValue) query = query.Where(q => q.CurrentStock <= q.Reorder);
            if (IsHigh.HasValue) query = query.Where(q => q.MaximumStock > 0 && q.CurrentStock > q.MaximumStock);
            if (IsAvailable.HasValue) query = query.Where(q => q.CurrentStock > 0);
            if (Alert.HasValue) query = query.Where(q => q.CurrentStock <= q.Product.ReorderLevel);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Product.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Product.Code.ToLower().Contains(Code.ToLower()));
            if (Status.HasValue)
            {
                switch (Status.Value)
                {
                    case StockStatus.OverStock:
                        query = query.Where(q => q.MaximumStock > 0 && q.CurrentStock > q.MaximumStock);
                        break;
                    case StockStatus.Reorder:
                        query = query.Where(q => q.CurrentStock <= q.Reorder);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (IsAvailable.HasValue) query = query.Where(q => q.CurrentStock > 0);

            return query;
        }
    }

    public enum StockStatus
    {
        Reorder,
        OverStock
    }
}