using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class ProductFilter : Filter<Product>
    {
        public string Name;
        public string Code;
        public long TypeId;

        public override IQueryable<Product> BuildQuery(IQueryable<Product> query)
        {
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (TypeId > 0) query = query.Where(q => q.TypeId == TypeId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

}