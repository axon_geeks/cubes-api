﻿using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class PaymentFilter : Filter<Payment>
    {
        public string Reference;
        public string ReceiptNumber;
        public long CustomerId;
        public string Personel;
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public PaymentMode? PaymentMode;


        public override IQueryable<Payment> BuildQuery(IQueryable<Payment> query)
        {
            if (!string.IsNullOrEmpty(Reference)) query = query.Where(q => q.ReceiptNumber.ToLower().Contains(Reference.ToLower()));
            if (!string.IsNullOrEmpty(ReceiptNumber)) query = query.Where(q => q.ReceiptNumber.ToLower() == ReceiptNumber.ToLower());
            if (PaymentMode.HasValue) query = query.Where(q => q.PaymentMode == PaymentMode.Value);
            if (CustomerId > 0) query = query.Where(q => q.Customer.Id == CustomerId);
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            if (!string.IsNullOrWhiteSpace(Personel)) query = query.Where(x => x.CreatedBy == Personel);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}