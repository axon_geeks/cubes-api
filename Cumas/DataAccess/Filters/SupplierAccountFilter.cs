using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class SupplierAccountFilter : Filter<SupplierAccount>
    {
        public long SupplierId;
        public SupplierTransaction? Source;
        public DateTime? StartDate;
        public DateTime? EndDate;


        public override IQueryable<SupplierAccount> BuildQuery(IQueryable<SupplierAccount> query)
        {
            if (SupplierId > 0) query = query.Where(q => q.SupplierId == SupplierId);
            if (Source.HasValue) query = query.Where(q => q.Source == Source.Value);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}