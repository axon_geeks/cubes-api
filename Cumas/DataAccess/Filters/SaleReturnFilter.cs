﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class SaleReturnFilter : Filter<SaleReturn>
    {
        public long Id;
        public string InvoiceNumber;
        public string Personel;
        public long CustomerId;
        public long StoreId;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<SaleReturn> BuildQuery(IQueryable<SaleReturn> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(InvoiceNumber)) query = query.Where(x => x.Sale.InvoiceNumber.ToLower() == InvoiceNumber.ToLower());
            if (CustomerId > 0) query = query.Where(q => q.Sale.Customer.Id == CustomerId);
            if (StoreId > 0) query = query.Where(q => q.Sale.Store.Id == StoreId);
            if (!string.IsNullOrWhiteSpace(Personel)) query = query.Where(x => x.CreatedBy == Personel);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Hidden);

            return query;
        }
    }
}