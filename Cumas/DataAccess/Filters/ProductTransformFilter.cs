using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class ProductTransformFilter : Filter<ProductTransform>
    {
        public string Name { get; set; }
        public long StoreId;
        public long SourceId;
        public long EndProductId;
        public DateTime? StartDate;
        public DateTime? EndDate;

        public override IQueryable<ProductTransform> BuildQuery(IQueryable<ProductTransform> query)
        {
            if (StoreId > 0) query = query.Where(q => q.Store.Id == StoreId);
            //if (SourceId > 0) query = query.Where(q => q.Source.Id == SourceId);
            if (EndProductId > 0) query = query.Where(q => q.EndProduct.Id == EndProductId);
            if (StartDate.HasValue) query = query.Where(q => q.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(24);
                query = query.Where(q => q.Date <= EndDate.Value);
            }

            return query;
        }
    }
}