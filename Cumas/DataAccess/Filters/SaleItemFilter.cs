using System;
using System.Linq;
using Cumas.Models;

namespace Cumas.DataAccess.Filters
{
    public class SaleItemFilter : Filter<SaleItem>
    {
        public long Id;
        public string InvoiceNumber;
        public long CustomerId;
        public long SaleId;
        public long ProductId;
        public long PackageId;
        public long StoreId;
        public DateTime? Date;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public SaleStatus? Status;
        public bool? Paid;

        public override IQueryable<SaleItem> BuildQuery(IQueryable<SaleItem> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrWhiteSpace(InvoiceNumber)) query = query.Where(x => x.Sale.InvoiceNumber.ToLower() == InvoiceNumber.ToLower());
            if (Status.HasValue) query = query.Where(q => q.Sale.Status == Status.Value);
            if (Paid.HasValue) query = query.Where(q => q.Sale.Paid == Paid.Value);
            if (CustomerId > 0) query = query.Where(q => q.Sale.Customer.Id == CustomerId);
            if (PackageId > 0) query = query.Where(q => q.Scheme.PackageId == PackageId);
            if (ProductId > 0) query = query.Where(q => q.ProductId == ProductId);
            if (SaleId > 0) query = query.Where(q => q.SaleId == SaleId);
            if (StoreId > 0) query = query.Where(q => q.Sale.Store.Id == StoreId);
            if (Date.HasValue) { StartDate = Date; EndDate = Date; }
            if (StartDate.HasValue) query = query.Where(q => q.Sale.Date >= StartDate.Value);
            if (EndDate.HasValue)
            {
                EndDate = EndDate.Value.AddHours(23).AddMinutes(59);
                query = query.Where(q => q.Sale.Date <= EndDate.Value);
            }
            query = query.Where(q => !q.Sale.Hidden);

            return query;
        }
    }
}