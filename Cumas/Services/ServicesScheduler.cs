﻿using Quartz;
using Quartz.Impl;

namespace Cumas.Services
{
    public class ServicesScheduler
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var endOfDayService = JobBuilder.Create<EndOfDayService>().Build();

            var dayEndtrigger = TriggerBuilder.Create()
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(23, 00))
                .ForJob(endOfDayService)
                .Build();

            scheduler.ScheduleJob(endOfDayService, dayEndtrigger);
        }
    }
}