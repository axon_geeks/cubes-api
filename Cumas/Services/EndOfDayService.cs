﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Cumas.Models;
using NLog;
using Quartz;

namespace Cumas.Services
{
    public class EndOfDayService : IJob
    {
        protected readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("End of Day Service Started");
            try
            {
                using (var sysContext = new SysDbContext())
                {
                    var sDate = DateTime.Today;
                    var eDate = DateTime.Today.AddHours(23).AddMinutes(59);
                    var accounts = sysContext.Accounts.Where(q => q.Status == AccountStatus.Active).ToList();
                    Logger.Info($"No. of accounts to process end of day service: {accounts.Count}");

                    foreach (var account in accounts)
                    {
                        Logger.Info($"Processing account: {account.Name}.");
                        var accountContext = new AppDbContext(account.Subdomain);
                        foreach (var stock in accountContext.Stocks.ToList())
                        {
                            var yesterday = DateTime.Today.AddDays(-1);
                            var yesterdayDiary = accountContext.StockDiaries.FirstOrDefault(q => q.Date == yesterday);
                            var sales = accountContext.SaleDetails.Where(q => q.Sale.Date >= sDate && q.Sale.Date <= eDate && q.ProductId == stock.ProductId).Include(q=>q.Sale).ToList();
                            var purchases = accountContext.PurchaseItems.Where(q => q.Purchase.Date >= sDate && q.Purchase.Date <= eDate && q.ProductId == stock.ProductId).ToList();
                            var diary = accountContext.StockDiaries.FirstOrDefault(q => q.Date == DateTime.Today) ?? new StockDiary { Date = DateTime.Today };
                            diary.OpeningStock = yesterdayDiary?.ClosingStock ?? 0;
                            diary.ClosingStock = stock.CurrentStock;
                            diary.CreditedStock = sales.Where(q => q.Sale.Status == SaleStatus.CreditSale).ToList().Sum(q => (q.Quantity * q.Unit));
                            diary.QuantityPurchases = purchases.Sum(x => (x.Quantity * x.Unit));
                            diary.AmountPurchased = purchases.Sum(x => x.Amount);
                            diary.QuantitySold = sales.Sum(x => (x.Quantity * x.Unit));
                            diary.AmountSold = sales.Sum(x => x.PriceSold);
                            diary.Margin = diary.AmountSold - ((diary.OpeningStock + diary.QuantityPurchases) * stock.Product.Cost);
                            accountContext.StockDiaries.AddOrUpdate(diary);
                        }
                        Logger.Info($"No of stock items: {accountContext.Stocks.Count()}.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Background Process");
            }


            Logger.Info("End of Day Service Ended");
        }
    }
}