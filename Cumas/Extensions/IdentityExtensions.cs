﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.Extensions
{
    public static class IdentityExtensions
    {
        public static User AsAppUser(this IIdentity identity)
        {
            var user = new UserRepository().Get(identity.Name);
            return user;
        }
    }
}