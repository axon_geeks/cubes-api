using System.Collections.Generic;
using System.Web;
using Cumas.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cumas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public UserManager<User> UserManager { get; private set; }

        private static string GetSubdomain()
        {
            var url = HttpContext.Current.Request.Url.Host;
            var subdomain = url.Contains(".") ? url.Split('.').FirstOrDefault() : "demo";
            return subdomain;
        }

        public Configuration()
            : this(new UserManager<User>(new UserStore<User>(new AppDbContext(GetSubdomain()))))
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        public Configuration(UserManager<User> userManager) { UserManager = userManager; }

        protected override void Seed(AppDbContext context)
        {
            #region Roles [Privileges]
            var roles = new List<IdentityRole>
                        {
                            new IdentityRole { Name = Privileges.SaleRead },
                            new IdentityRole { Name = Privileges.SaleViewStockLevel},
                            new IdentityRole { Name = Privileges.SaleCreate },
                            new IdentityRole { Name = Privileges.SaleUpdate },
                            new IdentityRole { Name = Privileges.SaleDelete },
                            new IdentityRole { Name = Privileges.SaleUnscoped },
                            new IdentityRole { Name = Privileges.SaleReturnRead },
                            new IdentityRole { Name = Privileges.SaleReturnCreate },
                            new IdentityRole { Name = Privileges.SaleReturnUpdate },
                            new IdentityRole { Name = Privileges.SaleReturnDelete },
                            new IdentityRole { Name = Privileges.StockReturnRead },
                            new IdentityRole { Name = Privileges.StockReturnCreate },
                            new IdentityRole { Name = Privileges.StockReturnUpdate },
                            new IdentityRole { Name = Privileges.StockReturnDelete },
                            new IdentityRole { Name = Privileges.SupplierPaymentCreate },
                            new IdentityRole { Name = Privileges.SupplierPaymentRead },
                            new IdentityRole { Name = Privileges.SupplierPaymentUpdate },
                            new IdentityRole { Name = Privileges.SupplierPaymentDelete },
                            new IdentityRole { Name = Privileges.SalePayment },
                            new IdentityRole { Name = Privileges.ProductRead },
                            new IdentityRole { Name = Privileges.ProductCreate },
                            new IdentityRole { Name = Privileges.ProductUpdate },
                            new IdentityRole { Name = Privileges.ProductDelete },
                            new IdentityRole { Name = Privileges.SupplierRead },
                            new IdentityRole { Name = Privileges.SupplierCreate },
                            new IdentityRole { Name = Privileges.SupplierUpdate },
                            new IdentityRole { Name = Privileges.SupplierDelete },
                            new IdentityRole { Name = Privileges.BadStockRead },
                            new IdentityRole { Name = Privileges.BadStockCreate },
                            new IdentityRole { Name = Privileges.BadStockUpdate },
                            new IdentityRole { Name = Privileges.BadStockDelete },
                            new IdentityRole { Name = Privileges.ProductTransformRead },
                            new IdentityRole { Name = Privileges.ProductTransformCreate },
                            new IdentityRole { Name = Privileges.ProductTransformUpdate },
                            new IdentityRole { Name = Privileges.ProductTransformDelete },
                            new IdentityRole { Name = Privileges.MessageRead },
                            new IdentityRole { Name = Privileges.MessageCreate },
                            new IdentityRole { Name = Privileges.MessageUpdate },
                            new IdentityRole { Name = Privileges.MessageDelete },
                            new IdentityRole { Name = Privileges.PaymentRead },
                            new IdentityRole { Name = Privileges.PaymentCreate },
                            new IdentityRole { Name = Privileges.PaymentUpdate },
                            new IdentityRole { Name = Privileges.PaymentDelete },
                            new IdentityRole { Name = Privileges.CustomerRead },
                            new IdentityRole { Name = Privileges.CustomerCreate },
                            new IdentityRole { Name = Privileges.CustomerUpdate },
                            new IdentityRole { Name = Privileges.CustomerDelete },
                            new IdentityRole { Name = Privileges.BankTransactionRead },
                            new IdentityRole { Name = Privileges.BankTransactionCreate },
                            new IdentityRole { Name = Privileges.BankTransactionUpdate },
                            new IdentityRole { Name = Privileges.BankTransactionDelete },
                            new IdentityRole { Name = Privileges.StockIssueRead },
                            new IdentityRole { Name = Privileges.StockIssueCreate },
                            new IdentityRole { Name = Privileges.StockIssueUpdate },
                            new IdentityRole { Name = Privileges.StockIssueDelete },
                            new IdentityRole { Name = Privileges.StockIssueApprove },
                            new IdentityRole { Name = Privileges.StockRequestRead },
                            new IdentityRole { Name = Privileges.StockRequestCreate },
                            new IdentityRole { Name = Privileges.StockRequestUpdate },
                            new IdentityRole { Name = Privileges.StockRequestDelete },
                            new IdentityRole { Name = Privileges.StockRequestApprove },

                            new IdentityRole { Name = Privileges.StockBookRead },
                            new IdentityRole { Name = Privileges.StockBookCreate },
                            new IdentityRole { Name = Privileges.StockBookUpdate },
                            new IdentityRole { Name = Privileges.StockBookDelete },

                            new IdentityRole { Name = Privileges.ExpenseRead },
                            new IdentityRole { Name = Privileges.ExpenseCreate },
                            new IdentityRole { Name = Privileges.ExpenseUpdate },
                            new IdentityRole { Name = Privileges.ExpenseDelete },
                            new IdentityRole { Name = Privileges.PurchaseRead },
                            new IdentityRole { Name = Privileges.PurchaseCreate },
                            new IdentityRole { Name = Privileges.PurchaseUpdate },
                            new IdentityRole { Name = Privileges.PurchaseDelete },
                            new IdentityRole { Name = Privileges.PurchaseApprove },
                            new IdentityRole { Name = Privileges.PurchaseOrderRead },
                            new IdentityRole { Name = Privileges.PurchaseOrderCreate },
                            new IdentityRole { Name = Privileges.PurchaseOrderUpdate },
                            new IdentityRole { Name = Privileges.PurchaseOrderDelete },
                            new IdentityRole { Name = Privileges.PurchaseOrderApprove },
                            new IdentityRole { Name = Privileges.Accounting },
                            new IdentityRole { Name = Privileges.Dashboard },
                            new IdentityRole { Name = Privileges.Reports },
                            new IdentityRole { Name = Privileges.Settings },
                            new IdentityRole { Name = Privileges.MultiStore },
                            new IdentityRole { Name = Privileges.Inventory },
                            new IdentityRole { Name = Privileges.Administration }
                        };

            roles.ForEach(r => context.Roles.AddOrUpdate(q => q.Name, r));
            var a = "";
            roles.ForEach(q => a += q.Name + ",");
            #endregion

            #region App Roles
            var adminProfile = new Profile
            {
                Name = "Administrator",
                Notes = "Administrator Role",
                Privileges = a.Trim(','),
                Locked = true
            };
            #endregion

            #region Users
            var userManager = new UserManager<User>(new UserStore<User>(context))
            {
                UserValidator = new UserValidator<User>(UserManager)
                {
                    AllowOnlyAlphanumericUserNames = false
                }
            };

            //Admin User
            if (UserManager.FindByNameAsync("Admin").Result == null)
            {
                var res = userManager.CreateAsync(new User
                {
                    Name = "Administrator",
                    Profile = adminProfile,
                    UserName = "Admin",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    Locked = true,
                    StoreId = 1,
                    Store = new Store
                    {
                        Id = 1,
                        Name = "Main Store",
                        CreatedAt = DateTime.Now,
                        ModifiedAt = DateTime.UtcNow,
                        CreatedBy = "System",
                        ModifiedBy = "System",
                        PhoneNumber = "00000000",
                        Location = "Main Location"
                    }
                }, "admin@cubes");

                if (res.Result.Succeeded)
                {
                    var userId = userManager.FindByNameAsync("Admin").Result.Id;
                    roles.ForEach(q => userManager.AddToRole(userId, q.Name));
                }
            }

            #endregion

            #region Expense Types
            var types = new List<ExpenseType>
            {
                new ExpenseType { Name = "Electricity Bills",Notes = "", CreatedAt = DateTime.UtcNow, CreatedBy = "Admin", ModifiedAt = DateTime.UtcNow, ModifiedBy = "Admin"},
                new ExpenseType { Name = "Water Bill",Notes = "", CreatedAt = DateTime.UtcNow, CreatedBy = "Admin", ModifiedAt = DateTime.UtcNow, ModifiedBy = "Admin"},
            };

            foreach (var spec in types.Where(origin => !context.ExpenseTypes.Any(q => q.Name == origin.Name)))
            {
                context.ExpenseTypes.Add(spec);
            }
            #endregion

            #region Update Roles
            roles.ForEach(q => context.Roles.AddOrUpdate(q));
            #endregion

            #region AppSettings
            var appSettings = new List<AppSetting>
            {
                new AppSetting {Name = ConfigKeys.StoreName, Notes = "Cubes Main Store", Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
                new AppSetting {Name = ConfigKeys.StorePhoneNumber, Notes = "+000 000 00 0000", Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
                new AppSetting {Name = ConfigKeys.StoreEmail, Notes = "store@cubes.net",Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
                new AppSetting {Name = ConfigKeys.StoreAddress, Notes = "P.O. Box MP 1013", Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
                new AppSetting {Name = ConfigKeys.SeperatedSaleAndPaymentPoints, Notes = "No", Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
                new AppSetting {Name = ConfigKeys.Vat, Notes = "0.175", Locked = true, CreatedAt = DateTime.UtcNow, CreatedBy = "System", ModifiedAt = DateTime.UtcNow, ModifiedBy = "System"},
            };

            foreach (var setting in appSettings.Where(p => !context.AppSettings.Any(x => x.Name == p.Name)))
            {
                context.AppSettings.Add(setting);
            }
            #endregion

            //if (!context.BankAccounts.Any(x => x.Name == ConfigKeys.Cash))
            //{
            //    context.BankAccounts.Add(new BankAccount
            //    {
            //        Name = ConfigKeys.Cash,
            //        Bank = ConfigKeys.Cash,
            //        Branch = ConfigKeys.Cash,
            //        Number = "0000000000000",
            //        Locked = true,
            //        Hidden = true,
            //        CreatedAt = DateTime.UtcNow,
            //        CreatedBy = "System",
            //        ModifiedAt = DateTime.UtcNow,
            //        ModifiedBy = "System"
            //    });
            //}


            context.SaveChanges();
            base.Seed(context);
        }
    }
}
