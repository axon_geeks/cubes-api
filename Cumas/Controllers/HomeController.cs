﻿using System.Web.Mvc;
using Cumas.ApiControllers;

namespace Cumas.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var data = new ExpenseTypeController().Get();
            ViewBag.Title = "Home Page";
            ViewBag.Text = $"Expense Types: {data.Total}";

            return View();
        }
    }
}
