﻿namespace Cumas
{
    public static class AppConfig
    {
        public static Setting Setting { get; set; }
    }

    public class Setting
    {
        public SmsService SmsService { get; set; }
        public EmailService EmailService { get; set; }
    }

    public class SmsService
    {
        public string Gateway { get; set; }
        public string ApiKey { get; set; }
        public string SenderId { get; set; }
        public bool Active { get; set; }
        public string CreditBalance { get; set; }
    }

    public class EmailService
    {
        public string IncomingServer { get; set; }
        public string OutgoingServer { get; set; }
        public bool Active { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}