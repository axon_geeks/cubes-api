﻿using System;
using System.IO;
using Microsoft.Owin;
using Newtonsoft.Json;
using Owin;

[assembly: OwinStartup(typeof(Cumas.Startup))]

namespace Cumas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            LoadSettings();
        }

        private static void LoadSettings()
        {
            AppConfig.Setting = JsonConvert.DeserializeObject<Setting>
                (File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configs/AppSettings.json")));
        }
    }
}
