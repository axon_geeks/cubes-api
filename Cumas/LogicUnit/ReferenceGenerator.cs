﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.LogicUnit
{
    public class ReferenceGenerator
    {
        internal static string SalesReceipt()
        {
            /*
             * Reference Number ::: [INV/2015/MAY/0001]
             */
            var num = 0;
            var term = $"INV/{DateTime.Today.ToString("yyyy/MMM")}/".ToUpper();
            var app = new BaseRepository<Sale>().Get(new SaleFilter { Reference = term }).LastOrDefault();
            if (app != null) num = int.Parse(app.InvoiceNumber.Split('/').LastOrDefault() ?? "0");

            return $"{term}{(num + 1).ToString("0000").ToUpper()}";
        }

        internal static string PaymentReceipt()
        {
            /*
             * Reference Number ::: [PAY/2015/MAY/0001]
             */
            var num = 0;
            var term = $"PAY/{DateTime.Today.ToString("yyyy/MMM")}/".ToUpper();
            var app = new BaseRepository<Payment>().Get(new PaymentFilter { Reference = term }).LastOrDefault();
            if (app != null) num = int.Parse(app.ReceiptNumber.Split('/').LastOrDefault() ?? "0");

            return $"{term}{(num + 1).ToString("0000").ToUpper()}";
        }

        internal static string StockIssueNumber()
        {
            /*
             * Reference Number ::: [SI/2015/MAY/001]
             */
            var num = 0;
            var term = $"SI/{DateTime.Today.ToString("yyyy/MMM")}/".ToUpper();
            var app = new BaseRepository<StockIssue>().Get(new StockIssueFilter{ Reference = term }).LastOrDefault();
            if (app != null) num = int.Parse(app.Number.Split('/').LastOrDefault() ?? "0");

            return $"{term}{(num + 1).ToString("000").ToUpper()}";
        }

        internal static string StockRequestNumber()
        {
            /*
             * Reference Number ::: [ST/2015/MAY/001]
             */
            var num = 0;
            var term = $"SR/{DateTime.Today.ToString("yyyy/MMM")}/".ToUpper();
            var app = new BaseRepository<StockRequest>().Get(new StockRequestFilter { Reference = term }).LastOrDefault();
            if (app != null) num = int.Parse(app.Number.Split('/').LastOrDefault() ?? "0");

            return $"{term}{(num + 1).ToString("000").ToUpper()}";
        }

        internal static string PurchaseOrderNumber()
        {
            /*
             * Reference Number ::: [PO/2015/MAY/001]
             */
            var num = 0;
            var term = $"PO/{DateTime.Today.ToString("yyyy/MMM")}/".ToUpper();
            var app = new BaseRepository<PurchaseOrder>().Get(new PurchaseOrderFilter { Reference = term }).LastOrDefault();
            if (app != null) num = int.Parse(app.Number.Split('/').LastOrDefault() ?? "0");

            return $"{term}{(num + 1).ToString("000").ToUpper()}";
        }

    }
}