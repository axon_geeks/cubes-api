﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;
using WebGrease.Css.Extensions;

namespace Cumas.LogicUnit
{
    public class StoreKeeper
    {
        public static long GetCurrentStock(long productId, long storeId, long units)
        {
            var stock = new BaseRepository<Stock>().Find(new StockFilter { StoreId = storeId, ProductId = productId });
            return (stock?.CurrentStock ?? 0) * units;
        }


        public static void IncreaseStock(long productId, long storeId, long quantity)
        {
            var repo = new BaseRepository<Stock>();
            var stock = repo.Find(new StockFilter { StoreId = storeId, ProductId = productId }) ??
                        new Stock { CurrentStock = 0, StoreId = storeId, ProductId = productId };

            stock.CurrentStock += quantity;
            if (stock.Id > 0) repo.Update(stock);
            else repo.Insert(stock);
        }

        public static void DecreaseStock(long productId, long storeId, long quantity)
        {
            var repo = new BaseRepository<Stock>();
            var stock = repo.Find(new StockFilter { StoreId = storeId, ProductId = productId });
            if (stock == null) throw new Exception("Stock not avaliable");

            stock.CurrentStock -= quantity;
            if (stock.Id > 0) repo.Update(stock);
            else repo.Insert(stock);
        }

        public static void CloseSalesPeriod()
        {
            var stockRepo = new BaseRepository<Stock>();
            var diaryRepo = new BaseRepository<StockDiary>();
            var saleItemRepo = new BaseRepository<SaleItem>();
            var purchaseItemRepo = new BaseRepository<PurchaseItem>();
            var stocks = stockRepo.Get();
            foreach (var stock in stocks)
            {
                var yesterday = DateTime.Today.AddDays(-1);
                var yesterdayDiary = diaryRepo.Query().FirstOrDefault(q => q.Date == yesterday && q.StockId == stock.Id);
                var sales = saleItemRepo.Query(new SaleItemFilter { Date = DateTime.Today, ProductId = stock.ProductId }).Include(q => q.Sale).ToList();
                var purchases = purchaseItemRepo.Query(new PurchaseItemFilter { ProductId = stock.ProductId, Date = DateTime.Today }).ToList();
                var diary = diaryRepo.Query().FirstOrDefault(q => q.Date == DateTime.Today && q.StockId == stock.Id) ?? new StockDiary { Date = DateTime.Today };
                diary.StockId = stock.Id;
                diary.OpeningStock = yesterdayDiary?.ClosingStock ?? 0;
                diary.ClosingStock = stock.CurrentStock;
                diary.QuantityPurchases = purchases.Sum(x => (x.Quantity * x.Unit));
                diary.CreditedStock = sales.Where(q => q.Sale.Status == SaleStatus.CreditSale).Sum(q => (q.Quantity * q.Unit));
                diary.AmountPurchased = purchases.Sum(x => x.Amount);
                diary.QuantitySold = sales.Sum(x => (x.Quantity * x.Unit));
                diary.AmountSold = sales.Sum(x => x.PriceSold);
                diary.Margin = diary.AmountSold - ((diary.OpeningStock + diary.QuantityPurchases) * stock.Product.Cost);
                diaryRepo.Save(diary);
            }

            new SaleRepository().CloseSalePeriod();
        }


    }
}