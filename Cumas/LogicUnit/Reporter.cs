﻿using System.IO;
using HiQPdf;
using PdfSharp;
using PdfSharp.Drawing;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace Cumas.LogicUnit
{
    public class Reporter
    {
        public static byte[] GeneratePdf(string html, PdfPageSize size, bool landscape = false)
        {
            var htmlToPdfConverter = new HtmlToPdf();
            //htmlToPdfConverter.SerialNumber = "";
            htmlToPdfConverter.Document.PageSize = size;
            htmlToPdfConverter.Document.Margins = new PdfMargins(40);
            htmlToPdfConverter.Document.PageOrientation = landscape
                ? PdfPageOrientation.Landscape
                : PdfPageOrientation.Portrait;

            var css = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/ReportTemplates/ReportStyle.css"));
            var raw = $"<style type='text/css'>{css}</style>{html}";
            var dd = htmlToPdfConverter.ConvertHtmlToMemory(raw, "");
            return dd;
        }

        public static byte[] GeneratePdf(string html, PageSize size, bool landscape = false)
        {
            using (var ms = new MemoryStream())
            {
                var config = new PdfGenerateConfig
                {
                    PageOrientation = landscape ? PageOrientation.Landscape : PageOrientation.Portrait,
                    PageSize = size,
                    MarginBottom = 40,
                    MarginLeft = 40,
                    MarginTop = 20,
                    MarginRight = 40,
                };

                var css = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/ReportTemplates/ReportStyle.css"));
                var pdf = PdfGenerator.GeneratePdf(html, config, PdfGenerator.ParseStyleSheet(css));
                pdf.Save(ms);
                return ms.ToArray();
            }


            //var converter = new HtmlToPdf
            //{
            //    PrintOptions =
            //    {
            //        DPI = 300,
            //        PaperSize = PdfPrintOptions.PdfPaperSize.A4,
            //        Header = new PdfPrintOptions.PdfHeaderFooter {CenterText = "{page} of {total-pages}"}
            //    }
            //};
            //converter.PrintOptions.EnableJavaScript = true;
            //converter.PrintOptions.AllowScreenCss = false;
            // create a new pdf document converting the html code
            //var css = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/ReportTemplates/ReportStyle.css"));
            //var rawData = $"<style>{css}</style>{html}";

            //var doc = converter.RenderHtmlAsPdf(rawData);


            //// save pdf document
            //var response = doc.BinaryData;

            //// close pdf document
            //return response;
        }

        public static byte[] GeneratePdf(string html, double width, double height, int margin)
        {
            using (var ms = new MemoryStream())
            {
                var config = new PdfGenerateConfig
                {
                    MarginBottom = margin,
                    MarginLeft = margin,
                    MarginTop = 0,
                    MarginRight = margin,
                    ManualPageSize = new XSize(width, height)
                };

                var css = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/ReportTemplates/ReportStyle.css"));
                var pdf = PdfGenerator.GeneratePdf(html, config, PdfGenerator.ParseStyleSheet(css));
                pdf.Save(ms);
                return ms.ToArray();
            }
        }
    }
}