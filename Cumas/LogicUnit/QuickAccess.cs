﻿using System;
using System.Linq;
using Cumas.DataAccess.Filters;
using Cumas.DataAccess.Repositories;
using Cumas.Models;

namespace Cumas.LogicUnit
{
    public class QuickAccess
    {
        public static StoreInfo GetStoreInfo(long id)
        {
            var repo = new AppSettingRepository();
            if (id > 0)
            {
                var store = new BaseRepository<Store>().Get(id);
                return new StoreInfo
                {
                    Name = store.Name,
                    PhoneNumber = store.PhoneNumber,
                    Email = store.Email,
                    Address = store.Address,
                    ShowVat = store.ShowVat,
                    PrinterSet = store.PrinterSet,
                    FooterNote = store.ReceiptFooterNote,
                };
            }

            
            var info = new StoreInfo
            {
                Name = repo.Get(ConfigKeys.StoreName).Notes,
                PhoneNumber = repo.Get(ConfigKeys.StorePhoneNumber).Notes,
                Email = repo.Get(ConfigKeys.StoreEmail).Notes,
                Address = repo.Get(ConfigKeys.StoreAddress).Notes,
               
            };
            return info;
        }

        public static decimal GetStockLevel(long storeId, DateTime? startDate, DateTime? endDate)
        {
            var productRepo = new ProductRepository();
            var purchaseItemRepo = new BaseRepository<PurchaseItem>();
            var saleItemRepo = new BaseRepository<SaleItem>();

            decimal openingstock = 0;
            var stockRepo = new BaseRepository<Stock>();
            var products = productRepo.Get();
            foreach (var product in products)
            {
                var productCurrentStock = stockRepo.Find(new StockFilter { StoreId = storeId, ProductId = product.Id })?.CurrentStock ?? 0;

                var salesMadeInPeriod = saleItemRepo.Query(new SaleItemFilter
                {
                    StoreId = storeId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ProductId = product.Id
                });

                var stockSoldAfterPeriod = saleItemRepo.Query(new SaleItemFilter
                {
                    StoreId = storeId,
                    ProductId = product.Id,
                    StartDate = endDate?.Date.AddDays(1),
                    EndDate = DateTime.UtcNow
                }).ToList().Sum(q => q.Quantity * q.Unit);

                var stockSoldInPeriod = salesMadeInPeriod.Any() ? salesMadeInPeriod.Sum(q => q.Quantity * q.Unit) : 0;

                var periodPurchases = purchaseItemRepo.Query(new PurchaseItemFilter
                {
                    StoreId = storeId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ProductId = product.Id
                }).ToList();

                var stockPurchasedAfterPeriod = purchaseItemRepo.Query(new PurchaseItemFilter
                {
                    StoreId = storeId,
                    StartDate = endDate?.Date.AddDays(1),
                    EndDate = DateTime.UtcNow,
                    ProductId = product.Id
                }).ToList().Sum(x => x.Quantity * x.Unit);

                var stockQuantityPurchasedInPeriod = periodPurchases.Any() ? periodPurchases.Sum(x => x.Quantity * x.Unit) : 0;
                decimal firstPurchasePrice = 0;
                var firstPurchase = periodPurchases.FirstOrDefault();
                if (firstPurchase != null) firstPurchasePrice = (firstPurchase.Cost / firstPurchase.Unit);
                if (firstPurchasePrice == 0)
                {
                    var lastPurchase = purchaseItemRepo.Query(new PurchaseItemFilter
                    {
                        StoreId = storeId,
                        ProductId = product.Id
                    }).ToList().LastOrDefault();

                    if (lastPurchase != null) firstPurchasePrice = (lastPurchase.Cost / lastPurchase.Unit);
                }

                var stockAsAtPeriod = (productCurrentStock + stockSoldAfterPeriod) - stockPurchasedAfterPeriod;
                var periodOpeningStockQuantity = stockAsAtPeriod - (stockQuantityPurchasedInPeriod - stockSoldInPeriod);
                var periodOpeningStockAmount = periodOpeningStockQuantity * firstPurchasePrice;

                openingstock += periodOpeningStockAmount;
            }

            return openingstock;
        }
    }

    public class StockLevel
    {
        public decimal OpeningStock { get; set; }
        public decimal ClosingStock { get; set; }
    }

    public class ReportFilter
    {
        public long StoreId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}