﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Web;

namespace Cumas.LogicUnit
{
    public class BarcodeGenerator
    {
        readonly string _fontPath = HttpContext.Current.Server.MapPath(@"~/Code39Font/FREE3OF9.TTF");

        public byte[] Generate(string code)
        {
            var c39 = new Barcode();

            var ms = new MemoryStream();
            c39.FontFamilyName = "Free 3 of 9";
            c39.FontFileName = _fontPath;
            c39.FontSize = 25;
            c39.ShowCodeString = false;
            c39.Title = "";
            var objBitmap = c39.GenerateBarcode(code);
            objBitmap.Save(ms, ImageFormat.Png);

            return ms.GetBuffer();
        }

    }

    public class Barcode
    {
        private const int ItemSepHeight = 3;

        SizeF _titleSize = SizeF.Empty;
        SizeF _barCodeSize = SizeF.Empty;
        SizeF _codeStringSize = SizeF.Empty;

        #region Barcode Title

        public string Title { get; set; }

        public Font TitleFont { get; set; }

        #endregion

        #region Barcode code string

        public bool ShowCodeString { get; set; }

        public Font CodeStringFont { get; set; }

        #endregion

        #region Barcode Font

        private Font _c39Font;

        public string FontFileName { get; set; }

        public string FontFamilyName { get; set; }

        public float FontSize { get; set; } = 12;

        private Font Code39Font
        {
            get
            {
                if (_c39Font == null)
                {
                    // Load the barcode font			
                    var pfc = new PrivateFontCollection();
                    pfc.AddFontFile(FontFileName);
                    var family = new FontFamily(FontFamilyName, pfc);
                    _c39Font = new Font(family, FontSize);
                }
                return _c39Font;
            }
        }

        #endregion

        public Barcode()
        {
            Title = null;
            TitleFont = new Font("Arial", 10);
            CodeStringFont = new Font("Arial", 10);
        }

        #region Barcode Generation

        public Bitmap GenerateBarcode(string barCode)
        {

            var bcodeWidth = 0;
            var bcodeHeight = 0;

            // Get the image container...
            var bcodeBitmap = CreateImageContainer(barCode, ref bcodeWidth, ref bcodeHeight);
            var objGraphics = Graphics.FromImage(bcodeBitmap);

            // Fill the background			
            objGraphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, bcodeWidth, bcodeHeight));

            var vpos = 0;

            // Draw the title string
            if (Title != null)
            {
                objGraphics.DrawString(Title, TitleFont, new SolidBrush(Color.Black), XCentered((int)_titleSize.Width, bcodeWidth), vpos);
                vpos += (((int)_titleSize.Height) + ItemSepHeight);
            }
            // Draw the barcode
            objGraphics.DrawString(barCode, Code39Font, new SolidBrush(Color.Black), XCentered((int)_barCodeSize.Width, bcodeWidth), vpos);

            // Draw the barcode string
            if (ShowCodeString)
            {
                vpos += (((int)_barCodeSize.Height));
                objGraphics.DrawString(barCode, CodeStringFont, new SolidBrush(Color.Black), XCentered((int)_codeStringSize.Width, bcodeWidth), vpos);
            }

            // return the image...									
            return bcodeBitmap;
        }

        private Bitmap CreateImageContainer(string barCode, ref int bcodeWidth, ref int bcodeHeight)
        {
            // Create a temporary bitmap...
            var tmpBitmap = new Bitmap(1, 1, PixelFormat.Format32bppArgb);
            var objGraphics = Graphics.FromImage(tmpBitmap);

            // calculate size of the barcode items...
            if (Title != null)
            {
                _titleSize = objGraphics.MeasureString(Title, TitleFont);
                bcodeWidth = (int)_titleSize.Width;
                bcodeHeight = (int)_titleSize.Height + ItemSepHeight;
            }

            _barCodeSize = objGraphics.MeasureString(barCode, Code39Font);
            bcodeWidth = Max(bcodeWidth, (int)_barCodeSize.Width);
            bcodeHeight += (int)_barCodeSize.Height;

            if (ShowCodeString)
            {
                _codeStringSize = objGraphics.MeasureString(barCode, CodeStringFont);
                bcodeWidth = Max(bcodeWidth, (int)_codeStringSize.Width);
                bcodeHeight += (ItemSepHeight + (int)_codeStringSize.Height);
            }

            // dispose temporary objects...
            objGraphics.Dispose();
            tmpBitmap.Dispose();

            return (new Bitmap(bcodeWidth, bcodeHeight, PixelFormat.Format32bppArgb));
        }

        #endregion


        #region Auxiliary Methods

        private static int Max(int v1, int v2)
        {
            return (v1 > v2 ? v1 : v2);
        }

        private static int XCentered(int localWidth, int globalWidth)
        {
            return ((globalWidth - localWidth) / 2);
        }

        #endregion

    }
}