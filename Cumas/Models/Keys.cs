﻿namespace Cumas.Models
{
    public class Privileges
    {
        public const string SaleRead = "Sale.Read";
        public const string SaleViewStockLevel = "Sale.ViewStockLevel";
        public const string SaleUnscoped = "Sale.Unscoped";
        public const string SaleCreate = "Sale.Create";
        public const string SaleUpdate = "Sale.Update";
        public const string SaleDelete = "Sale.Delete";
        public const string SalePayment = "Sale.Payment";

        public const string PaymentRead = "Payment.Read";
        public const string PaymentCreate = "Payment.Create";
        public const string PaymentUpdate = "Payment.Update";
        public const string PaymentDelete = "Payment.Delete";

        public const string CustomerRead = "Customer.Read";
        public const string CustomerCreate = "Customer.Create";
        public const string CustomerUpdate = "Customer.Update";
        public const string CustomerDelete = "Customer.Delete";

        public const string ProductRead = "Product.Read";
        public const string ProductCreate = "Product.Create";
        public const string ProductUpdate = "Product.Update";
        public const string ProductDelete = "Product.Delete";

        public const string ProductTransformRead = "ProductTransform.Read";
        public const string ProductTransformCreate = "ProductTransform.Create";
        public const string ProductTransformUpdate = "ProductTransform.Update";
        public const string ProductTransformDelete = "ProductTransform.Delete";

        public const string StockIssueRead = "StockIssue.Read";
        public const string StockIssueCreate = "StockIssue.Create";
        public const string StockIssueUpdate = "StockIssue.Update";
        public const string StockIssueDelete = "StockIssue.Delete";
        public const string StockIssueApprove = "StockIssue.Approve";

        public const string StockRequestRead = "StockRequest.Read";
        public const string StockRequestCreate = "StockRequest.Create";
        public const string StockRequestUpdate = "StockRequest.Update";
        public const string StockRequestDelete = "StockRequest.Delete";
        public const string StockRequestApprove = "StockRequest.Approve";

        public const string PurchaseRead = "Purchase.Read";
        public const string PurchaseCreate = "Purchase.Create";
        public const string PurchaseUpdate = "Purchase.Update";
        public const string PurchaseDelete = "Purchase.Delete";
        public const string PurchaseApprove = "Purchase.Approve";

        public const string PurchaseOrderRead = "PurchaseOrder.Read";
        public const string PurchaseOrderCreate = "PurchaseOrder.Create";
        public const string PurchaseOrderUpdate = "PurchaseOrder.Update";
        public const string PurchaseOrderDelete = "PurchaseOrder.Delete";
        public const string PurchaseOrderApprove = "PurchaseOrder.Approve";

        public const string MessageRead = "Message.Read";
        public const string MessageCreate = "Message.Create";
        public const string MessageUpdate = "Message.Update";
        public const string MessageDelete = "Message.Delete";

        public const string SupplierRead = "Supplier.Read";
        public const string SupplierCreate = "Supplier.Create";
        public const string SupplierUpdate = "Supplier.Update";
        public const string SupplierDelete = "Supplier.Delete";

        public const string ExpenseRead = "Expense.Read";
        public const string ExpenseCreate = "Expense.Create";
        public const string ExpenseUpdate = "Expense.Update";
        public const string ExpenseDelete = "Expense.Delete";

        public const string BadStockRead = "BadStock.Read";
        public const string BadStockCreate = "BadStock.Create";
        public const string BadStockUpdate = "BadStock.Update";
        public const string BadStockDelete = "BadStock.Delete";

        public const string BankTransactionRead = "BankTransaction.Read";
        public const string BankTransactionCreate = "BankTransaction.Create";
        public const string BankTransactionUpdate = "BankTransaction.Update";
        public const string BankTransactionDelete = "BankTransaction.Delete";

        public const string SupplierPaymentRead = "SupplierPayment.Read";
        public const string SupplierPaymentCreate = "SupplierPayment.Create";
        public const string SupplierPaymentUpdate = "SupplierPayment.Update";
        public const string SupplierPaymentDelete = "SupplierPayment.Delete";

        public const string StockReturnRead = "StockReturn.Read";
        public const string StockReturnCreate = "StockReturn.Create";
        public const string StockReturnUpdate = "StockReturn.Update";
        public const string StockReturnDelete = "StockReturn.Delete";

        public const string SaleReturnRead = "SaleReturn.Read";
        public const string SaleReturnCreate = "SaleReturn.Create";
        public const string SaleReturnUpdate = "SaleReturn.Update";
        public const string SaleReturnDelete = "SaleReturn.Delete";

        public const string StockBookRead = "StockBook.Read";
        public const string StockBookCreate = "StockBook.Create";
        public const string StockBookUpdate = "StockBook.Update";
        public const string StockBookDelete = "StockBook.Delete";

        public const string Accounting = "Accounting";
        public const string Inventory = "Inventory";
        public const string Reports = "Reports";
        public const string Administration = "Administration";
        public const string Settings = "Settings";
        public const string Dashboard = "Dashboard";
        public const string MultiStore = "MultiStore";
    }

    public class GenericProperties
    {
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class SettingKeys
    {
        public static string Deposit = "Deposit";
        public static string Withdrawal = "Withdrawal";
    }

    public class DefaultKeys
    {
        public static string CurrencyFormat = "GHC ##,###.00";
        public static string FormatAmount = "##,###.00";
    }

    public class ConfigKeys
    {
        public static string Cash = "Cash";
        public static string StoreName = "Store Name";
        public static string StoreAddress = "Store Address";
        public static string StoreEmail = "Store Email";
        public static string StorePhoneNumber = "Store Phone Number";
        public static string SeperatedSaleAndPaymentPoints = "Seperated Sale And Payment Points";
        public static string Vat = "Vat";
    }

    public class StoreInfo
    {
        

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string FooterNote { get; set; }
        public bool ShowVat { get; set; }
        public PrinterSet PrinterSet { get; set; }
    }
}