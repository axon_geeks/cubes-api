﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cumas.Models
{
    public class AppDbContext : IdentityDbContext<User>
    {
        //NB: Use this for Production
        //public AppDbContext(string db) : base(
        //        "Data Source=tcp:s06.winhost.com;Initial Catalog=DB_99988_" +
        //        ((db == "www" || db == "axoncubes") ? "demo" : db) +
        //        "db;User ID=DB_99988_" +
        //        ((db == "www" || db == "axoncubes") ? "demo" : db) +
        //        "db_user;Password=d3v3l0p3rs;Integrated Security=False;"
        //        )
        //{ ContextProvider.DbName = db; }

        //NB: Development LocalHost
        //public AppDbContext(string db) : base($"server=.\\mssqlserver1; integrated security=SSPI; database=cubes{db}")
        //{
        //    ContextProvider.DbName = db;
        //}

        //NB: Use this for stage hosting
        public AppDbContext(string db) : base("AppDbContext") { }

        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ExpenseType> ExpenseTypes { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerAccount> CustomerAccounts { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleItem> SaleDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductTransform> ProductTransforms { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<InvoicePayment> InvoicePayments { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<SupplierAccount> SupplierAccounts { get; set; }
        public DbSet<SupplierPayment> SupplierPayments { get; set; }
        public DbSet<PurchaseDiscount> PurchaseDiscounts { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseItem> PurchaseItems { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<StockDiary> StockDiaries { get; set; }
        public DbSet<StockIssue> StockIssues { get; set; }
        public DbSet<StockIssueItem> StockIssueItems { get; set; }
        public DbSet<StockRequest> StockRequests { get; set; }
        public DbSet<StockRequestItem> StockRequestItems { get; set; }
        public DbSet<BadStock> BadStocks { get; set; }
        public DbSet<ProductPackage> ProductPackages { get; set; }
        public DbSet<PackageScheme> PackageSchemes { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<BankTransaction> BankTransactions { get; set; }
        public DbSet<SaleReturn> SaleReturn { get; set; }
        public DbSet<SaleReturnItem> SaleReturnItem { get; set; }
        public DbSet<StockTally> StockTallies { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<DailySaleSummary> DailySaleSummaries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductTransform>()
                 .HasRequired(q => q.EndProduct)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockIssue>()
                 .HasRequired(q => q.Origin)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockIssue>()
                 .HasRequired(q => q.Destination)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                 .HasRequired(q => q.Store)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasRequired(q => q.Package)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PackageScheme>()
                 .HasRequired(q => q.Package)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, Migrations.Configuration>());
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.CreatedAt = DateTime.UtcNow;
                entry.ModifiedAt = DateTime.UtcNow;
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.ModifiedAt = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }
    }

    public class ContextProvider
    {
        public static string DbName;
    }

    public class AppDbContextFactory : IDbContextFactory<AppDbContext>
    {
        public AppDbContext Create()
        {
            return new AppDbContext(ContextProvider.DbName);
        }
    }

    public class SysDbContext : DbContext
    {
        public SysDbContext() : base("SysDbContext") { }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<Audit> Audits { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SysDbContext, SysMigrations.Configuration>());
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.CreatedAt = DateTime.UtcNow;
                entry.ModifiedAt = DateTime.UtcNow;
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.ModifiedAt = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }
    }
}