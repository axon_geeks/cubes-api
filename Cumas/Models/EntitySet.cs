﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cumas.Models
{
    public interface IHasId
    {
        [Key]
        long Id { get; set; }
    }
    public interface ISecured
    {
        bool Locked { get; set; }
        bool Hidden { get; set; }
    }
    public interface IAuditable : IHasId
    {
        [Required]
        string CreatedBy { get; set; }
        [Required]
        string ModifiedBy { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime ModifiedAt { get; set; }
    }


    public class HasId : IHasId
    {
        public long Id { get; set; }
    }
    public class AuditFields : HasId, IAuditable, ISecured
    {
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string ModifiedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
    }
    public class LookUp : AuditFields
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
    }

    public class Account : HasId
    {
        [MaxLength(256), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(256), Required]
        public string ContactPerson { get; set; }
        [MaxLength(32)]
        public string PhoneNumber { get; set; }
        [MaxLength(512)]
        public string OtherNumbers { get; set; }
        [MaxLength(256)]
        public string Email { get; set; }
        public DateTime SignUp { get; set; }
        public AccountStatus Status { get; set; }
        public virtual AccountType Type { get; set; }
        [MaxLength(32), Required, Index(IsUnique = true)]
        public string Subdomain { get; set; }
        public DateTime? ExpireDate { get; set; }
    }

    public class AccountType : LookUp { }

    public enum AccountStatus
    {
        Active,
        Suspended,
        Cancelled
    }

    public class Audit : HasId
    {
        public DateTime Timestamp { get; set; }
        [MaxLength(128), Required]
        public string UserName { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public string PreviousData { get; set; }
        public string NewData { get; set; }
    }

    public class User : IdentityUser
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        public virtual Profile Profile { get; set; }
        public long ProfileId { get; set; }
        public virtual Store Store { get; set; }
        public long StoreId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }
    }

    public class Store : AuditFields
    {
        [Required, MaxLength(128), Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string PhoneNumber { get; set; }
        [MaxLength(128), Required]
        public string Location { get; set; }
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(512)]
        public string Manager { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        [MaxLength(512)]
        public string Address { get; set; }
        public bool AllowDiscountAtPayment { get; set; }
        public bool AllowDiscountOnUnitPrice { get; set; }
        public bool ShowVat { get; set; }
        public PrinterSet PrinterSet { get; set; }
        public List<User> Users { get; set; }
        public int? SaleReturnLimitDays { get; set; }
        [MaxLength(1028)]
        public string ReceiptFooterNote { get; set; }
        public int? TimeOut { get; set; }
        public virtual Tax Tax { get; set; }
        public long? TaxId { get; set; }
    }

    public enum PrinterSet
    {
        POSPrinter,
        A5InvoiceBook
    }

    public class Profile : HasId
    {
        [Required, MaxLength(512), Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        [MaxLength(500000)]
        public string Privileges { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
        public List<User> Users { get; set; }
    }
    public class AppSetting : LookUp { }

    public class ProductType : LookUp { }

    public class ProductCategory : LookUp
    {
        public long? ParentId { get; set; }
        public virtual ProductCategory Parent { get; set; }
        public List<Product> Products { get; set; }
    }

    public class ExpenseType : LookUp { }

    public class ProductPackage : LookUp
    {
        public List<Product> Products { get; set; }
        public List<PackageScheme> PackageSchemes { get; set; }
        public List<PurchaseItem> PurchaseItems { get; set; }
    }

    public class Customer : AuditFields
    {
        [MaxLength(128), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string ContactPerson { get; set; }
        [MaxLength(32)]
        public string ContactNumber { get; set; }
        [MaxLength(12)]
        public string CardNumber { get; set; }
        [MaxLength(256)]
        public string Email { get; set; }
        [MaxLength(20), Required]
        public string PhoneNumber { get; set; }
        [MaxLength(256)]
        public string ResidentialAddress { get; set; }
        [MaxLength(512)]
        public string PostalAddress { get; set; }
        public decimal TotalDebt { get; set; }
        public List<CustomerAccount> Accounts { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
    }

    public enum CustomerTransaction { Sales, Payments, SaleReturns }

    public class Expense : AuditFields
    {
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(32)]
        public string ReceiptNumber { get; set; }
        public long TypeId { get; set; }
        public virtual ExpenseType Type { get; set; }
        public decimal Amount { get; set; }
        public Purchase Purchase { get; set; }
        public long? PurchaseId { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public PaymentMode PaymentMode { get; set; }
        [MaxLength(128)]
        public string TransactionCode { get; set; }
        [NotMapped]
        public long BankAccountId { get; set; }
        [NotMapped]
        public decimal OldAmount { get; set; }
    }

    public class CustomerAccount : AuditFields
    {
        public long CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public CustomerTransaction Source { get; set; }
        public long RecordId { get; set; }
    }

    public class Sale : AuditFields
    {
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        [MaxLength(18), Required, Index(IsUnique = true)]
        public string InvoiceNumber { get; set; }
        [MaxLength(512)]
        public string Buyer { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public virtual List<SaleItem> Items { get; set; }
        public long? CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public decimal TotalCost { get; set; }
        public decimal Discount { get; set; }
        public virtual Tax Tax { get; set; }
        public long? TaxId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal Balance { get; set; }
        public decimal ActualBalance { get; set; }
        public SaleStatus Status { get; set; }
        public PaymentMode PaymentMode { get; set; }
        [MaxLength(128)]
        public string TransactionCode { get; set; }
        [NotMapped]
        public long BankAccountId { get; set; }
        public bool Paid { get; set; }
        public List<SaleReturn> SaleReturns { get; set; }
        [NotMapped]
        public long PaymentId { get; set; }
    }

    public class DailySaleSummary : HasId
    {
        public DateTime Date { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public virtual Store Store { get; set; }
        public long StoreId { get; set; }
        public decimal AmountExVat { get; set; }
        public decimal VatAmount { get; set; }
        public decimal SaleValue { get; set; }
    }

    public enum PaymentMode
    {
        Cash,
        Cheque,
        MobileMoney,
        BankCard,
        Credit,
        Other
    }

    public enum SaleStatus
    {
        Pending,
        CreditSale,
        Paid
    }

    public class SaleItem : HasId
    {
        public long SaleId { get; set; }
        public Sale Sale { get; set; }
        [MaxLength(256)]
        public string Desc { get; set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public long? SchemeId { get; set; }
        public virtual PackageScheme Scheme { get; set; }
        public decimal Price { get; set; }
        public decimal PriceSold { get; set; }
        public decimal Discount { get; set; }
        public int Quantity { get; set; }
        public long Unit { get; set; }
        public decimal Amount { get; set; }
        [NotMapped]
        public long PreviousQuantity { get; set; }
    }

    public class Product : AuditFields
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        public string Number { get; set; }
        [MaxLength(32)]
        public string Code { get; set; }
        public virtual ProductType Type { get; set; }
        public long TypeId { get; set; }
        public List<ProductCategory> Categories { get; set; }
        public decimal Price { get; set; }
        public decimal Margin { get; set; }
        public decimal MinPrice { get; set; }
        public long ReorderLevel { get; set; }
        public long MaximumStock { get; set; }
        public long CurrentStock { get; set; }
        public string Image { get; set; }
        public decimal Cost { get; set; }
        [MaxLength(128)]
        public string Barcode { get; set; } //NB: Not being used. Code is being used in place of barcode
        public List<ProductTransform> ProductTransforms { get; set; }
        public List<SaleItem> SalesDetails { get; set; }
        public List<PurchaseItem> PurchaseItems { get; set; }
        public List<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public List<BadStock> BadStocks { get; set; }
        public virtual ProductPackage Package { get; set; }
        public long PackageId { get; set; }
        public List<PackageScheme> PackageSchemes { get; set; }
    }

    public class PackageScheme : HasId
    {
        [MaxLength(128)]
        public string Barcode { get; set; }
        [MaxLength(32)]
        public string Desc { get; set; }
        public virtual ProductPackage Package { get; set; }
        public long PackageId { get; set; }
        public long Quantity { get; set; }
        public Product Product { get; set; }
        public long ProductId { get; set; }
        public decimal Price { get; set; }
        public decimal MinPrice { get; set; }
        public decimal Cost { get; set; }
        public List<SaleItem> SaleItems { get; set; }
    }

    public class ProductTransform : AuditFields
    {
        public DateTime Date { get; set; }
        public Store Store { get; set; }
        public long StoreId { get; set; }
        public virtual List<SourceProduct> SourceProducts { get; set; }
        public long EndProductId { get; set; }
        public virtual Product EndProduct { get; set; }
        public long Quantity { get; set; }
        public long ResultQuantity { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
    }

    public class SourceProduct : HasId
    {
        public ProductTransform ProductTransform { get; set; }
        public long ProductTransformId { get; set; }
        public virtual Product Product { get; set; }
        public long ProductId { get; set; }
        public long Quantity { get; set; }
    }

    public class Payment : AuditFields
    {
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        [Index(IsUnique = true), Required, MaxLength(20)]
        public string ReceiptNumber { get; set; }
        public DateTime Date { get; set; }
        public long CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public PaymentMode PaymentMode { get; set; }
        [MaxLength(128)]
        public string TransactionCode { get; set; }
        [NotMapped]
        public long BankAccountId { get; set; }
        [MaxLength(256)]
        public string PaidBy { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public virtual List<InvoicePayment> InvoicePayments { get; set; }
    }

    public class InvoicePayment : HasId
    {
        public long PaymentId { get; set; }
        public long SaleId { get; set; }
        public decimal Amount { get; set; }
    }

    public class Supplier : AuditFields
    {
        [MaxLength(64), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(32), Required]
        public string PhoneNumber { get; set; }
        [MaxLength(256)]
        public string ContactPerson { get; set; }
        [MaxLength(32)]
        public string ContactNumber { get; set; }
        [MaxLength(256)]
        public string Email { get; set; }
        [MaxLength(512)]
        public string Location { get; set; }
        [MaxLength(512)]
        public string PostalAddress { get; set; }
        public decimal TotalCredit { get; set; }
        public List<SupplierAccount> Accounts { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
    }

    public class SupplierAccount : AuditFields
    {
        public long SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public virtual SupplierTransaction Source { get; set; }
        public long RecordId { get; set; }
    }

    public enum SupplierTransaction { SupplierPayment, Purchase, PurchaseDiscount }

    public class SupplierPayment : AuditFields
    {
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        [MaxLength(20)]
        public string ReceiptNumber { get; set; }
        public DateTime Date { get; set; }
        public long SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public long PurchaseId { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        [MaxLength(256)]
        public string PaidBy { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public PaymentMode PaymentMode { get; set; }
        [MaxLength(128)]
        public string TransactionCode { get; set; }
        [MaxLength(512)]
        public string InvoiceRef { get; set; }
        [NotMapped]
        public long BankAccountId { get; set; }
    }

    public class PurchaseDiscount : AuditFields
    {
        public DateTime Date { get; set; }
        public long PurchaseId { get; set; }
        public virtual Purchase Purchase { get; set; }
        public decimal Amount { get; set; }
        public long PurchaseBalance { get; set; }
    }

    public class PurchaseOrder : AuditFields
    {
        public DateTime Date { get; set; }
        [MaxLength(32), Required, Index(IsUnique = true)]
        public string Number { get; set; }
        public long? StoreId { get; set; }
        public virtual Store Store { get; set; }
        public long SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public List<PurchaseOrderItem> Items { get; set; }
        public decimal TotalAmount { get; set; }
        public PurchaseOrderStatus Status { get; set; }
    }

    public enum PurchaseOrderStatus
    {
        Pending,
        Approved,
        Rejected,
        Delivered
    }

    public class PurchaseOrderItem : HasId
    {
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public decimal Cost { get; set; }
        public long Quantity { get; set; }
        public decimal Amount { get; set; }
        public PurchaseOrder PurchaseOrder { get; set; }
        public long PurchaseOrderId { get; set; }
        public virtual PackageScheme Scheme { get; set; }
        public long? SchemeId { get; set; }
    }

    public class Purchase : AuditFields
    {
        public DateTime Date { get; set; }
        [MaxLength(32)]
        public string ReceiptNumber { get; set; }
        [MaxLength(32)]
        public string InvoiceNumber { get; set; }
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        public long SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public virtual List<PurchaseItem> Items { get; set; }
        public float Tax { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal Balance { get; set; }
        public bool Paid { get; set; }
        public DateTime? DeadLine { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public long? PurchaseOrderId { get; set; }
        public virtual List<Expense> Expenses { get; set; }
        public PaymentMode PaymentMode { get; set; }
        [MaxLength(128)]
        public string TransactionCode { get; set; }
        [NotMapped]
        public long BankAccountId { get; set; }
    }

    public class PurchaseItem : HasId
    {
        [MaxLength(512)]
        public string Desc { get; set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductPackage Package { get; set; }
        public long PackageId { get; set; }
        public long Unit { get; set; } //NB: This indicates package unit
        public decimal Cost { get; set; }
        public long Quantity { get; set; }
        public decimal Amount { get; set; }
        public Purchase Purchase { get; set; }
        public long PurchaseId { get; set; }
        public virtual PackageScheme Scheme { get; set; }
        public long? SchemeId { get; set; }
        public DateTime? ExpireDate { get; set; }
    }

    public class Stock : HasId
    {
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public long Reorder { get; set; }
        public long MaximumStock { get; set; }
        public long CurrentStock { get; set; }
        public decimal Price { get; set; }
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        [NotMapped] //NB: This is assit in stock request generation
        public long SchemeId { get; set; }
        [NotMapped]
        public long Quantity { get; set; }
        [NotMapped]
        public long Unit { get; set; }
    }

    public class StockIssue : AuditFields
    {
        public DateTime Date { get; set; }
        [MaxLength(32), Required, Index(IsUnique = true)]
        public string Number { get; set; }
        [MaxLength(32)]
        public string Driver { get; set; }
        public long OriginId { get; set; }
        public virtual Store Origin { get; set; }
        public long DestinationId { get; set; }
        public virtual Store Destination { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public virtual List<StockIssueItem> Items { get; set; }
        public IssueStatus Status { get; set; }
    }

    public enum IssueStatus
    {
        Issued,
        Delivered,
        Rejected
    }

    public class StockIssueItem : HasId
    {
        public long StockIssueId { get; set; }
        public StockIssue StockIssue { get; set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public long Quantity { get; set; }
        public long Unit { get; set; }
        public virtual PackageScheme Scheme { get; set; }
        public long? SchemeId { get; set; }
    }
    public enum RequestStatus
    {
        Pending,
        Approved,
        Rejected
    }
    public class StockRequest : AuditFields
    {
        public DateTime Date { get; set; }
        [MaxLength(32), Required, Index(IsUnique = true)]
        public string Number { get; set; }
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
        public List<StockRequestItem> Items { get; set; }
        public RequestStatus Status { get; set; }
    }
    public class StockRequestItem : HasId
    {
        public long StockRequestId { get; set; }
        public StockRequest StockRequest { get; set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public long Quantity { get; set; }
        public long Unit { get; set; }
        public virtual PackageScheme Scheme { get; set; }
        public long? SchemeId { get; set; }
        [MaxLength(1000)]
        public string Remarks { get; set; }
    }

    public class BadStock : AuditFields
    {
        public DateTime Date { get; set; }
        public virtual Product Product { get; set; }
        public long ProductId { get; set; }
        public virtual Store Store { get; set; }
        public long StoreId { get; set; }
        public long Quantity { get; set; }
        [MaxLength(512)]
        public string Notes { get; set; }
    }

    public class BankAccount : AuditFields
    {
        [Required, MaxLength(64), Index(IsUnique = true)]
        public string Name { get; set; }
        [Required, MaxLength(32)]
        public string Number { get; set; }
        [Required, MaxLength(128)]
        public string Bank { get; set; }
        [Required, MaxLength(256)]
        public string Branch { get; set; }
        public decimal Balance { get; set; }
        public bool ChequeAccount { get; set; }
        [MaxLength(10000)]
        public string Note { get; set; }
    }
    public class BankTransaction : AuditFields
    {
        public DateTime Date { get; set; }
        public virtual Store Store { get; set; }
        public long StoreId { get; set; }
        public long AccountId { get; set; }
        public virtual BankAccount Account { get; set; }
        public Transaction Transaction { get; set; }
        public TransactionSource Source { get; set; }
        public long? SourceId { get; set; }
        public decimal Amount { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        [MaxLength(10000)]
        public string Note { get; set; }
        [NotMapped]
        public decimal OldAmount { get; set; }

    }

    public class StockTally : AuditFields
    {
        public virtual Stock Stock { get; set; }
        public long StockId { get; set; }
        public long Quantity { get; set; }
        public long Balance { get; set; }
        public long SourceId { get; set; }
        public string SourceReference { get; set; }
        public StockSource Source { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Notes { get; set; }
    }

    public enum StockSource
    {
        Sales,
        Purchase,
        StockIssue,
        BadStock,
        SalesReturn,
        StockAdjustment
    }

    public class StockDiary : HasId
    {
        public virtual Stock Stock { get; set; }
        public long StockId { get; set; }
        public DateTime Date { get; set; }
        public long OpeningStock { get; set; }
        public long QuantityPurchases { get; set; }
        public decimal AmountPurchased { get; set; }
        public decimal AmountSold { get; set; }
        public long QuantitySold { get; set; }
        public long ClosingStock { get; set; }
        public long CreditedStock { get; set; }
        public decimal Margin { get; set; }
    }

    public enum TransactionSource
    {
        Bank,
        Sale,
        CustomerPayment,
        SupplierPayment,
        Expense,
        Purchase
    }

    public enum Transaction
    {
        Deposit,
        Withdrawal
    }

    public class SaleReturn : AuditFields
    {
        public DateTime Date { get; set; }
        public long SaleId { get; set; }
        public virtual Sale Sale { get; set; }
        public decimal Balance { get; set; }
        [MaxLength(10000)]
        public string Note { get; set; }
        public string Recepient { get; set; }
        public virtual List<SaleReturnItem> Items { get; set; }
    }

    public class SaleReturnItem : HasId
    {
        public long SaleReturnId { get; set; }
        public SaleReturn SaleReturn { get; set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
        public long Unit { get; set; }
        public decimal Amount { get; set; }
    }

    public class OwnnerTransaction : AuditFields
    {
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        [MaxLength(10000)]
        public string Note { get; set; }
    }

    public class Tax : AuditFields
    {
        [Required, Index(IsUnique = true)]
        public float Percentage { get; set; }
        public float Vat { get; set; }
        public float Nhil { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
    }

}