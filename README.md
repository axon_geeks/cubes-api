# Cubes API Project #

* Current Version 0.0.1
* Built with ASP.NET WebAPI 4.5, C# 6.0, Entity Framework 6.0
* MSSQLServer default instance.

### Getting Started ###

* Clone the project
* `cd cumas`
* Open the project as administrator
* Run the project to create and setup database.
* Application will create a  default database named cubesdemo
Username: `admin`
Password: `admin@cubes`